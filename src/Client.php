<?php
/**
 *
 * @package   Anemone
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone;

use Anemone\Contracts\BeClient;
use Anemone\Core\Auth\AuthService;
use Anemone\Core\Cache\CacheDriver;
use Anemone\Core\Cache\CacheService;
use Anemone\Core\Helpers\ModelsInstances;
use Anemone\Core\Helpers\ValidatorClient;
use Anemone\Core\Managed\ManagingService;
use Anemone\Core\Query\QueryService;
use Anemone\Exceptions\InvalidDataException;
use Illuminate\Support\Collection;

final class Client implements BeClient
{
    use ModelsInstances;
    use ValidatorClient;

    /**
     * @var AuthService $authService
     * */
    protected $authService;
    /**
     * @var CacheService $cacheService
     * */
    protected $cacheService;
    /**
     * @var ManagingService $managingService
     * */
    protected $managingService;
    /**
     * @var Collection $_instances
     * */
    private static $_instances;

    /**
     * @param mixed $params
     * @return void
     * @throws \Anemone\Exceptions\InvalidDataException
     * */
    public function __construct($params)
    {
        $this->authService = new AuthService($this->validate($params));
        $this->cacheService = new CacheService(new CacheDriver($this));
        $this->managingService = new ManagingService();
    }

    /**
     * @return AuthService
     * */
    public function getAuthService()
    {
        return $this->authService;
    }

    /**
     * @return QueryService
     * */
    public function getQueryService()
    {
        return new QueryService($this->authService);
    }

    /**
     * @return ManagingService
     * */
    public function getManagingService()
    {
        return $this->managingService;
    }

    /**
     * @return CacheService
     * */
    public function getCacheService()
    {
        return $this->cacheService;
    }

    /** account domain
     * @return string
     * */
    public function getDomain()
    {
        return $this->authService->getDomain();
    }

    /** push new instance or collect of instances to $_instances
     * @param Collection|Client $clients
     * @return void
     * @throws InvalidDataException
     * */
    public static function addInstance($clients)
    {
        if (!isset(self::$_instances)) {
            self::$_instances = new Collection();
        }

        if ($clients instanceof Collection) {
            self::validateInstances($clients);
            self::$_instances = self::$_instances
                ->filter(
                    function ($item) use ($clients) {
                        return !$clients->some(
                            function ($one) use ($item) {
                                return $one->getDomain() == $item->getDomain();
                            }
                        );
                    }
                )
                ->merge($clients);
        } elseif ($clients instanceof Client) {
            self::$_instances = self::$_instances
                ->filter(
                    function ($item) use ($clients) {
                        return $clients->getDomain() != $item->getDomain();
                    }
                )
                ->push($clients);
        } else {
            throw new InvalidDataException(
                'Param $clients should be instance of [Illuminate\Support\Collection] or [Anemone\Client]'
            );
        }
    }

    /** get instance for $accountId or Collection of instances for $accountId (array|null)
     * @param string[]|string|null $domain
     * @return static|Collection|null
     * */
    public static function getInstance($domain = null)
    {
        if (!isset(self::$_instances)) {
            return null;
        }

        if (is_null($domain)) { // all instances in Collect
            return self::$_instances;
        } elseif (is_array($domain)) { // filtered instances in Collect
            return self::$_instances->filter(
                function ($item) use ($domain) {
                    return in_array($item->getDomain(), $domain);
                }
            );
        } else { // one instance
            return self::$_instances->first(
                function ($item) use ($domain) {
                    return $item->getDomain() == $domain;
                }
            );
        }
    }


    /**
     * @param Collection $clients
     * @return void
     * @throws InvalidDataException
     */
    public static function initClients(Collection $clients)
    {
        self::addInstance(
            $clients->map(
                function ($client) {
                    return new static($client);
                }
            )
        );
    }
}
