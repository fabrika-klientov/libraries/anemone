<?php
/**
 *
 * @package   Anemone
 * @category  Collection
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Collection;

use Anemone\Contracts\BeCollection;
use Illuminate\Support\Collection as BaseCollection;

class Collection extends BaseCollection implements BeCollection
{
}
