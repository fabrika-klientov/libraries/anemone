<?php
/**
 * */

namespace Anemone\Core\Auth\Connectors;

use Anemone\Contracts\BeAuthConnector;

class JsonConnector implements BeAuthConnector
{
    /**
     * @var string $authPath
     * */
    private $authPath;

    /**
     * @return void
     * */
    public function __construct()
    {
        $this->authPath = aConfig('auth.path');
        $this->storeAuthDir();
    }

    /**
     * @param array $authData
     * @return array
     * */
    public function read(array $authData): array
    {
        $file = $this->getFile($authData['domain']);

        return json_decode((string)file_get_contents($file), true) ?? [];
    }

    /**
     * @param array $authData
     * @param array $refreshAuthData
     * @return bool
     * */
    public function write(array $authData, array $refreshAuthData): bool
    {
        $file = $this->getFile($authData['domain']);
        $fp = fopen($file, "w");
        if ($fp) {
            $status = fwrite($fp, (string)json_encode($refreshAuthData));
            fclose($fp);

            return (bool)$status;
        }

        return false;
    }

    /** file for auth
     * @param string $domain
     * @return string
     * */
    protected function getFile(string $domain)
    {
        $file = $this->authPath . DIRECTORY_SEPARATOR . $domain . '.json';
        if (file_exists($file)) {
            return $file;
        }

        if ($fs = fopen($file, 'x')) {
            fclose($fs);
            chmod($file, 0664);
        }

        return $file;
    }

    /** path for auth
     * @return void
     * @throws \RuntimeException
     * */
    protected function storeAuthDir()
    {
        if (!file_exists($this->authPath)) {
            if (!mkdir($this->authPath, 0775, true)) {
                throw new \RuntimeException('Did\'t stored path for auth. Permission denied.');
            }
        }
    }
}