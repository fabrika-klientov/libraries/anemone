<?php
/**
 *
 * @package   Anemone
 * @category  Decorators
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Auth;

use Anemone\Contracts\BeAuthService;
use Anemone\Core\Query\QueryService;

final class AuthService implements BeAuthService
{
    private $authParams;

    /**
     * @param array $authParams
     * @return void
     * */
    public function __construct(array $authParams)
    {
        $this->authParams = $authParams;
    }

    /**
     * @param QueryService $queryService
     * @return \Anemone\Contracts\BeAuth
     */
    public function getDecorator(QueryService $queryService)
    {
        if (isset($this->authParams['access_token'])) {
            $class = OAuth2Decorator::class;
        }

        $class = $class ?? CookiesDecorator::class;
        if (class_exists($class)) {
            return new $class($queryService, $this->authParams);
        }

        throw new \RuntimeException('Decorator class not found [' . $class . ']');
    }

    /** get full domain
     * @return string
     * */
    public function getDomain()
    {
        return $this->authParams['domain'];
    }

    /** auth params
     * @return array
     * */
    public function getAuthParams()
    {
        return $this->authParams;
    }
}
