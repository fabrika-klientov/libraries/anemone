<?php
/**
 *
 * @package   Anemone
 * @category  Decorators
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Auth;

use Anemone\Contracts\BeAuth;
use Anemone\Core\Query\QueryService;
use Anemone\Exceptions\BadResponseException;
use GuzzleHttp\Cookie\FileCookieJar;

class CookiesDecorator implements BeAuth
{
    /**
     * @var QueryService $queryService
     * */
    protected $queryService;
    /**
     * @var array $authParams
     * */
    protected $authParams;
    /**
     * @var string $cookiesPath
     * */
    protected $cookiesPath;
    /**
     * @var array $sessionData
     * */
    protected $sessionData = [];

    /**
     * @param array $authParams
     * @param QueryService $queryService
     * @return void
     * */
    public function __construct(QueryService $queryService, array $authParams)
    {
        $this->queryService = $queryService;
        $this->authParams = $authParams;
        $this->cookiesPath = aConfig('cookies.path');
        $this->storeCookiesDir();
        $this->sessionData['cookies'] = new FileCookieJar($this->cookieName(), true);
    }

    /** default session in cookies (if other, for example [token], you should override this method)
     * @inheritDoc
     * @throws BadResponseException
     * */
    public function getSessionData(): array
    {
        if ($this->sessionData['cookies']->count() > 0) {
            return $this->sessionData;
        }

        return $this->auth();
    }

    /**
     * @inheritDoc
     * @throws BadResponseException
     * */
    public function auth(): array
    {
        try {
            $result = json_decode(
                $this->queryService->post(
                    aEntryData('4.auth.path'),
                    [
                        'USER_LOGIN' => $this->authParams['login'],
                        'USER_HASH' => $this->authParams['secret_key'],
                    ],
                    [
                        'query' => [
                            'type' => 'json',
                        ],
                        'cookies' => $this->sessionData['cookies'],
                    ],
                    false
                ),
                true
            );

            if (isset($result['response'], $result['response']['auth']) && $result['response']['auth']) {
                return $this->sessionData;
            }

            return [];
        } catch (\Exception $exception) {
            throw new BadResponseException('Invalid Auth result. ' . $exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @return void
     * @throws \RuntimeException
     * */
    protected function storeCookiesDir()
    {
        if (!file_exists($this->cookiesPath)) {
            if (!mkdir($this->cookiesPath, 0775, true)) {
                throw new \RuntimeException('Did\'t stored path for cookies. Permission denied.');
            }
        }
    }

    /**
     * @return string
     * */
    protected function cookieName()
    {
        $subDomain = head(explode('.', $this->authParams['domain']));
        $file = $this->cookiesPath . DIRECTORY_SEPARATOR . $subDomain . '.v4' . '.json';
        if (file_exists($file)) {
            return $file;
        }

        if ($fs = fopen($file, 'x')) {
            fclose($fs);
            chmod($file, 0664);
        }

        return $file;
    }

}
