<?php
/**
 * */

namespace Anemone\Core\Auth;

use Anemone\Contracts\BeAuth;
use Anemone\Contracts\BeAuthConnector;
use Anemone\Core\Query\QueryService;
use Anemone\Exceptions\AnemoneException;
use Anemone\Exceptions\BadResponseException;

class OAuth2Decorator implements BeAuth
{
    /**
     * @var QueryService $queryService
     * */
    protected $queryService;
    /**
     * @var array $authParams
     * */
    protected $authParams;
    /**
     * @var array $sessionData
     * */
    protected $sessionData = [
        'headers' => [
            'X-Requested-With' => 'XMLHttpRequest',
        ],
    ];

    /**
     * @param array $authParams
     * @param QueryService $queryService
     * @return void
     * */
    public function __construct(QueryService $queryService, array &$authParams)
    {
        $this->queryService = $queryService;
        $this->authParams = &$authParams;
        $this->sessionData['headers']['Authorization'] = ($authParams['token_type'] ?? 'Bearer') . ' ' . $authParams['access_token'];
    }

    /**
     * @inheritDoc
     *
     * @throws BadResponseException
     * @throws AnemoneException
     */
    public function auth(): array
    {
        if (empty($this->authParams['client_id'])
            || empty($this->authParams['client_secret'])
            || empty($this->authParams['refresh_token'])
            || !isset($this->authParams['redirect_uri'])
        ) {
            throw new AnemoneException(
                'For refreshing access_token Anemone\Client params [client_id, client_secret, refresh_token, redirect_uri] is required'
            );
        }

        try {
            $result = json_decode(
                $this->queryService->post(
                    aExtEntryData('link_oauth'),
                    [
                        'client_id' => $this->authParams['client_id'],
                        'client_secret' => $this->authParams['client_secret'],
                        'grant_type' => 'refresh_token',
                        'refresh_token' => $this->authParams['refresh_token'],
                        'redirect_uri' => $this->authParams['redirect_uri'],
                    ],
                    [
                        'headers' => [
                            'Content-Type' => 'application/json',
                        ],
                    ],
                    false
                ),
                true
            );

            if (isset($result['access_token'], $result['refresh_token'], $result['token_type'])) {
                $this->getConnector()->write($this->authParams, $result);
                $this->authParams = array_merge($this->authParams, $result);
                $this->sessionData['headers']['Authorization'] = ($this->authParams['token_type'] ?? 'Bearer') . ' ' . $this->authParams['access_token'];

                return $this->sessionData;
            }

            return [];
        } catch (\Exception $exception) {
            throw new BadResponseException('Invalid Auth result. ' . $exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @inheritDoc
     * @return array
     * */
    public function getSessionData(): array
    {
        return $this->sessionData;
    }

    /**
     * @return BeAuthConnector
     *
     * @throws AnemoneException
     */
    protected function getConnector()
    {
        $class = aConfig('auth.connector');
        if (class_exists($class) && is_subclass_of($class, BeAuthConnector::class)) {
            return new $class();
        }

        throw new AnemoneException(
            'Connector class [' . $class . '] not found or not subclass of [' . BeAuthConnector::class . ']'
        );
    }
}
