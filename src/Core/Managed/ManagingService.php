<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.11.20
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Managed;

use Anemone\Contracts\BePersist;
use Anemone\Core\Collection\Collection;

class ManagingService
{
    private $data = [];

    /**
     * @param Collection|\Anemone\Contracts\BeModel $data
     * @return void
     */
    public function persist($data)
    {
        if (!is_object($data)) {
            return;
        }

        switch (true) {
            case $data instanceof Collection:
                foreach ($data as $item) {
                    self::persist($item);
                }
                break;
            case $data instanceof BePersist:
                self::doPersist($data);
        }
    }

    /**
     * @param mixed|BePersist $model
     * @return SinglePersist|null
     */
    public function getPersisted($model)
    {
        if (!is_object($model) || !$model instanceof BePersist) {
            return null;
        }

        $hash = spl_object_hash($model);
        if (empty($this->data[$hash])) {
            return null;
        }

        return $this->data[$hash];
    }

    /**
     * @param mixed|BePersist $model
     * @return BePersist|mixed|null
     */
    public function getOptimized($model)
    {
        $persisted = self::getPersisted($model);
        if (empty($persisted)) {
            return $model;
        }

        return $persisted->getOptimized($model);
    }

    /**
     * @param Collection|\Anemone\Contracts\BeModel $data
     * @return void
     */
    public function detach($data)
    {
        if (!is_object($data)) {
            return;
        }

        switch (true) {
            case $data instanceof Collection:
                foreach ($data as $item) {
                    self::detach($item);
                }
                break;
            case $data instanceof BePersist:
                self::doDetach($data);
        }
    }

    /**
     * @param BePersist $model
     * @return void
     */
    private function doPersist(BePersist $model)
    {
        $hash = spl_object_hash($model);
        $single = new SinglePersist($model);

        $this->data[$hash] = $single;
    }

    /**
     * @param BePersist $model
     * @return void
     */
    private function doDetach(BePersist $model)
    {
        if (empty(self::getPersisted($model))) {
            return;
        }

        $hash = spl_object_hash($model);
        unset($this->data[$hash]);
    }

    public function __serialize(): array
    {
        return [];
    }

    public function __unserialize(array $data): void
    {
        new static();
    }
}
