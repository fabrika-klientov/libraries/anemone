<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.11.20
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Managed;

use Anemone\Contracts\BePersist;
use Anemone\Core\Collection\Collection;
use Anemone\Models\CF;

class SinglePersist
{
    /**
     * @var string $hash
     * */
    private $hash;
    /**
     * @var array $fixed
     * */
    private $fixed = [];

    /**
     * @param BePersist $model
     */
    public function __construct(BePersist $model)
    {
        $this->hash = spl_object_hash($model);
        self::init($model);
    }

    /**
     * @param BePersist $model
     * @return void
     */
    protected function init(BePersist $model)
    {
        $this->fixed = json_decode(json_encode($model->getFixed()), true);
    }

    /**
     * @param mixed|BePersist $model
     * @return BePersist|mixed|null
     */
    public function getOptimized($model)
    {
        if (!is_object($model) || !$model instanceof BePersist) {
            return $model;
        }

        $hash = spl_object_hash($model);
        if ($hash != $this->hash) {
            return $model;
        }

        return self::getOptModel($model);
    }

    /**
     * @param BePersist $model
     * @return mixed|null
     */
    protected function getOptModel(BePersist $model)
    {
        $newData = [];
        foreach ($this->fixed as $key => $value) {
            if ($key == 'custom_fields_values') {
                $cf = self::getOptCF($model);
                if ($cf->isNotEmpty()) {
                    $newData[$key] = $cf;
                }
            } elseif ($key == '_embedded') {
                $embedded = self::getOptEmbedded($model);
                if (!empty($embedded)) {
                    $newData[$key] = $embedded;
                }
            } elseif (
                method_exists($model, 'existProp') &&
                $model->existProp($key) &&
                $value != $model->{$key}
            ) {
                $newData[$key] = $model->{$key};
            } elseif ($value != $model->{$key}) {
                $newData[$key] = $model->{$key};
            }
        }

        if (empty($newData)) {
            return null;
        }

        if (!empty($model->id)) {
            $newData['id'] = $model->id;
        }

        return $model->factory($newData);
    }

    /**
     * @param BePersist $model
     * @return Collection
     */
    protected function getOptCF(BePersist $model)
    {
        $newCF = new Collection();
        if (!method_exists($model, 'cf')) {
            return $newCF;
        }

        foreach ($this->fixed['custom_fields_values'] as $item) {
            $cf = $model->cf($item['id']);
            if (empty($cf)) {
                continue;
            }

            switch (true) {
                case $cf instanceof CF\TextCustomField:
                case $cf instanceof CF\TextareaCustomField:
                case $cf instanceof CF\BirthdayCustomField:
                case $cf instanceof CF\DateCustomField:
                case $cf instanceof CF\NumericCustomField:
                case $cf instanceof CF\RadiobuttonCustomField:
                case $cf instanceof CF\SelectCustomField:
                case $cf instanceof CF\StreetAddressCustomField:
                case $cf instanceof CF\UrlCustomField:
                case $cf instanceof CF\PriceCustomField:
                case $cf instanceof CF\CategoryCustomField:
                case $cf instanceof CF\DateTimeCustomField:
                    if (!isset($item['values'][0]['value'])) {
                        if (is_null($cf->getValue())) {
                            continue 2;
                        }

                        $newCF->add($cf);
                        continue 2;
                    }

                    if ($item['values'][0]['value'] != $cf->getValue()) {
                        $newCF->add($cf);
                    }
                    break;

                case $cf instanceof CF\CheckboxCustomField:
                    if (!isset($item['values'][0]['value'])) {
                        if (!$cf->getValue()) {
                            continue 2;
                        }

                        $newCF->add($cf);
                        continue 2;
                    }

                    if ((bool)$item['values'][0]['value'] != $cf->getValue()) {
                        $newCF->add($cf);
                    }
                    break;

                case $cf instanceof CF\MultiSelectCustomField:
                case $cf instanceof CF\MultiTextCustomField:
                case $cf instanceof CF\LegalEntityCustomField:
                case $cf instanceof CF\OrgLegalNameCustomField:
                case $cf instanceof CF\SmartAddressCustomField:
                    if (!isset($item['values'])) {
                        if (empty($cf->values)) {
                            continue 2;
                        }

                        $newCF->add($cf);
                        continue 2;
                    }

                    $strOld = json_encode($item['values']);
                    $strNew = json_encode($cf->values);
                    if ($strOld != $strNew) {
                        $newCF->add($cf);
                    }
                    break;

                default:
                    $newCF->add($cf);
            }
        }

        return $newCF;
    }

    /**
     * @param BePersist $model
     * @return array
     */
    protected function getOptEmbedded(BePersist $model)
    {
        if (empty($this->fixed['_embedded']) && empty($model->_embedded)) {
            return [];
        }

        $newData = [];

        $keys = array_merge(array_keys($this->fixed['_embedded'] ?? []), array_keys($model->_embedded ?? []));
        foreach ($keys as $key) {
            $old = json_encode($this->fixed['_embedded'][$key] ?? '');
            $new = json_encode($model->_embedded[$key] ?? '');
            if ($old != $new) {
                $newData[$key] = $model->_embedded[$key] ?? null;
            }
        }

        return $newData;
    }
}
