<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters;

use Anemone\Core\Collection\Collection;
use Anemone\Models\Instances\ModelInstance;

abstract class BaseAdapter
{
    /**
     * @var mixed|array<string, mixed>|\Anemone\Core\Builder\Builder|Collection $srcData
     * */
    protected $srcData;
    /**
     * @var ModelInstance $modelInstance
     * */
    protected $modelInstance;

    /**
     * @param ModelInstance $modelInstance
     * @param mixed $data
     * @param bool $isCache
     * @return void
     * */
    public function __construct(ModelInstance $modelInstance, $data, bool $isCache = false)
    {
        $this->modelInstance = $modelInstance;
        $this->srcData = $data;
        $this->missingCache($isCache);
    }

    /** base query params
     * @override
     * @return array
     * */
    public function getSelectRequest()
    {
        return $this->srcData->getResult();
    }

    /**
     * @override
     * @return array
     * */
    public function getInsertRequest()
    {
        return $this->srcData->toArray();
    }

    /**
     * @override
     * @param Collection $collect
     * @return Collection
     */
    public function mergeInsertResponse($collect)
    {
        array_map(
            function ($item, $append) {
                $item->id = $append['id'];
                return null;
            },
            $collect->toArray(),
            $this->srcData['_embedded'][$this->getResponseCode()]
        );

        return $collect;
    }

    /**
     * @override
     * @return array
     * */
    public function getUpdateRequest()
    {
        return $this->getInsertRequest();
    }

    /**
     * @override
     * @param Collection $collect
     * @return Collection
     */
    public function mergeUpdateResponse($collect)
    {
        $result = collect($this->srcData['_embedded'][$this->getResponseCode()] ?? []);
        $collect->each(
            function ($item) use ($result) {
                $oneResult = $result
                    ->first(
                        function ($one) use ($item) {
                            return $one['id'] == $item->id;
                        }
                    );
                if (isset($oneResult['updated_at'])) {
                    $item->updated_at = $oneResult['updated_at'];
                }
            }
        );

        return $collect;
    }

    /**
     * @override
     * @return mixed
     * */
    public function getDeleteRequest()
    {
        return null;
    }

    /**
     * @override
     * @param Collection $collect
     * @return Collection
     */
    public function mergeDeleteResponse($collect)
    {
        return $collect;
    }

    /** smart converting custom field in models
     * @param array $customFields
     * @return Collection|array
     * */
    protected function injectCustomFields(array $customFields)
    {
        if (!method_exists($this->modelInstance, 'cf')) {
            return new Collection();
        }

        return $this->modelInstance->cf(
            array_filter(
                $customFields,
                function ($one) {
                    return isset($one['field_id']);
                }
            )
        );
    }

    /**
     * @return string
     * */
    abstract protected function getClassModel();

    /** helper for caching converting
     * @param bool $isCache
     * @return void
     * */
    abstract protected function missingCache(bool $isCache);

    /**
     * @return string
     * */
    abstract protected function getResponseCode(): string;
}
