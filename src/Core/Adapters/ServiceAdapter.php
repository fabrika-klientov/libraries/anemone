<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters;

use Anemone\Contracts\BeServiceAdapter;
use Anemone\Exceptions\InvalidDataException;
use Anemone\Models\Instances\ModelInstance;
use Illuminate\Support\Str;

class ServiceAdapter implements BeServiceAdapter
{
    /**
     * @var ModelInstance $modelInstance
     * */
    private $modelInstance;
    /**
     * @var string $version
     * */
    private $version;
    /**
     * @var string $entity
     * */
    private $entity;
    /**
     * @var bool $isCache
     * */
    private $isCache = false;

    /**
     * @param ModelInstance $modelInstance
     * @param string $version
     * @param string $entity
     * @return void
     * */
    public function __construct(ModelInstance $modelInstance, $version, $entity)
    {
        $this->modelInstance = $modelInstance;
        $this->version = $version;
        $this->entity = $entity;
    }

    /** check key for cache instances
     * @param bool $status
     * @return void
     * */
    public function fromCache(bool $status = false)
    {
        $this->isCache = $status;
    }

    /**
     * @param array $srcData
     * @return \Anemone\Core\Collection\Collection
     * @throws InvalidDataException
     * */
    public function getData($srcData = null)
    {
        return $this->getAdapter($srcData)->getData();
    }

    /**
     * @param \Anemone\Core\Builder\Builder $data
     * @return array
     * @throws InvalidDataException
     * */
    public function getSelectRequest($data = null)
    {
        return $this->getAdapter($data)->getSelectRequest();
    }

    /**
     * @param \Anemone\Core\Collection\Collection $data
     * @return \Anemone\Core\Collection\Collection|array|null
     * @throws InvalidDataException
     * */
    public function getInsertRequest($data = null)
    {
        return $this->getAdapter($data)->getInsertRequest();
    }

    /** merging result for insert
     * @param \Anemone\Core\Collection\Collection $collect
     * @param array $data
     * @return \Anemone\Core\Collection\Collection
     * @throws InvalidDataException
     * */
    public function mergeInsertResponse($collect, $data = null)
    {
        return $this->getAdapter($data)->mergeInsertResponse($collect);
    }

    /**
     * @param \Anemone\Core\Collection\Collection $data
     * @return \Anemone\Core\Collection\Collection|array|null
     * @throws InvalidDataException
     * */
    public function getUpdateRequest($data = null)
    {
        return $this->getAdapter($data)->getUpdateRequest();
    }

    /** merging result for update
     * @param \Anemone\Core\Collection\Collection $collect
     * @param array $data
     * @return \Anemone\Core\Collection\Collection
     * @throws InvalidDataException
     * */
    public function mergeUpdateResponse($collect, $data = null)
    {
        return $this->getAdapter($data)->mergeUpdateResponse($collect);
    }

    /**
     * @param \Anemone\Core\Collection\Collection $data
     * @return \Anemone\Core\Collection\Collection|array|null
     * @throws InvalidDataException
     * */
    public function getDeleteRequest($data = null)
    {
        return $this->getAdapter($data)->getDeleteRequest();
    }

    /** merging result for delete
     * @param \Anemone\Core\Collection\Collection $collect
     * @param array $data
     * @return \Anemone\Core\Collection\Collection
     * @throws InvalidDataException
     * */
    public function mergeDeleteResponse($collect, $data = null)
    {
        return $this->getAdapter($data)->mergeDeleteResponse($collect);
    }


    /**
     * @param mixed $srcData
     * @return \Anemone\Contracts\BeAdapter
     * @throws InvalidDataException
     * */
    protected function getAdapter($srcData)
    {
        $class = 'Anemone\Core\Adapters\V' . $this->version . '\\' . Str::ucfirst($this->entity) . 'Adapter';
        if (class_exists($class)) {
            return new $class($this->modelInstance, $srcData, $this->isCache);
        }

        throw new InvalidDataException('Class [' . $class . '] not exist');
    }
}
