<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.16
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4;

use Anemone\Contracts\BeAdapter;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Catalog;

class CatalogAdapter extends BaseAdapter implements BeAdapter
{
    /** get collect of models
     * @return Collection
     * @throws \Exception
     * */
    public function getData()
    {
        $class = $this->getClassModel();
        return new Collection(
            array_map(
                function ($item) use ($class) {
                    return new $class(
                        $this->modelInstance,
                        [
                            'id' => $item['id'],
                            'name' => $item['name'],
                            'created_by' => $item['created_by'] ?? null,
                            'updated_by' => $item['updated_by'] ?? null,
                            'created_at' => $item['created_at'] ?? null,
                            'updated_at' => $item['updated_at'] ?? null,
                            'sort' => $item['sort'] ?? null,
                            'type' => $item['type'] ?? null,
                            'can_add_elements' => $item['can_add_elements'] ?? null,
                            'can_show_in_cards' => $item['can_show_in_cards'] ?? null,
                            'can_link_multiple' => $item['can_link_multiple'] ?? null,
                            'can_be_deleted' => $item['can_be_deleted'] ?? null,
                            'sdk_widget_code' => $item['sdk_widget_code'] ?? null,
                            'account_id' => $item['account_id'] ?? null,
                        ]
                    );
                },
                $this->srcData['_embedded'][$this->getResponseCode()] ?? []
            )
        );
    }

    protected function getClassModel()
    {
        return Catalog::class;
    }

    protected function getResponseCode(): string
    {
        return 'catalogs';
    }
}
