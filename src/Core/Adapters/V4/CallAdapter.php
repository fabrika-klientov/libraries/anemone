<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.15
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4;

use Anemone\Contracts\BeAdapter;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Call;

class CallAdapter extends BaseAdapter implements BeAdapter
{
    /** get collect of models
     * @return Collection
     * */
    public function getData()
    {
        return new Collection();
    }

    public function getInsertRequest()
    {
        return array_map(function ($item) {
            $arItem = $item->jsonSerialize();
            unset($arItem['_embedded']);
            static::parseInt($arItem);
            return $arItem;
        }, parent::getInsertRequest());
    }

    public function mergeInsertResponse($collect)
    {
        array_map(function ($item, $append) {
            $item->id = $append['id'] ?? null;
            $item->entity_id = $append['entity_id'] ?? null;
            $item->entity_type = $append['entity_type'] ?? null;
            return null;
        }, $collect->toArray(), $this->srcData['_embedded'][$this->getResponseCode()] ?? []);

        return $collect;
    }

    public function getUpdateRequest()
    {
        return [];
    }

    public function mergeUpdateResponse($collect)
    {
        return $collect;
    }

    protected function getClassModel()
    {
        return Call::class;
    }

    protected function getResponseCode(): string
    {
        return 'calls';
    }
}
