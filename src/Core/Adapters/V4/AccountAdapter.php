<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4;

use Anemone\Contracts\BeAdapter;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Account;

class AccountAdapter extends BaseAdapter implements BeAdapter
{
    /**
     * @var string $code
     * */
    protected $code = 'anemone';

    /** adapting cache
     * @param bool $isCache
     * @return void
     * */
    protected function missingCache(bool $isCache)
    {
        if ($isCache) {
            $this->srcData = head($this->srcData);
        }
    }

    /** get collect of models
     * @return Collection
     * @throws \Exception
     * */
    public function getData()
    {
        $class = $this->getClassModel();
        return new Collection(
            [
                new $class(
                    $this->modelInstance,
                    [
                        'id' => $this->srcData['id'],
                        'name' => $this->srcData['name'],
                        'subdomain' => $this->srcData['subdomain'],
                        'created_at' => $this->srcData['created_at'] ?? null,
                        'created_by' => $this->srcData['created_by'] ?? null,
                        'updated_at' => $this->srcData['updated_at'] ?? null,
                        'updated_by' => $this->srcData['updated_by'] ?? null,
                        'current_user_id' => $this->srcData['current_user_id'],
                        'country' => $this->srcData['country'],
                        'customers_mode' => $this->srcData['customers_mode'],
                        'is_unsorted_on' => $this->srcData['is_unsorted_on'],
                        'mobile_feature_version' => $this->srcData['mobile_feature_version'],
                        'is_loss_reason_enabled' => $this->srcData['is_loss_reason_enabled'],
                        'is_helpbot_enabled' => $this->srcData['is_helpbot_enabled'],
                        'is_technical_account' => $this->srcData['is_technical_account'],
                        'contact_name_display_order' => $this->srcData['contact_name_display_order'],
                        'amojo_id' => $this->srcData['amojo_id'],
                        'version' => $this->srcData['version'],
                        'entity_names' => $this->srcData['entity_names'],
                        'amojo_rights' => $this->srcData['amojo_rights'] ?? $this->srcData['_embedded']['amojo_rights'] ?? null,
                        'users_groups' => $this->srcData['users_groups'] ?? $this->srcData['_embedded']['users_groups'] ?? null,
                        'task_types' => $this->srcData['task_types'] ?? $this->srcData['_embedded']['task_types'] ?? null,
                        'datetime_settings' => $this->srcData['datetime_settings'] ?? $this->srcData['_embedded']['datetime_settings'] ?? null,
                    ]
                ),
            ]
        );
    }

    public function getInsertRequest()
    {
        return [];
    }

    public function mergeInsertResponse($collect)
    {
        return $collect;
    }

    public function getUpdateRequest()
    {
        return [];
    }

    public function mergeUpdateResponse($collect)
    {
        return $collect;
    }

    protected function getClassModel()
    {
        return Account::class;
    }

    protected function getResponseCode(): string
    {
        return 'account';
    }
}
