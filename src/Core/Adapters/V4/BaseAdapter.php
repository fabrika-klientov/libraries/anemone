<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4;

use Anemone\Core\Adapters\BaseAdapter as Adapter;
use Anemone\Core\Adapters\V4\Base\Parse;

abstract class BaseAdapter extends Adapter
{
    use Parse;

    /** adapting cache
     * @override
     * @param bool $isCache
     * @return void
     * */
    protected function missingCache(bool $isCache)
    {
        if ($isCache) {
            $this->srcData = [
                '_embedded' => [
                    $this->getResponseCode() => $this->srcData,
                ],
            ];
        }
    }
}
