<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4;

use Anemone\Contracts\BeAdapter;
use Anemone\Core\Adapters\V4\Base\BaseModel;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Lead;

class LeadAdapter extends BaseAdapter implements BeAdapter
{
    use BaseModel;

    /** get collect of models
     * @return Collection
     * @throws \Exception
     * */
    public function getData()
    {
        $class = $this->getClassModel();
        return new Collection(
            array_map(
                function ($item) use ($class) {
                    return new $class(
                        $this->modelInstance,
                        [
                            'id' => $item['id'],
                            'name' => $item['name'],
                            'price' => $item['price'] ?? null,
                            'responsible_user_id' => $item['responsible_user_id'] ?? null,
                            'group_id' => $item['group_id'] ?? null,
                            'status_id' => $item['status_id'] ?? null,
                            'pipeline_id' => $item['pipeline_id'] ?? null,
                            'loss_reason_id' => $item['loss_reason_id'] ?? null,
                            'source_id' => $item['source_id'] ?? null,
                            'created_by' => $item['created_by'] ?? null,
                            'updated_by' => $item['updated_by'] ?? null,
                            'created_at' => $item['created_at'] ?? null,
                            'updated_at' => $item['updated_at'] ?? null,
                            'closed_at' => $item['closed_at'] ?? null,
                            'closest_task_at' => $item['closest_task_at'] ?? null,
                            'is_deleted' => $item['is_deleted'] ?? null,
                            'custom_fields_values' => $this->injectCustomFields($item['custom_fields_values'] ?? []),
                            'score' => $item['score'] ?? null,
                            'account_id' => $item['account_id'] ?? null,
                            'is_price_modified_by_robot' => $item['is_price_modified_by_robot'] ?? null,
                            '_embedded' => $item['_embedded'] ?? null,
                            // tags, catalog_elements, loss_reason, companies, contacts
                        ]
                    );
                },
                $this->srcData['_embedded'][$this->getResponseCode()] ?? []
            )
        );
    }

    protected function getClassModel()
    {
        return Lead::class;
    }

    protected function getResponseCode(): string
    {
        return 'leads';
    }
}
