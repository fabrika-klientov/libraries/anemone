<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4;

use Anemone\Contracts\BeAdapter;
use Anemone\Core\Collection\Collection;
use Anemone\Models\IncomingLead;
use Illuminate\Support\Str;

class IncomingLeadAdapter extends BaseAdapter implements BeAdapter
{
    /**
     * @var string|null $currentModel
     * */
    protected $currentModel = IncomingLead::class;

    /**
     * @var string[] $delKeys
     * */
    protected static $delKeys = ['updated_at'];

    public function getData()
    {
        $class = $this->getClassModel();
        return new Collection(
            array_map(
                function ($item) use ($class) {
                    unset($item['_links']);

                    /**
                     * @var IncomingLead $incomingLead
                     * */
                    $incomingLead = new $class($this->modelInstance, $item);
                    $incomingLead->_embedded = array_reduce(
                        array_keys($item['_embedded'] ?? []),
                        function ($result, $key) use ($item) {
                            $class = $this->getClassModelOfType($key);
                            // set model class (for CF)
                            $this->currentModel = $class;

                            $result[$key] = new Collection(
                                array_map(
                                    function ($one) use ($class, $key) {
                                        unset($one['_links']);
                                        $model = new $class($this->modelInstance->getClient()->{$key}, $one);
                                        $model->custom_fields_values = $this->injectCustomFields(
                                            $one['custom_fields_values'] ?? []
                                        );

                                        return $model;
                                    },
                                    class_exists($class ?? '') ? ($item['_embedded'][$key] ?? []) : []
                                )
                            );

                            return $result;
                        },
                        []
                    );

                    // set parent class
                    $this->currentModel = IncomingLead::class;

                    return $incomingLead;
                },
                $this->srcData['_embedded'][$this->getResponseCode()] ?? []
            )
        );
    }

    public function getInsertRequest()
    {
        return array_map(
            function ($item) {
                $arrayData = $item->jsonSerialize();

                static::parseInt($arrayData);

                $metadata = $arrayData['metadata'] ?? [];
                static::parseDate($metadata);
                static::parseInt($metadata);
                $arrayData['metadata'] = $metadata;

                $arrayData['_embedded'] = self::linkedAdapting($arrayData['_embedded'] ?? []);

                return array_filter(
                    $arrayData,
                    function ($key) {
                        return !in_array($key, static::$delKeys);
                    },
                    ARRAY_FILTER_USE_KEY
                );
            },
            parent::getInsertRequest()
        );
    }

    public function mergeInsertResponse($collect)
    {
        if (isset($this->srcData['_embedded'][$this->getResponseCode()])) {
            array_map(
                function ($item, $one) {
                    $item->uid = $one['uid'];
                    return null;
                },
                $collect->toArray(),
                $this->srcData['_embedded'][$this->getResponseCode()]
            );
        }
        return $collect;
    }

    public function mergeUpdateResponse($collect)
    {
        return $collect;
    }

    /** get Class helper
     * @param string $type
     * @return string|null
     * */
    protected function getClassModelOfType(string $type)
    {
        switch ($type) {
            case 'leads':
                return 'Anemone\Models\Lead';
            case 'contacts':
                return 'Anemone\Models\Contact';
            case 'companies':
                return 'Anemone\Models\Company';
            case 'customers':
                return 'Anemone\Models\Customer';
        }

        return null;
    }

    /**
     * @param array $data
     * @return array
     */
    protected function linkedAdapting(array $data): array
    {
        $result = [];
        foreach (['leads', 'contacts', 'companies'] as $type) {
            if ($linked = self::getDataFromAdapter($type, $data[$type] ?? null)) {
                $result[$type] = $linked;
            }
        }

        return $result;
    }

    /**
     * @param string $type
     * @param mixed $data
     * @return array
     */
    protected function getDataFromAdapter(string $type, $data): array
    {
        if (empty($data)) {
            return [];
        }

        if (!$data instanceof Collection) {
            $data = new Collection($data);
        }

        $start = Str::ucfirst($type == 'companies' ? 'company' : Str::beforeLast($type, 's'));
        $class = 'Anemone\\Core\\Adapters\\V4\\' . $start . 'Adapter';

        if (class_exists($class)) {
            $adapter = new $class($this->modelInstance, $data);

            return $adapter->getInsertRequest();
        }

        return [];
    }

    /**
     * @return string|null
     * */
    protected function getClassModel()
    {
        return $this->currentModel;
    }

    protected function getResponseCode(): string
    {
        return 'unsorted';
    }
}
