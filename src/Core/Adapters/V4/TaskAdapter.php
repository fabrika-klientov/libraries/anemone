<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4;

use Anemone\Contracts\BeAdapter;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Task;

class TaskAdapter extends BaseAdapter implements BeAdapter
{
    /** get collect of models
     * @return Collection
     * @throws \Exception
     * */
    public function getData()
    {
        $class = $this->getClassModel();
        return new Collection(
            array_map(
                function ($item) use ($class) {
                    return new $class(
                        $this->modelInstance,
                        [
                            'id' => $item['id'],
                            'text' => $item['text'],
                            'responsible_user_id' => $item['responsible_user_id'],
                            'created_by' => $item['created_by'],
                            'created_at' => $item['created_at'],
                            'updated_at' => $item['updated_at'],
                            'updated_by' => $item['updated_by'],
                            'entity_id' => $item['entity_id'],
                            'entity_type' => $item['entity_type'],
                            'group_id' => $item['group_id'],
                            'duration' => $item['duration'],
                            'complete_till' => $item['complete_till'],
                            'task_type_id' => $item['task_type_id'],
                            'is_completed' => $item['is_completed'],
                            'account_id' => $item['account_id'],
                            'result' => $item['result'],
                        ]
                    );
                },
                $this->srcData['_embedded'][$this->getResponseCode()] ?? []
            )
        );
    }

    public function getInsertRequest()
    {
        return array_map(
            function ($item) {
                $arItem = $item->jsonSerialize();
                static::parseInt($arItem);
                static::parseBool($arItem);
                return $arItem;
            },
            parent::getInsertRequest()
        );
    }

    public function getUpdateRequest()
    {
        return array_map(function ($item) {
            unset($item['created_at']);
            return $item;
        }, parent::getUpdateRequest());
    }

    protected function getClassModel()
    {
        return Task::class;
    }

    protected function getResponseCode(): string
    {
        return 'tasks';
    }
}
