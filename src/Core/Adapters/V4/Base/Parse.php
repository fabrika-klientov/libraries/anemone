<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.05
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4\Base;

use Anemone\Contracts\BeCustomField;
use Anemone\Models\CF;

trait Parse
{
    /**
     * @var string[] $intKeys
     * */
    protected static $intKeys = [
        'responsible_user_id',
        'price',
        'group_id',
        'status_id',
        'pipeline_id',
        'loss_reason_id',
        'source_id',
        'created_by',
        'updated_by',
        'next_price',
        'ltv',
        'purchases_count',
        'average_check',
        'account_id',
        'entity_id',
        'task_type_id',
        'duration',
        'complete_till',
        'call_status',
        'called_at',
    ];

    /**
     * @var string[] $boolKeys
     * */
    protected static $boolKeys = [
        'is_completed',
    ];

    /**
     * @var string[] $dateKeys
     * */
    protected static $dateKeys = [
        'called_at',
        'closed_at',
        'created_at',
        'updated_at',
        'received_at',
        'form_sent_at',
    ];

    /**
     * @var string[] $noNULL
     * */
    protected static $noNULL = [
        'first_name',
        'last_name',
    ];

    /**
     * @param array $data
     * @return void
     */
    protected static function parseInt(array &$data)
    {
        collect(static::$intKeys)->each(
            function ($key) use (&$data) {
                if (array_key_exists($key, $data) && !is_null($data[$key])) {
                    if (is_string($data[$key])) {
                        $val = preg_replace('/\D/', '', head(preg_split('/[.,]/', $data[$key])));
                    }
                    $data[$key] = (int)($val ?? $data[$key]);
                }
            }
        );
    }

    /**
     * @param array $data
     * @return void
     */
    protected static function parseBool(array &$data)
    {
        collect(static::$boolKeys)->each(
            function ($key) use (&$data) {
                if (array_key_exists($key, $data) && !is_null($data[$key])) {
                    $data[$key] = (bool)$data[$key];
                }
            }
        );
    }

    /**
     * @param array $data
     * @return void
     */
    protected static function parseDate(array &$data)
    {
        collect(static::$dateKeys)->each(
            function ($key) use (&$data) {
                if (array_key_exists($key, $data) && is_string($data[$key])) {
                    if (($time = strtotime($data[$key])) !== false) {
                        $data[$key] = $time;
                    }
                }
            }
        );
    }

    /**
     * @param BeCustomField $customField
     * @param mixed $value
     * @return bool|int|string
     */
    protected static function parseCF(BeCustomField $customField, $value)
    {
        switch (true) {
            case $customField instanceof CF\TextCustomField:
            case $customField instanceof CF\TextareaCustomField:
            case $customField instanceof CF\NumericCustomField:
            case $customField instanceof CF\MultiSelectCustomField:
            case $customField instanceof CF\SelectCustomField:
            case $customField instanceof CF\RadiobuttonCustomField:
            case $customField instanceof CF\MultiTextCustomField:
            case $customField instanceof CF\UrlCustomField:
            case $customField instanceof CF\StreetAddressCustomField:
            case $customField instanceof CF\PriceCustomField:
            case $customField instanceof CF\CategoryCustomField:
                return (string)$value;

            case $customField instanceof CF\DateCustomField:
            case $customField instanceof CF\BirthdayCustomField:
            case $customField instanceof CF\DateTimeCustomField:
                return (int)$value;

            case $customField instanceof CF\CheckboxCustomField:
                return (bool)$value;
        }

        return $value;
    }

    /**
     * @param BeCustomField $customField
     * @param array $value
     * @return array
     */
    protected static function resultCF(BeCustomField $customField, $value)
    {
        switch (true) {
            case $customField instanceof CF\TextCustomField:
            case $customField instanceof CF\TextareaCustomField:
            case $customField instanceof CF\NumericCustomField:
            case $customField instanceof CF\SelectCustomField:
            case $customField instanceof CF\RadiobuttonCustomField:
            case $customField instanceof CF\UrlCustomField:
            case $customField instanceof CF\CheckboxCustomField:
            case $customField instanceof CF\PriceCustomField:
            case $customField instanceof CF\CategoryCustomField:
                if (count($value['values']) > 1) {
                    $value['values'] = [head($value['values'])];
                }

                return $value;
        }

        return $value;
    }

    /**
     * @param array $data
     * @return void
     */
    protected static function noNull(array &$data)
    {
        collect(static::$noNULL)->each(
            function ($key) use (&$data) {
                if (array_key_exists($key, $data) && is_null($data[$key])) {
                    unset($data[$key]);
                }
            }
        );
    }

    /**
     * @param array $data
     * @return void
     */
    protected static function setActionUser(array &$data)
    {
        $user = (int)aConfig('general.user_update');
        if ($user > -1) {
            if (empty($data['id'])) {
                $data['created_by'] = $user;
            } else {
                $data['updated_by'] = $user;
            }
        }
    }

    /**
     * @param array $data
     * @return void
     */
    protected static function adaptingForStatus(array &$data)
    {
        if (empty($data['status_id'])) {
            return;
        }

        switch (true) {
            case $data['status_id'] != 143:
                if (!empty($data['loss_reason_id'])) {
                    $data['loss_reason_id'] = null;
                }
            // ..
        }
    }
}
