<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4\Base;

use Anemone\Contracts\BeCustomField;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Model;

/**
 * @property \Anemone\Models\Instances\ModelInstance $modelInstance
 * */
trait BaseModel
{
    /**
     * @inheritDoc
     * */
    public function getInsertRequest()
    {
        return self::_getInsertRequest($this->srcData);
    }

    private function _getInsertRequest(Collection $srcData)
    {
        return $srcData
            ->map(
                function ($item) {
                    $managingService = $this->modelInstance->getClient()->getManagingService();
                    return $managingService->getOptimized($item);
                }
            )
            ->filter(
                function ($item) {
                    return isset($item);
                }
            )
            ->map(
                function ($item) {
                    return self::_singleInsertRequest($item);
                }
            )
            ->toArray();
    }

    private function _singleInsertRequest($item)
    {
        $arrayData = $item->jsonSerialize();

        if (isset($item->custom_fields_values)) {
            $arrayData['custom_fields_values'] = $item->custom_fields_values->reduce(
                function ($result, BeCustomField $item) {
                    if (isset($item->values)) {
                        $prepare = [
                            'field_id' => $item->id,
                            'values' => array_map(
                                function ($value) use ($item) {
                                    return array_merge(
                                        ['value' => static::parseCF($item, $value['value'])],
                                        isset($value['enum_id']) ? ['enum_id' => $value['enum_id']] : [],
                                        isset($value['enum_code']) ? ['enum_code' => $value['enum_code']] : []
                                    );
                                },
                                $item->values
                            ),
                        ];

                        $result[] = static::resultCF($item, $prepare);
                    }
                    return $result;
                },
                []
            );
        }

        if (empty($arrayData['custom_fields_values'])) {
            unset($arrayData['custom_fields_values']);
        }

        if (isset($arrayData['_embedded'])) {
            foreach (['contacts', 'companies', 'leads', 'customers', 'tasks'] as $key) {
                if (!empty($arrayData['_embedded'][$key])) {
                    $arrayData['_embedded'][$key] = array_map(
                        function ($item) {
                            if ($item instanceof Model) {
                                $item = self::_singleInsertRequest($item);
                            }

                            if (empty($item['custom_fields_values'])) {
                                unset($item['custom_fields_values']);
                            }

                            if (empty($item['_embedded'])) {
                                unset($item['_embedded']);
                            }

                            unset($item['_links']);

                            return $item;
                        },
                        $arrayData['_embedded'][$key]
                    );
                }
            }
        }

        if (empty($arrayData['_embedded'])) {
            unset($arrayData['_embedded']);
        }

        static::parseDate($arrayData);
        static::parseInt($arrayData);
        static::noNull($arrayData);
        static::setActionUser($arrayData);
        static::adaptingForStatus($arrayData);

        return $arrayData;
    }
}
