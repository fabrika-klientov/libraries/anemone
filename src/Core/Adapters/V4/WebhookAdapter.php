<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4;

use Anemone\Contracts\BeAdapter;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Webhook;

class WebhookAdapter extends BaseAdapter implements BeAdapter
{
    public function getData()
    {
        $class = $this->getClassModel();
        return new Collection(
            array_map(function ($item) use ($class) {
                return new $class(
                    $this->modelInstance,
                    [
                        'id' => $item['id'],
                        'destination' => $item['destination'],
                        'created_at' => $item['created_at'],
                        'updated_at' => $item['updated_at'],
                        'account_id' => $item['account_id'],
                        'created_by' => $item['created_by'],
                        'sort' => $item['sort'],
                        'disabled' => $item['disabled'],
                        'settings' => $item['settings'] ?? [],
                    ]
                );
            }, $this->srcData['_embedded'][$this->getResponseCode()] ?? [])
        );
    }

    public function getInsertRequest()
    {
        $modelAr = $this->srcData->first()->jsonSerialize();
        unset($modelAr['_embedded']);

        return $modelAr;
    }

    public function mergeInsertResponse($collect)
    {
        $collect->first()->id = $this->srcData['id'] ?? null;
        return $collect;
    }

    public function getUpdateRequest()
    {
        return [];
    }

    public function mergeUpdateResponse($collect)
    {
        return $collect;
    }

    public function getDeleteRequest()
    {
        return ['destination' => $this->getInsertRequest()['destination'] ?? ''];
    }

    public function mergeDeleteResponse($collect)
    {
        return $collect;
    }

    protected function getClassModel()
    {
        return Webhook::class;
    }

    protected function getResponseCode(): string
    {
        return 'webhooks';
    }
}
