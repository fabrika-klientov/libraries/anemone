<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4;

use Anemone\Contracts\BeAdapter;
use Anemone\Core\Collection\Collection;
use Anemone\Models\User;

class UserAdapter extends BaseAdapter implements BeAdapter
{
    public function getData()
    {
        $class = $this->getClassModel();
        return new Collection(
            array_map(
                function ($item) use ($class) {
                    unset($item['_links']);
                    return new $class($this->modelInstance, $item);
                },
                $this->srcData['_embedded'][$this->getResponseCode()] ?? []
            )
        );
    }

    protected function getClassModel()
    {
        return User::class;
    }

    protected function getResponseCode(): string
    {
        return 'users';
    }
}
