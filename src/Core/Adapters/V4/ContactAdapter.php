<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4;

use Anemone\Contracts\BeAdapter;
use Anemone\Core\Adapters\V4\Base\BaseModel;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Contact;

class ContactAdapter extends BaseAdapter implements BeAdapter
{
    use BaseModel;

    /** get collect of models
     * @return Collection
     * @throws \Exception
     * */
    public function getData()
    {
        $class = $this->getClassModel();
        return new Collection(
            array_map(
                function ($item) use ($class) {
                    return new $class(
                        $this->modelInstance,
                        [
                            'id' => $item['id'],
                            'name' => $item['name'] ?? null,
                            'first_name' => $item['first_name'] ?? null,
                            'last_name' => $item['last_name'] ?? null,
                            'responsible_user_id' => $item['responsible_user_id'] ?? null,
                            'created_by' => $item['created_by'] ?? null,
                            'created_at' => $item['created_at'] ?? null,
                            'updated_at' => $item['updated_at'] ?? null,
                            'updated_by' => $item['updated_by'] ?? null,
                            'company_id' => $item['company_id'] ?? null, // in doc (bat not exist)
                            'group_id' => $item['group_id'] ?? null,
                            'account_id' => $item['account_id'] ?? null,
                            'closest_task_at' => $item['closest_task_at'] ?? null,
                            'custom_fields_values' => $this->injectCustomFields($item['custom_fields_values'] ?? []),
                            '_embedded' => $item['_embedded'] ?? null,
                            // tags, companies, leads
                        ]
                    );
                },
                $this->srcData['_embedded'][$this->getResponseCode()] ?? []
            )
        );
    }

    protected function getClassModel()
    {
        return Contact::class;
    }

    protected function getResponseCode(): string
    {
        return 'contacts';
    }
}
