<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4;

use Anemone\Contracts\BeAdapter;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Note;

class NoteAdapter extends BaseAdapter implements BeAdapter
{
    public function getData()
    {
        $class = $this->getClassModel();
        return new Collection(
            array_map(
                function ($item) use ($class) {
                    return new $class(
                        [
                            'id' => $item['id'],
                            'entity_id' => $item['entity_id'] ?? null,
                            'responsible_user_id' => $item['responsible_user_id'] ?? null,
                            'created_by' => $item['created_by'] ?? null,
                            'created_at' => $item['created_at'] ?? null,
                            'updated_at' => $item['updated_at'] ?? null,
                            'updated_by' => $item['updated_by'] ?? null,
                            'params' => $item['params'] ?? null,
                            'note_type' => $item['note_type'] ?? null,
                            'account_id' => $item['account_id'] ?? null,
                            'group_id' => $item['group_id'] ?? null,
                        ]
                    );
                },
                $this->srcData['_embedded'][$this->getResponseCode()] ?? []
            )
        );
    }

    protected function getClassModel()
    {
        return Note::class;
    }

    protected function getResponseCode(): string
    {
        return 'notes';
    }
}
