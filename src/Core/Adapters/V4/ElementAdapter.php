<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.16
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4;

use Anemone\Contracts\BeAdapter;
use Anemone\Core\Adapters\V4\Base\BaseModel;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Element;

class ElementAdapter extends BaseAdapter implements BeAdapter
{
    use BaseModel;

    /** get collect of models
     * @return Collection
     * @throws \Exception
     * */
    public function getData()
    {
        $class = $this->getClassModel();
        return new Collection(
            array_map(
                function ($item) use ($class) {
                    return new $class(
                        $this->modelInstance,
                        [
                            'id' => $item['id'],
                            'name' => $item['name'],
                            'created_by' => $item['created_by'] ?? null,
                            'updated_by' => $item['updated_by'] ?? null,
                            'created_at' => $item['created_at'] ?? null,
                            'updated_at' => $item['updated_at'] ?? null,
                            'is_deleted' => $item['is_deleted'] ?? null,
                            'catalog_id' => $item['catalog_id'] ?? null,
                            'custom_fields_values' => $this->injectCustomFields($item['custom_fields_values'] ?? []),
                            'account_id' => $item['account_id'] ?? null,
                        ]
                    );
                },
                $this->srcData['_embedded'][$this->getResponseCode()] ?? []
            )
        );
    }

    public function mergeInsertResponse($collect)
    {
        array_map(
            function ($item, $append) {
                $item->id = $append['id'];
                $item->catalog_id = $append['catalog_id'];
                return null;
            },
            $collect->toArray(),
            $this->srcData['_embedded'][$this->getResponseCode()]
        );

        return $collect;
    }

    protected function getClassModel()
    {
        return Element::class;
    }

    protected function getResponseCode(): string
    {
        return 'elements';
    }
}
