<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4;

use Anemone\Contracts\BeAdapter;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Event;

class EventAdapter extends BaseAdapter implements BeAdapter
{

    /** get collect of models
     * @return Collection
     * */
    public function getData()
    {
        $class = $this->getClassModel();
        return new Collection(
            array_map(function ($item) use ($class) {
                unset($item['_links']);
                return new $class($this->modelInstance, $item);
            }, $this->srcData['_embedded'][$this->getResponseCode()] ?? [])
        );
    }

    public function getSelectRequest()
    {
        $arrayQuery = parent::getSelectRequest();

        $arrayQuery['filter']['id'] = $arrayQuery['id'] ?? null;
        if (isset($arrayQuery['filter']['date_create']['from'])) {
            $arrayQuery['filter']['created_at'] = $arrayQuery['filter']['date_create']['from'];
            if (isset($arrayQuery['filter']['date_create']['to'])) {
                $arrayQuery['filter']['created_at'] .= ',' . $arrayQuery['filter']['date_create']['to'];
            }
        }
        $arrayQuery['filter']['entity'] = isset($arrayQuery['filter']['entity']) ? join(',', $arrayQuery['filter']['entity']) : null;
        $arrayQuery['filter']['entity_id'] = isset($arrayQuery['filter']['entity_id']) ? join(',', $arrayQuery['filter']['entity_id']) : null;
        $arrayQuery['filter']['type'] = isset($arrayQuery['filter']['type']) ? join(',', $arrayQuery['filter']['type']) : null;

        return $arrayQuery;
    }

    protected function getClassModel()
    {
        return Event::class;
    }

    protected function getResponseCode(): string
    {
        return 'events';
    }
}
