<?php
/**
 *
 * @package   Anemone
 * @category  Adapters
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Adapters\V4;

use Anemone\Contracts\BeAdapter;
use Anemone\Core\Adapters\V4\Base\BaseModel;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Customer;

class CustomerAdapter extends BaseAdapter implements BeAdapter
{
    use BaseModel;

    /** get collect of models
     * @return Collection
     * @throws \Exception
     * */
    public function getData()
    {
        $class = $this->getClassModel();
        return new Collection(
            array_map(function ($item) use ($class) {
                return new $class(
                    $this->modelInstance,
                    [
                        'id' => $item['id'],
                        'account_id' => $item['account_id'],
                        'name' => $item['name'],
                        'next_price' => $item['next_price'],
                        'next_date' => $item['next_date'],
                        'responsible_user_id' => $item['responsible_user_id'] ?? null,
                        'status_id' => $item['status_id'] ?? null,
                        'periodicity' => $item['periodicity'] ?? null,
                        'created_by' => $item['created_by'] ?? null,
                        'created_at' => $item['created_at'] ?? null,
                        'updated_at' => $item['updated_at'] ?? null,
                        'updated_by' => $item['updated_by'] ?? null,
                        'closest_task_at' => $item['closest_task_at'] ?? null,
                        'is_deleted' => $item['is_deleted'] ?? null,
                        'ltv' => $item['ltv'] ?? null,
                        'purchases_count' => $item['purchases_count'] ?? null,
                        'average_check' => $item['average_check'] ?? null,
                        'custom_fields_values' => $this->injectCustomFields($item['custom_fields_values'] ?? []),
                        '_embedded' => $item['_embedded'] ?? null,
                        // tags, segments, companies, contacts
                    ]
                );
            }, $this->srcData['_embedded'][$this->getResponseCode()] ?? [])
        );
    }

    protected function getClassModel()
    {
        return Customer::class;
    }

    protected function getResponseCode(): string
    {
        return 'customers';
    }
}
