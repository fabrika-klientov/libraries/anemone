<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.19
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Helpers;

use Anemone\Contracts\BeCustomField;
use Anemone\Models\CF\MultiTextCustomField;
use Illuminate\Support\Str;

trait HavePhoneEmail
{
    /**
     * @param string $phone
     * @return \Anemone\Core\Collection\Collection
     * */
    public function findByPhone(string $phone)
    {
        $phone = (string)preg_replace('/[^+\d]/', '', $phone);
        $short = $this->getPhoneWithoutCode($phone);

        $this->query($short);
        /**
         * @var \Anemone\Core\Collection\Collection $collect
         * */
        $collect = $this->get();

        return $collect->filter(function ($entity) use ($phone) {
            /**
             * @var MultiTextCustomField|null $phoneCF
             * */
            $phoneCF = $entity
                ->cf(function (BeCustomField $customField) {
                    if ($customField instanceof MultiTextCustomField) {
                        $fieldName = Str::upper($customField->name);

                        return $fieldName == 'ТЕЛЕФОН' || $fieldName == 'PHONE';
                    }

                    return false;
                })
                ->first();

            return isset($phoneCF) ? collect($phoneCF->values)->some(function ($item) use ($phone) {

                return array_reduce(range(9, Str::length($phone)), function ($result, $index) use ($item, $phone) {
                    $short = $this->getPhoneWithoutCode($phone, -((int)$index));
                    $value = preg_replace('/[^+\d]/', '', $item['value']);

                    return $result && (Str::length($value) >= $index ? (stripos($value, $short) !== false) : $result);
                }, true);
            }) : false;
        });
    }

    /**
     * @param string $email
     * @return \Anemone\Core\Collection\Collection
     * */
    public function findByEmail(string $email)
    {
        $email = trim($email);
        $this->query($email);

        /**
         * @var \Anemone\Core\Collection\Collection $collect
         * */
        $collect = $this->get();

        return $collect->filter(function ($entity) use ($email) {
            /**
             * @var MultiTextCustomField|null $emailCF
             * */
            $emailCF = $entity
                ->cf(function (BeCustomField $customField) {
                    if ($customField instanceof MultiTextCustomField) {
                        return Str::upper($customField->name) == 'EMAIL';
                    }

                    return false;
                })
                ->first();

            return isset($emailCF) ? collect($emailCF->values)->some(function ($item) use ($email) {
                return stripos($item['value'], $email) !== false;
            }) : false;
        });
    }

    /**
     * @param string $phone
     * @param int $start
     * @return string
     * */
    protected function getPhoneWithoutCode(string $phone, $start = -9)
    {
        return substr($phone, $start);
    }
}