<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Helpers;

trait WithCount
{

    /** get count models (helper, need override)
     * @return array
     */
    protected function countData()
    {
        $keySelect = '_select';
        $beforeVersion = $this->currentVersion;
        $this->setVersion('_');

        $path = $this->getPath($keySelect);

        $result = $this->client
            ->getQueryService()
            ->{$this->getMethod($keySelect)}(
                $path,
                [],
                ['headers' => ['X-Requested-With' => 'XMLHttpRequest']]
            );

        $this->setVersion($beforeVersion);

        return json_decode($result, true);
    }
}
