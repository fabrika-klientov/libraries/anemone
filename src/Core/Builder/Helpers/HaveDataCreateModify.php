<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Helpers;

/**
 * @deprecated
 * */
trait HaveDataCreateModify
{
    /** filter for date create
     * @deprecated
     * @param string $from
     * @param string $to
     * @return $this
     * */
    public function dateCreate($from, $to)
    {
        $this->builder->where('filter', [
            'date_create' => [
                'from' => $from,
                'to' => $to,
            ],
        ]);
        return $this;
    }

    /** filter for date modify
     * @deprecated
     * @param string $from
     * @param string $to
     * @return $this
     * */
    public function dateModify($from, $to)
    {
        $this->builder->where('filter', [
            'date_modify' => [
                'from' => $from,
                'to' => $to,
            ],
        ]);
        return $this;
    }
}
