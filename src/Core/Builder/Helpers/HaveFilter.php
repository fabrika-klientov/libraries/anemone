<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Helpers;

/**
 * @deprecated
 * */
trait HaveFilter
{
    /** if have access - patching all https://www.amocrm.ru/developers/content/crm_platform/filters
     * @deprecated
     * @param string $name
     * @return $this
     * */
    public function filterName($name)
    {
        $this->builder->where('filter[name]', $name);
        return $this;
    }

    /**
     * @param string $type
     * @param mixed $value
     * @return $this
     */
    public function filter(string $type, $value)
    {
        $this->builder->where('filter', [
            $type => $value,
        ]);
        return $this;
    }
}
