<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Helpers;

trait HavePipelineStatus
{
    /** pipeline -> statuses
     * @param array $params
     * @todo
     * [
     *  [
     *      'pipeline' => 12345,
     *      'statuses' => [11111, 22222, 33333],
     *  ],
     *  [
     *      'pipeline' => 54321,
     *      'statuses' => [44444, 55555],
     *  ],
     * ]
     * @return $this
     * */
    public function pipe(array $params)
    {
        $this->builder->where('filter', [
            'pipe' => array_reduce($params, function ($result, $item) {
                $result[$item['pipeline']] = $item['statuses'];
                return $result;
            }, []),
        ]);
        return $this;
    }
}
