<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Helpers;

/**
 * @deprecated
 * */
trait HaveTasks
{
    /** tasks
     * @deprecated
     * @return $this
     * */
    public function noTasks()
    {
        $this->builder->where('filter', [
            'tasks' => 1,
        ]);
        return $this;
    }

    /** tasks unfulfilled
     * @deprecated
     * @return $this
     * */
    public function unTasks()
    {
        $this->builder->where('filter', [
            'tasks' => 2,
        ]);
        return $this;
    }
}
