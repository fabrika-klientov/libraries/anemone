<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Helpers;

trait HaveId
{
    /** id(s)
     * @param int|array $id
     * @return $this
     * */
    public function find($id)
    {
        $this->builder->where('id', $id);
        return $this;
    }
}
