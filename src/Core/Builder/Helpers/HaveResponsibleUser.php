<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Helpers;

/**
 * @deprecated
 * */
trait HaveResponsibleUser
{
    /** filter for responsible users
     * @param int|array $id
     * @return $this
     * */
    public function responsibleUser($id)
    {
        $this->builder->where('responsible_user_id', $id);
        return $this;
    }
}
