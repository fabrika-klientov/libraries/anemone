<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.31
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Helpers;


trait HavePipeline
{
    /**
     * @param int $id
     * @return $this
     * */
    public function status($id)
    {
        $this->builder->where('pipeline_id', $id);
        return $this;
    }
}