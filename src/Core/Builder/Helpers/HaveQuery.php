<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Helpers;

trait HaveQuery
{
    /** search in custom fields
     * @deprecated
     * @param mixed $search
     * @return $this
     * */
    public function query($search)
    {
        $this->builder->where('query', $search);
        return $this;
    }
}
