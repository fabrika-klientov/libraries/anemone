<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Helpers;

/**
 * @deprecated
 * */
trait HaveActive
{
    /** active entities
     * @param bool $status
     * @return $this
     * */
    public function active(bool $status = true)
    {
        $this->builder->where('filter', [
            'active' => $status ? 1 : 0,
        ]);
        return $this;
    }
}
