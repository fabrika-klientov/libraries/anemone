<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Helpers;

trait HaveWith
{
    /**
     * @param string|array $withData
     * @return $this
     * */
    public function with($withData)
    {
        $this->builder->where('with', is_array($withData) ? join(',', $withData) : $withData);
        return $this;
    }
}
