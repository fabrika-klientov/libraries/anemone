<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Helpers;

trait HaveLimits
{

    /** limit
     * @param int $count
     * @return $this
     */
    public function limit(int $count)
    {
        $this->builder->where('limit', $count);
        return $this;
    }

    /** page
     * @param int $page
     * @return $this
     */
    public function page(int $page)
    {
        $this->builder->where('page', $page);
        return $this;
    }
}
