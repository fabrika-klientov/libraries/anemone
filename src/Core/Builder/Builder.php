<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder;

class Builder
{
    /**
     * @var array $data
     * */
    protected $data;

    /**
     * @param array $whereData
     * @return void
     * */
    public function __construct(array $whereData = null)
    {
        $this->data = $whereData ?? [];
    }

    /** where filter
     * @param string $key
     * @param mixed $value
     * @return $this
     * */
    public function where($key, $value)
    {
        if (isset($this->data[$key]) && $key == 'filter') {
            $this->merge($this->data[$key], $value);
        } else {
            $this->data[$key] = $value;
        }

        return $this;
    }

    /** clear filter
     * @return $this
     * */
    public function clear()
    {
        $this->data = [];
        return $this;
    }

    /** get filter
     * @return array
     * */
    public function getResult()
    {
        return $this->data;
    }

    /** if has id query params
     * @return bool
     * */
    public function hasID()
    {
        return isset($this->data['id']);
    }

    /**
     * @param array $array
     * @param array $mergeArray
     * @return void
     * */
    protected function merge(&$array, $mergeArray)
    {
        $array = array_merge_recursive($array, $mergeArray);
    }
}
