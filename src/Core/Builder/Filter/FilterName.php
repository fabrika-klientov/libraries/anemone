<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.11.27
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Filter;

/**
 * @property \Anemone\Core\Builder\Builder $builder
 * */
trait FilterName
{
    /**
     * @param string|string[] $name
     * @return $this
     */
    public function filterName($name)
    {
        $this->builder->where('filter[name]', $name);

        return $this;
    }
}
