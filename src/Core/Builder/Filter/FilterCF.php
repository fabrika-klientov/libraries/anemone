<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.11.27
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Filter;

/**
 * @property \Anemone\Core\Builder\Builder $builder
 * */
trait FilterCF
{
    /** TODO: to work other types of custom fields
     * @param int $id
     * @param mixed $value
     * @return $this
     */
    public function filterCF($id, $value)
    {
        $this->builder->where("filter[custom_fields_values][$id]", $value);

        return $this;
    }
}
