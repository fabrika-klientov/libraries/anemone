<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.11.27
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Filter;

/**
 * @property \Anemone\Core\Builder\Builder $builder
 * */
trait FilterResponsible
{
    /**
     * @param int|int[] $user
     * @return $this
     */
    public function filterResponsible($user)
    {
        $this->builder->where('filter[responsible_user_id]', $user);

        return $this;
    }
}
