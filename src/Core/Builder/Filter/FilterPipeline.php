<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.11.27
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Filter;

/**
 * @property \Anemone\Core\Builder\Builder $builder
 * */
trait FilterPipeline
{
    /** TODO: analise
     * dont work (27.11.2020)
     * @param int|int[] $pipeline
     * @return $this
     */
//    public function filterPipeline($pipeline)
//    {
//        $pipeline = is_array($pipeline) ? $pipeline : [$pipeline];
//        foreach ($pipeline as $item) {
//            $this->builder->where('filter', ['pipeline_id' => [$item]]);
//        }
//
//        return $this;
//    }
}
