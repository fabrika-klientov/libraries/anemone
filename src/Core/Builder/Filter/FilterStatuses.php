<?php
/**
 *
 * @package   Anemone
 * @category  Builders
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.11.27
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Builder\Filter;

/**
 * @property \Anemone\Core\Builder\Builder $builder
 * */
trait FilterStatuses
{
    /**
     * @param int $pipeline
     * @param int|int[] $status
     * @return $this
     */
    public function filterStatus($pipeline, $status)
    {
        $status = is_array($status) ? $status : [$status];
        foreach ($status as $item) {
            $this->builder->where('filter', ['statuses' => [['pipeline_id' => $pipeline, 'status_id' => $item]]]);
        }

        return $this;
    }
}
