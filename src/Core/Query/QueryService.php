<?php
/**
 *
 * @package   Anemone
 * @category  Query
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Query;

use Anemone\Contracts\BeHttpClient;
use Anemone\Core\Auth\AuthService;
use Anemone\Core\Helpers\Logging;
use Anemone\Exceptions\BadResponseException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Promise;
use Psr\Http\Message\ResponseInterface;

class QueryService implements BeHttpClient
{
    use Logging;

    /**
     * @var AuthService $authService
     * */
    private $authService;
    /**
     * @var Client $httpClient
     * */
    private $httpClient;

    /** Query session
     * @param AuthService $authService
     * @return void
     * */
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
        $this->httpClient = new Client();
    }

    /** public GET method request
     * @param string $link
     * @param array $query
     * @param array $guzzleOptions
     * @param bool $withSessionData
     * @param bool $isResponseInterface
     * @param bool $async
     * @return ResponseInterface|\GuzzleHttp\Promise\PromiseInterface|mixed
     * @throws BadResponseException
     * @throws \Anemone\Exceptions\InvalidVersionException
     */
    public function get(
        string $link,
        array $query = [],
        array $guzzleOptions = [],
        bool $withSessionData = true,
        bool $isResponseInterface = false,
        bool $async = false
    ) {
        $response = $this->request(
            'GET',
            $link,
            array_merge(
                $guzzleOptions,
                [
                    'query' => $query,
                ]
            ),
            $withSessionData,
            $async
        );

        return $isResponseInterface || $async ? $response : $this->getContents($response);
    }

    /** public POST method request
     * @param string $link
     * @param array $data
     * @param array $guzzleOptions
     * @param bool $withSessionData
     * @param bool $isResponseInterface
     * @param bool $async
     * @return ResponseInterface|\GuzzleHttp\Promise\PromiseInterface|mixed
     * @throws BadResponseException
     * @throws \Anemone\Exceptions\InvalidVersionException
     */
    public function post(
        string $link,
        array $data,
        array $guzzleOptions = [],
        bool $withSessionData = true,
        bool $isResponseInterface = false,
        bool $async = false
    ) {
        $response = $this->request(
            'POST',
            $link,
            array_merge(
                $guzzleOptions,
                empty($data) ? [] : [
                    'json' => $data,
                ]
            ),
            $withSessionData,
            $async
        );

        return $isResponseInterface || $async ? $response : $this->getContents($response);
    }

    /** public PATCH method request
     * @param string $link
     * @param array $data
     * @param array $guzzleOptions
     * @param bool $withSessionData
     * @param bool $isResponseInterface
     * @param bool $async
     * @return ResponseInterface|\GuzzleHttp\Promise\PromiseInterface|mixed
     * @throws BadResponseException
     * @throws \Anemone\Exceptions\InvalidVersionException
     */
    public function patch(
        string $link,
        array $data,
        array $guzzleOptions = [],
        bool $withSessionData = true,
        bool $isResponseInterface = false,
        bool $async = false
    ) {
        $response = $this->request(
            'PATCH',
            $link,
            array_merge(
                $guzzleOptions,
                empty($data) ? [] : [
                    'json' => $data,
                ]
            ),
            $withSessionData,
            $async
        );

        return $isResponseInterface || $async ? $response : $this->getContents($response);
    }

    /** public DELETE method request
     * @param string $link
     * @param array $query
     * @param array $guzzleOptions
     * @param bool $withSessionData
     * @param bool $isResponseInterface
     * @param bool $async
     * @return ResponseInterface|\GuzzleHttp\Promise\PromiseInterface|mixed
     * @throws BadResponseException
     * @throws \Anemone\Exceptions\InvalidVersionException
     */
    public function delete(
        string $link,
        array $query = [],
        array $guzzleOptions = [],
        bool $withSessionData = true,
        bool $isResponseInterface = false,
        bool $async = false
    ) {
        $response = $this->request(
            'DELETE',
            $link,
            array_merge(
                $guzzleOptions,
                [
                    'query' => $query,
                ]
            ),
            $withSessionData,
            $async
        );

        return $isResponseInterface || $async ? $response : $this->getContents($response);
    }

    /** get full url
     * @param string $link
     * @param string $domainWithoutHttp
     * @param bool $isAccountDomain
     * @return string
     * */
    public function getUrl(string $link, $domainWithoutHttp = null, $isAccountDomain = false)
    {
        if (preg_match('/^https?:\/\/.*/', $link)) {
            return $link;
        }
        $link = mb_substr($link, 0, 1) == '/' ? $link : ('/' . $link);
        return self::_protocol . ($domainWithoutHttp ?? $this->authService->getDomain()) . $link;
    }

    /**
     * @param \Closure $closure (returned array of promises)
     * @return array
     * @throws \Exception
     * */
    public function merge($closure)
    {
        return $this->asyncRequest($closure);
    }

    /**
     * @inheritDoc
     * */
    public function getGuzzle()
    {
        return $this->httpClient;
    }


    /** smart request
     * @param string $method
     * @param string $link
     * @param array $options
     * @param bool $withSessionData
     * @param bool $async
     * @param bool $isStopped
     * @return ResponseInterface
     * @throws BadResponseException
     * @throws \Anemone\Exceptions\InvalidVersionException
     */
    protected function request(
        string $method,
        string $link,
        array $options = [],
        bool $withSessionData = true,
        bool $async = false,
        bool $isStopped = false
    ) {
        // push auth data in request params (headers)
        if ($withSessionData) {
            $options = $this->injectSessionData($options, $isStopped);
        }

        $startTime = time();

        try {
            $response = $this->httpClient->{$async ? 'requestAsync' : 'request'}(
                $method,
                $this->getUrl($link),
                $options
            );

            // async not available for logging
            if (!$async) {
                $this->loggingSuccess($method, $link, $options, $response, $startTime);
                $response->getBody()->rewind();
            }

            return $response;
        } catch (ClientException $clientException) {
            $this->loggingError($method, $link, $options, $clientException, $startTime);

            if ($withSessionData && $clientException->getCode() == 401 && !$isStopped) {
                return $this->request($method, $link, $options, $withSessionData, $async, true);
            }

            throw $clientException;
        }
    }

    /** smart async request
     * @param \Closure $closure (returned array of promises)
     * @param bool $isStopped
     * @return array
     * @throws \Exception
     * */
    protected function asyncRequest($closure, bool $isStopped = false)
    {
        try {
            if (!$isStopped) {
                $this->get($this->getUrl(self::_ping, $this->authService->getDomain()), [], []);
            }

            $response = Promise\unwrap($closure());

            array_walk(
                $response,
                function (ResponseInterface $item) {
                    $item->getBody()->rewind();
                }
            );

            return $response;
        } catch (ClientException $clientException) {
            if ($clientException->getCode() == 401 && !$isStopped) {
                return $this->asyncRequest($closure, true);
            }

            throw $clientException;
        } catch (\Throwable $throwable) {
            $logger = aLogger();
            $message = 'Fatal error in async request. ' . $throwable->getMessage();
            if (isset($logger)) {
                $logger->error('/// ' . $message, ['lib' => 'anemone']);
            }

            throw new BadResponseException($message);
        }
    }

    /** helper for get body data
     * @param ResponseInterface $response
     * @return mixed
     * */
    protected function getContents(ResponseInterface $response = null)
    {
        return isset($response) ? $response->getBody()->getContents() : null;
    }

    /**
     * @param array $options
     * @param bool $force
     * @return array
     */
    protected function injectSessionData(array $options, bool $force = false)
    {
        $decorator = $this->authService->getDecorator($this);
        return array_replace_recursive($options, $force ? $decorator->auth() : $decorator->getSessionData());
    }
}
