<?php
/**
 *
 * @package   Anemone
 * @category  Cache
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Cache;

use Anemone\Contracts\BeCollection;
use Anemone\Contracts\BeBaseModel;
use Anemone\Core\Builder\Builder;
use Anemone\Core\Helpers\ParseEntityWithId;

class CacheService
{
    use ParseEntityWithId;

    /**
     * @var CacheDriver $cacheDriver
     * */
    private $cacheDriver;

    /**
     * @var string[] $force
     * */
    private $force = [
        'account',
        'catalog',
        'tag',
        'cf_lead',
        'cf_customer',
        'cf_contact',
        'cf_company',
        'cf_element',
    ];

    /**
     * @param CacheDriver $cacheDriver
     * @return void
     * */
    public function __construct(CacheDriver $cacheDriver)
    {
        $this->cacheDriver = $cacheDriver;
    }

    /** get (only is have id)
     * @param Builder $builder
     * @param string $version
     * @param string $entity
     * @return \Anemone\Contracts\BeCollection|null
     * */
    public function get(Builder $builder, $version, $entity)
    {
        $collection = $this->cacheDriver->read($entity, $version);
        if (is_null($collection)) {
            return null;
        }

        if (in_array(self::getParsedEntityName($entity), $this->force) && $collection->isNotEmpty()) {
            return $collection;
        }

        if ($builder->hasID()) {
            $id = $builder->getResult()['id'];
            $id = is_array($id) ? $id : [$id];

            $collection = $collection->filter(function (BeBaseModel $data) use ($id) {
                return in_array($data->id, $id);
            });

            if ($collection->isNotEmpty() && $collection->count() == count($id)) {
                return $collection;
            }
        }

        return null;
    }

    /** set collect in cache
     * @param \Anemone\Contracts\BeCollection $collection
     * @param string $version
     * @param string $entity
     * @param bool $inverse
     * @return \Anemone\Contracts\BeCollection
     * @throws \Exception
     * */
    public function set(BeCollection $collection, $version, $entity, bool $inverse = false)
    {
        if ($collection->isNotEmpty()) {
            $this->cacheDriver->write($entity, $version, $collection, $inverse);
        }

        return $collection;
    }

    /** clear for id(s)
     * @param array $id
     * @param string $version
     * @param string $entity
     * @return void
     * */
    public function cleanID($version, $entity, array $id = null)
    {
        $this->cacheDriver->clearForID($entity, $version, $id);
    }
}
