<?php
/**
 *
 * @package   Anemone
 * @category  Cache
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Cache;

use Anemone\Client;
use Anemone\Contracts\BeCollection;
use Anemone\Contracts\BeBaseModel;
use Anemone\Core\Collection\Collection;
use Anemone\Core\Helpers\ParseEntityWithId;
use Anemone\Exceptions\InvalidDataException;
use Anemone\Models\CF\BaseCustomField;

class CacheDriver
{
    use ParseEntityWithId;

    private $client;
    private $cachePath;

    /**
     * @param Client $client
     * @return void
     * */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->cachePath = aConfig('cache.path');
        $this->storeCacheDir();
    }

    /** push in cache
     * @param string $type
     * @param string $version
     * @param BeCollection|BeBaseModel|mixed $object
     * @param bool $inverse
     * @return bool
     * @throws InvalidDataException
     * */
    public function write(string $type, $version, $object, bool $inverse = false)
    {
        if ($object instanceof BeBaseModel || $object instanceof BeCollection) {
            $cacheConfig = aConfig('cache');
            if (!$cacheConfig['power']) {
                return false;
            }

            if ($object instanceof BeBaseModel) {
                $object = new Collection([$object]);
            }

            $file = $this->getFile();
            if (file_exists($file) && !is_writable($file)) {
                throw new \RuntimeException('FIle [' . $file . '] is not writable. Permission denied.');
            }

            $expTime = time() + $cacheConfig['timings'][self::getParsedEntityName($type)]; // in future set in 'other'
            $result = file_exists($file) ? json_decode((string)file_get_contents($file), true) : [];
            if (empty($result)) $result = [];

            if (!isset($result[$type]) && !$inverse) {
                $result[$type] = [
                    $version => $this->convertCollectToCacheData($object, $expTime),
                ];
            } elseif (isset($result[$type][$version])) {
                $collect = $this->getFromJSON($result, $type, $version)
                    ->filter(function (CacheData $data) { // filter Expires
                        return $data->getExpires() > time();
                    })
                    ->map(function (CacheData $data) use ($object, $expTime, $inverse) { // replace old data
                        $id = $data->getID();
                        $one = $object->first(function ($item) use ($id) {
                            return $item->id == $id;
                        });

                        if (isset($one)) {
                            if ($inverse) { // delete in collect cache
                                return null;
                            }

                            return new CacheData($one, $expTime);
                        }

                        return $data;
                    })
                    ->filter(function ($item) { // filter deleting models (if null)
                        return isset($item);
                    });

                if (!$inverse) {
                    $object->each(function ($item) use ($collect, $expTime) { // push new data
                        if (!$collect->some(function (CacheData $data) use ($item) {
                            return $data->getID() == $item->id;
                        })) {
                            $collect->push(new CacheData($item, $expTime));
                        }
                    });
                }

                $result[$type][$version] = $collect;
            } else {
                if (!$inverse) {
                    $result[$type][$version] = $this->convertCollectToCacheData($object, $expTime);
                }
            }

            $fp = fopen($file, "w");
            if ($fp) {
                $status = fwrite($fp, (string)json_encode($result));
                fclose($fp);
            }

            return (bool)($status ?? false);
        }

        throw new InvalidDataException('Instance $object should be [Anemone\Contracts\BeCollection] or [Anemone\Contracts\BeModel]');
    }

    /** get from cache
     * @param string $type
     * @param string $version
     * @return BeCollection|null
     * */
    public function read(string $type, $version)
    {
        $cacheConfig = aConfig('cache');
        if (!$cacheConfig['power']) {
            return null;
        }

        $file = $this->getFile();
        if (file_exists($file)) {
            $result = json_decode((string)file_get_contents($file), true);

            if (isset($result[$type], $result[$type][$version])) {
                return $this->getFromJSON($result, $type, $version)
                    ->filter(function (CacheData $item) {
                        return $item->getExpires() > time();
                    })
                    ->map(function (CacheData $item) {
                        return $item->getModel();
                    });
            }
        }

        return new Collection();
    }

    /** clear data for id(s)
     * @param string $type
     * @param string $version
     * @param array $id
     * @return void
     * */
    public function clearForID(string $type, $version, array $id = null)
    {
        $file = $this->getFile();
        if (file_exists($file) && !is_writable($file)) {
            throw new \RuntimeException('FIle [' . $file . '] is not writable. Permission denied.');
        }

        $result = file_exists($file) ? json_decode((string)file_get_contents($file), true) : [];
        if (empty($result)) $result = [];

        if (isset($result[$type][$version])) {
            if (is_null($id)) {
                $result[$type][$version] = new Collection();
            } elseif (empty($id)) {
                return;
            } else {
                $result[$type][$version] = $this->getFromJSON($result, $type, $version)
                    ->filter(function (CacheData $data) use ($id) { // filter Expires and id(s)
                        return $data->getExpires() > time() && !in_array($data->getID(), $id);
                    });
            }

            $fp = fopen($file, "w");
            if ($fp) {
                $status = fwrite($fp, (string)json_encode($result));
                fclose($fp);
            }
        }
    }

    /**
     * @param BeCollection $collection
     * @param int $expTime
     * @return BeCollection
     * */
    protected function convertCollectToCacheData(BeCollection $collection, $expTime)
    {
        return $collection->map(function ($item) use ($expTime) {
            return new CacheData($item, $expTime);
        });
    }

    /** convert in Collection
     * @param array $result
     * @param string $type
     * @param string $version
     * @return BeCollection
     * */
    protected function getFromJSON($result, $type, $version)
    {
        return new Collection(array_map(function ($item) {
            $modelClass = $item['class'];
            if (is_subclass_of($modelClass, BaseCustomField::class)) {
                /** for phpstan
                 * @var mixed $oneAttrClass
                 * */
                $oneAttrClass = $modelClass;
                $model = new $oneAttrClass($item['model']);
            }

            return new CacheData($model ?? new $modelClass(null, $item['model']), $item['expires']);
        }, $result[$type][$version]));
    }

    /** file for cache
     * @return string
     * */
    protected function getFile()
    {
        $file = $this->cachePath . DIRECTORY_SEPARATOR . $this->client->getAuthService()->getDomain() . '.json';
        if (file_exists($file)) {
            return $file;
        }

        if ($fs = fopen($file, 'x')) {
            fclose($fs);
            chmod($file, 0664);
        }

        return $file;
    }

    /** path for cache
     * @return void
     * @throws \RuntimeException
     * */
    protected function storeCacheDir()
    {
        if (!file_exists($this->cachePath)) {
            if (!mkdir($this->cachePath, 0775, true)) {
                throw new \RuntimeException('Did\'t stored path for cache. Permission denied.');
            }
        }
    }
}
