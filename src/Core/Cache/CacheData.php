<?php
/**
 *
 * @package   Anemone
 * @category  Cache
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Cache;

use Anemone\Contracts\BeBaseModel;

class CacheData implements \JsonSerializable
{
    /**
     * @var array $data
     * */
    private $data;

    /**
     * @param BeBaseModel $model
     * @param int $expires
     * @return void
     * */
    public function __construct(BeBaseModel $model, int $expires)
    {
        $this->data = [
            'model' => $model,
            'expires' => $expires,
            'id' => $model->id,
            'class' => get_class($model),
        ];
    }

    /**
     * @return int
     * */
    public function getExpires()
    {
        return $this->data['expires'];
    }

    /**
     * @return BeBaseModel
     * */
    public function getModel()
    {
        return $this->data['model'];
    }

    /**
     * @return int
     * */
    public function getID()
    {
        return $this->data['id'];
    }

    /**
     * @return string
     * */
    public function getModelClass()
    {
        return $this->data['class'];
    }

    /**
     * @return array
     * */
    public function jsonSerialize()
    {
        return $this->data;
    }
}
