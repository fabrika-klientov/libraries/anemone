<?php
/**
 *
 * @package   Anemone
 * @category  Helpers
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.07.03
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Helpers;

use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

trait Logging
{

    /**
     * @param string $method
     * @param string $link
     * @param array $options
     * @param ResponseInterface $response
     * @param int $startTime
     * @return void
     */
    protected function loggingSuccess(
        string $method,
        string $link,
        array $options,
        ResponseInterface $response,
        int $startTime
    ) {
        aLogger(
            function (LoggerInterface $logger) use ($method, $link, $options, $response, $startTime) {
                $resData = $this->getContents($response);

                $logger->info(
                    'SUCCESS:: CODE [200] V [4] METHOD [' . $method . '] || URL [' . $this->getUrl($link) . ']',
                    [
                        'lib' => 'anemone',
                        'status' => true,
                        'request' => (string)json_encode($options, JSON_UNESCAPED_UNICODE),
                        'request_url' => $this->getUrl($link),
//                        'request_headers' => $options['headers'] ?? [],
                        'request_method' => $method,
                        'response' => substr((string)json_encode($resData ?? '{}', JSON_UNESCAPED_UNICODE), 0, 10000),
                        'response_code' => $response->getStatusCode(),
                        'response_headers' => $response->getHeaders(),
                        'execute_time' => time() - $startTime,
                        'api' => 4,
                        'amocrm' => $this->authService->getDomain(),
                        'request_content_length' => strlen((string)json_encode($options)),
                        'response_content_length' => strlen($resData),
                    ]
                );
            }
        );
    }

    /** error response logging
     * @param string $method
     * @param string $link
     * @param array $options
     * @param ClientException $clientException
     * @param int $startTime
     * @return void
     */
    protected function loggingError(
        string $method,
        string $link,
        array $options,
        ClientException $clientException,
        int $startTime
    ) {
        aLogger(
            function (LoggerInterface $logger) use ($method, $link, $options, $clientException, $startTime) {
                $response = $clientException->getResponse();
                $resData = $this->getContents($response);

                $logger->warning(
                    'ERROR:: CODE [' . $clientException->getCode() . '] V [4] METHOD [' . $method . '] || URL [' . $this->getUrl($link) . ']',
                    [
                        'lib' => 'anemone',
                        'status' => false,
                        'request' => (string)json_encode($options, JSON_UNESCAPED_UNICODE),
                        'request_url' => $this->getUrl($link),
//                        'request_headers' => $options['headers'] ?? [],
                        'request_method' => $method,
                        'response' => substr((string)json_encode($resData ?? '{}', JSON_UNESCAPED_UNICODE), 0, 10000),
                        'response_code' => $response ? $response->getStatusCode() : 'null',
                        'response_headers' => $response ? $response->getHeaders() : [],
                        'execute_time' => time() - $startTime,
                        'api' => 4,
                        'amocrm' => $this->authService->getDomain(),
                        'request_content_length' => strlen((string)json_encode($options)),
                        'response_content_length' => strlen($resData),
                    ]
                );
            }
        );
    }
}
