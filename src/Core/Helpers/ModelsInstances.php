<?php
/**
 *
 * @package   Anemone
 * @category  Helpers
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 20.05.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Helpers;

use Illuminate\Support\Str;

/**
 * @property-read \Anemone\Models\Instances\LeadsInstance $leads
 * @property-read \Anemone\Models\Instances\CustomersInstance $customers
 * @property-read \Anemone\Models\Instances\ContactsInstance $contacts
 * @property-read \Anemone\Models\Instances\CompaniesInstance $companies
 * @property-read \Anemone\Models\Instances\TasksInstance $tasks
 * @property-read \Anemone\Models\Instances\IncomingLeadsInstance $incomingLeads
 * @property-read \Anemone\Models\Instances\AccountInstance $account
 * @property-read \Anemone\Models\Instances\CallsInstance $calls
 * @property-read \Anemone\Models\Instances\EventsInstance $events
 * @property-read \Anemone\Models\Instances\WebhooksInstance $webhooks
 * @property-read \Anemone\Models\Instances\UsersInstance $users
 * @property-read \Anemone\Models\Instances\CatalogsInstance $catalogs
 * */
trait ModelsInstances
{
    /**
     * @param string $name
     * @return mixed
     * */
    public function __get($name)
    {
        switch ($name) {
            case 'account':
            case 'leads':
            case 'customers':
            case 'contacts':
            case 'companies':
            case 'tasks':
            case 'incomingLeads':
            case 'calls':
            case 'events':
            case 'webhooks':
            case 'users':
            case 'catalogs':
                $class = 'Anemone\\Models\\Instances\\' . Str::ucfirst($name) . 'Instance';
                if (class_exists($class)) {
                    return new $class($this);
                }
        }

        return null;
    }
}
