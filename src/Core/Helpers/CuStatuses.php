<?php
/**
 *
 * @package   Anemone
 * @category  Helpers
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.19
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Helpers;

use Anemone\Core\Collection\Collection;
use Anemone\Models\StatusCu;

trait CuStatuses
{
    /**
     * @var string[] $supportStatusesEntities
     * */
    protected $supportStatusesEntities = [
        'customer',
    ];

    /**
     * @return Collection
     */
    public function statuses()
    {
        if (!in_array($this->entity(), $this->supportStatusesEntities)) {
            return new Collection();
        }

        $result = json_decode($this->client->getQueryService()->get(self::getPathOfStatuses('get')), true);

        return (new Collection($result['_embedded']['statuses'] ?? []))
            ->map(
                function ($item) {
                    unset($item['_links']);
                    return new StatusCu($item);
                }
            );
    }

    // in future

    /**
     * @param string $code
     * @return string
     */
    protected function getPathOfStatuses(string $code)
    {
        return aEntryData($this->currentVersion . '.customer_statuses.' . $code);
    }
}
