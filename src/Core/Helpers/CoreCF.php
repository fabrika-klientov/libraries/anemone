<?php
/**
 *
 * @package   Anemone
 * @category  Helpers
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.16
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Helpers;

use Anemone\Core\Collection\Collection;
use Anemone\Models\CF\CustomFieldService;
use Anemone\Models\GroupCF;

trait CoreCF
{
    /**
     * @param array $inject
     * @param bool $isGroup
     * @return Collection
     */
    private function _cf(array $inject = [], bool $isGroup = false)
    {
        if (!in_array($this->entity(), $this->supportCFEntities)) {
            return new Collection();
        }

        $cfService = new CustomFieldService();
        $cacheKey = 'cf_' . $this->entity();

        if (!$isGroup) {
            // from cache
            $cacheResult = $this->cacheService->get($this->builder, $this->currentVersion, $cacheKey);
            if (isset($cacheResult)) { // get cached data
                $result = ['_embedded' => ['custom_fields' => json_decode((string)json_encode($cacheResult), true)]];
            }
        }

        if (empty($result)) {
            $page = 1;
            while (true) {
                $tmp = json_decode(
                    $this->client->getQueryService()->get(self::getPathOfCF('get', $isGroup), ['page' => $page]),
                    true
                );

                if (empty($result)) { // first set
                    $result = $tmp;
                }

                if ($isGroup) { // is group skip
                    break;
                }

                if (empty($tmp['_page_count'])) {
                    break;
                }

                if ($tmp['_page_count'] >= $page) {
                    if ($page != 1) {
                        $result['_embedded']['custom_fields'] = array_merge(
                            $result['_embedded']['custom_fields'],
                            $tmp['_embedded']['custom_fields']
                        );
                    }

                    $page++;
                }

                if ($tmp['_page_count'] < $page) {
                    break;
                }
            }
        }

        if ($isGroup) {
            return (new Collection($result['_embedded']['custom_field_groups'] ?? []))
                ->map(
                    function ($item) {
                        unset($item['_links']);
                        return new GroupCF($item);
                    }
                );
        }

        $collect = $cfService->getByCollect($result['_embedded']['custom_fields'] ?? [], $inject);

        // to cache
        if (empty($cacheResult) && !empty($result['_embedded']['custom_fields'])) {
            $this->cacheService->set(
                $cfService->getByCollect($result['_embedded']['custom_fields']),
                $this->currentVersion,
                $cacheKey
            );
        }

        return $collect;
    }

    /**
     * @param Collection $collection
     * @param bool $isGroup
     * @return Collection
     */
    private function _saveCF(Collection $collection, bool $isGroup = false)
    {
        if ($collection->isEmpty()) {
            return $collection;
        }

        $cfService = new CustomFieldService();
        $cacheKey = 'cf_' . $this->entity();

        [$store, $update] = $collection->reduce(
            function (array $result, $item) {
                $result[isset($item->id) ? 1 : 0]->add($item);
                return $result;
            },
            [new Collection(), new Collection()]
        );

        $collect = new Collection();

        if ($store->isNotEmpty()) {
            $result = json_decode(
                $this->client->getQueryService()->post(self::getPathOfCF('add', $isGroup), $store->toArray()),
                true
            );
            $collect = $collect->merge(
                $isGroup
                    ? (new Collection($result['_embedded']['custom_field_groups'] ?? []))
                    ->map(
                        function ($item) {
                            return new GroupCF($item);
                        }
                    )
                    : $cfService->getByCollect($result['_embedded']['custom_fields'] ?? [])
            );
        }

        if ($update->isNotEmpty()) {
            $path = self::getPathOfCF('update', $isGroup);
            $collect = $collect->merge(
                $update->reduce(
                    function ($resulted, $item) use ($path, $cfService, $isGroup) {
                        $data = $item->jsonSerialize();
                        unset($data['id']);
                        $result = json_decode(
                            $this->client->getQueryService()->patch($path . '/' . $item->id, $data),
                            true
                        );

                        return $resulted->merge(
                            $isGroup
                                ? new Collection([new GroupCF($result)])
                                : $cfService->getByCollect([$result])
                        );
                    },
                    new Collection()
                )
            );
        }

        if (!$isGroup) {
            $this->cacheService->set($collect, $this->currentVersion, $cacheKey);
        }

        return $collect;
    }

    /**
     * @param Collection $collection
     * @param bool $isGroup
     * @return bool
     */
    private function _deleteCF(Collection $collection, bool $isGroup = false)
    {
        $deleted = $collection->filter(
            function ($item) {
                return isset($item->id);
            }
        );

        if ($deleted->isEmpty()) {
            return false;
        }

        $cacheKey = 'cf_' . $this->entity();
        $path = self::getPathOfCF('delete', $isGroup);

        $deleted->each(
            function ($item) use ($path) {
                $this->client->getQueryService()->delete($path . '/' . $item->id);
            }
        );

        if (!$isGroup) {
            $this->cacheService->set($deleted, $this->currentVersion, $cacheKey, true);
        }

        return true;
    }

    /**
     * @param string $code
     * @param bool $isGroup
     * @return string
     */
    protected function getPathOfCF(string $code, bool $isGroup)
    {
        $path = aEntryData($this->currentVersion . '.field.' . $code);
        $entity = self::adaptingEntitiesNames();

        return str_replace(
                ['{entity_type}', '{entity_id}'],
                [
                    $entity == 'company' ? 'companies' : ($entity . 's'),
                    isset($this->entityId) ? ('/' . $this->entityId) : ''
                ],
                $path
            )
            . ($isGroup ? '/groups' : '');
    }

    /**
     * @param string|null $name
     * @return string
     */
    private function adaptingEntitiesNames(string $name = null): string
    {
        $name = $name ?? $this->entity();
        $entity = isset($this->entityId) ? str_replace('_' . $this->entityId, '', $name) : $name;

        switch ($entity) {
            case 'element':
                return 'catalog';
            default:
                return $entity;
        }
    }
}
