<?php
/**
 *
 * @package   Anemone
 * @category  Helpers
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.16
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Helpers;

use Anemone\Core\Collection\Collection;

trait CFOnly
{
    use CoreCF;

    /**
     * @var string[] $supportCFEntities
     * */
    protected $supportCFEntities = [
        'lead',
        'customer',
        'contact',
        'company',
    ];

    /**
     * @param array $inject
     * @return Collection
     */
    public function cf(array $inject = [])
    {
        return self::_cf($inject);
    }

    /**
     * @param Collection $collection
     * @return Collection
     */
    public function saveCF(Collection $collection)
    {
        return self::_saveCF($collection);
    }

    /**
     * @param Collection $collection
     * @return bool
     */
    public function deleteCF(Collection $collection)
    {
        return self::_deleteCF($collection);
    }
}
