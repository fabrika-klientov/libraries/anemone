<?php
/**
 *
 * @package   Anemone
 * @category  Helpers
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.17
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Helpers;

use Anemone\Core\Collection\Collection;

trait Complex
{
    /**
     * @param Collection $collection
     * @return array|mixed
     */
    public function complex(Collection $collection)
    {
        if ($collection->isEmpty()) {
            return new Collection();
        }

        $storeData = $this->serviceAdapter->getInsertRequest($collection);

        $result = json_decode(
            $this->client->getQueryService()->post(self::getPathOfComplex(), $storeData),
            true
        );

        // inject ID(s)
        array_map(
            function ($item, $deep) {
                $item->id = $deep['id'];

                if (!empty($deep['contact_id']) && !empty($item->_embedded['contacts'])) {
                    $first = $this->getFirstIfExist($item->_embedded['contacts']);
                    if ($first && is_object($first)) {
                        $first->id = $deep['contact_id'];
                    }
                }

                if (!empty($deep['company_id']) && !empty($item->_embedded['companies'])) {
                    $first = $this->getFirstIfExist($item->_embedded['companies']);
                    if ($first && is_object($first)) {
                        $first->id = $deep['company_id'];
                    }
                }

                return null;
            },
            $collection->toArray(),
            $result
        );

        return $collection->values();
    }

    /**
     * @param mixed $data
     * @return mixed|null
     */
    protected function getFirstIfExist($data)
    {
        switch (true) {
            case $data instanceof Collection:
                return $data->first();
            case is_array($data):
                return $data[0];
            default:
                return null;
        }
    }

    /**
     * @return string
     */
    protected function getPathOfComplex()
    {
        $path = self::getPath('_update');
        return $path . "/complex";
    }
}
