<?php
/**
 *
 * @package   Anemone
 * @category  Helpers
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.17
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Helpers;

use Anemone\Core\Collection\Collection;
use Anemone\Exceptions\AnemoneException;
use Anemone\Models\Company;
use Anemone\Models\Contact;
use Anemone\Models\Customer;
use Anemone\Models\Element;
use Anemone\Models\Lead;

trait LinkEntities
{
    /**
     * @param \Anemone\Contracts\BeModel $model
     * @param Collection $collection
     * @param array $metadata
     * @return bool
     */
    public function link($model, Collection $collection, array $metadata = [])
    {
        if (empty($model) || empty($model->id) || $collection->isEmpty()) {
            return false;
        }

        $result = json_decode(
            $this->client->getQueryService()->post(
                self::getPathOfLinked($model->id),
                self::getPrepareLinkData($model, $collection, $metadata)->toArray()
            ),
            true
        );

        return isset($result['_embedded']['links']);
    }

    /** Collection of LinkedData::class
     * @param Collection $collection
     * @return bool
     * @throws AnemoneException
     */
    public function links(Collection $collection)
    {
        if ($collection->isEmpty()) {
            return false;
        }

        if (!$collection->some(
            function ($item) {
                return $item instanceof LinkedData;
            }
        )) {
            throw new AnemoneException('Bad linked data. Expected LinkedData::class');
        }

        $data = $collection
            ->reduce(
                function (Collection $result, LinkedData $item) {
                    return $result->merge($item->amoPreparedData());
                },
                new Collection()
            )
            ->values();

        $result = json_decode(
            $this->client->getQueryService()->post(self::getPathOfMassLinked(), $data->toArray()),
            true
        );

        return isset($result['_embedded']['links']);
    }

    /**
     * @param \Anemone\Contracts\BeModel $model
     * @param Collection $collection
     * @return bool
     */
    public function unlink($model, Collection $collection)
    {
        if (empty($model) || empty($model->id) || $collection->isEmpty()) {
            return false;
        }

        $this->client->getQueryService()->post(
            self::getPathOfLinked($model->id, 'unlink'),
            self::getPrepareLinkData($model, $collection)->toArray()
        );

        return true;
    }

    /** Collection of LinkedData::class
     * @param Collection $collection
     * @return bool
     * @throws AnemoneException
     */
    public function unlinks(Collection $collection)
    {
        if ($collection->isEmpty()) {
            return false;
        }

        if (!$collection->some(
            function ($item) {
                return $item instanceof LinkedData;
            }
        )) {
            throw new AnemoneException('Bad unlinked data. Expected LinkedData::class');
        }

        $data = $collection
            ->reduce(
                function (Collection $result, LinkedData $item) {
                    return $result->merge($item->amoPreparedData());
                },
                new Collection()
            )
            ->values();

        json_decode(
            $this->client->getQueryService()->post(self::getPathOfMassLinked('unlink'), $data->toArray()),
            true
        );

        return true;
    }

    /**
     * @param mixed $base
     * @param Collection $collection
     * @param array $metadataSrc
     * @return Collection
     */
    protected function getPrepareLinkData($base, Collection $collection, array $metadataSrc = []): Collection
    {
        return $collection->reduce(
            function ($result, $model) use ($base, $metadataSrc) {
                $code = self::getCodeEntity($model);
                if ($code && isset($model->id) && in_array($code, self::supportedLinkedEntities($base))) {
                    $metadata = $metadataSrc[$model->id] ?? [];
                    if ($model instanceof Element) {
                        $metadata['quantity'] = $metadata['quantity'] ?? 1;
                        $metadata['catalog_id'] = $model->catalog_id;
                    }

                    $result->add(
                        [
                            'to_entity_id' => $model->id,
                            'to_entity_type' => $code,
                            'metadata' => empty($metadata) ? null : $metadata,
                        ]
                    );
                }

                return $result;
            },
            new Collection()
        );
    }

    /**
     * @param mixed $model
     * @return string|null
     */
    protected function getCodeEntity($model)
    {
        switch (true) {
            case $model instanceof Lead:
                return 'leads';
            case $model instanceof Customer:
                return 'customers';
            case $model instanceof Contact:
                return 'contacts';
            case $model instanceof Company:
                return 'companies';
            case $model instanceof Element:
                return 'catalog_elements';
        }

        return null;
    }

    /**
     * @param mixed $model
     * @return array
     */
    protected function supportedLinkedEntities($model): array
    {
        switch (true) {
            case $model instanceof Lead:
            case $model instanceof Customer:
                return ['contacts', 'companies', 'catalog_elements'];
            case $model instanceof Contact:
                return ['companies', 'leads', 'customers', 'catalog_elements'];
            case $model instanceof Company:
                return ['contacts', 'leads', 'customers', 'catalog_elements'];
            // catalog_elements
        }

        return [];
    }

    /**
     * @param int $entityID
     * @param string $type
     * @return string
     */
    protected function getPathOfLinked($entityID, string $type = 'link')
    {
        $path = self::getPath('_update');
        return $path . "/$entityID/$type";
    }

    /**
     * @param string $type
     * @return string
     */
    protected function getPathOfMassLinked(string $type = 'link')
    {
        $path = self::getPath('_update');
        return $path . "/$type";
    }
}
