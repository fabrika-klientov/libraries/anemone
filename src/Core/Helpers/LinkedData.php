<?php
/**
 *
 * @package   Anemone
 * @category  Helpers
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.03.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Helpers;

use Anemone\Contracts\BeModel;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Company;
use Anemone\Models\Contact;
use Anemone\Models\Customer;
use Anemone\Models\Element;
use Anemone\Models\Lead;

class LinkedData
{
    private $base;
    private $linking;

    /**
     * @param BeModel $base
     * @param Collection|null $linking
     */
    public function __construct(BeModel $base, Collection $linking = null)
    {
        $this->base = $base;
        $this->linking = $linking ?? new Collection();
    }

    /**
     * @param LinkedMetadata $model
     * @return LinkedData
     */
    public function add(LinkedMetadata $model)
    {
        $this->linking->add($model);

        return $this;
    }

    /**
     * @param int $id
     * @return LinkedData
     */
    public function remove($id)
    {
        $this->linking = $this->linking->filter(
            function (LinkedMetadata $item) use ($id) {
                return $item->getTo()->id != $id;
            }
        );

        return $this;
    }

    /**
     * @return LinkedData
     */
    public function clear()
    {
        $this->linking = new Collection();

        return $this;
    }

    /**
     * @return BeModel
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * @return Collection
     */
    public function getLinking()
    {
        return $this->linking;
    }

    /**
     * @return Collection
     */
    public function amoPreparedData()
    {
        return $this->linking
            ->filter(
                function ($item) {
                    return $item instanceof LinkedMetadata;
                }
            )
            ->map(
                function (LinkedMetadata $item) {
                    $metadata = $item->getMetadata();
                    return array_merge(
                        [
                            'entity_id' => $this->base->id,
                            'to_entity_id' => $item->getTo()->id,
                            'to_entity_type' => self::getToEntityType($item->getTo()),
                        ],
                        empty($metadata) ? [] : ['metadata' => $metadata]
                    );
                }
            )
            ->values();
    }

    /**
     * @param BeModel $model
     * @return string|null
     */
    protected function getToEntityType(BeModel $model)
    {
        switch (true) {
            case $model instanceof Lead:
                return 'leads';
            case $model instanceof Customer:
                return 'customers';
            case $model instanceof Contact:
                return 'contacts';
            case $model instanceof Company:
                return 'companies';
            case $model instanceof Element:
                return 'catalog_elements';
        }

        return null;
    }
}
