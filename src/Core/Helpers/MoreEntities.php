<?php
/**
 *
 * @package   Anemone
 * @category  Helpers
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Helpers;

trait MoreEntities
{
    /**
     * @var string[] $supportEntities
     * */
    protected $supportEntities = [
        'lead',
        'customer',
        'contact',
        'company',
        'task',
        'incomingLead',
        'event',
        'tag',
        'element',
//        'user',
    ];
    /**
     * @var int $page
     * */
    protected $page = 0;
    /** count duplicates for get models
     * @var int $countDuplicates
     * */
    protected $countDuplicates = 0;

    /** is more entities
     * @return bool
     * */
    protected function isMore()
    {
        if (!in_array(self::getParsedEntityName(), $this->supportEntities)) {
            return false;
        }

        if ($this->builder->hasID()) {
            return false;
        }

        $query = $this->builder->getResult();
        if (isset($query['limit']) || isset($query['page'])) {
            return false;
        }

        return true;
    }

    /** next page (system)
     * @return void
     * */
    protected function next()
    {
        $this->page++;
        $this->builder->where('limit', 250); // v4
        $this->builder->where('page', $this->page);
    }

    /** clear (system)
     * @param bool $force
     * @return void
     */
    protected function clearLimit(bool $force = false)
    {
        $this->builder->where('limit', null);
        $this->builder->where('page', null);
        if ($force) {
            $this->page = 0;
        }
    }
}
