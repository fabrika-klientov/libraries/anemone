<?php
/**
 *
 * @package   Anemone
 * @category  Helpers
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Helpers;

use Anemone\Client;
use Anemone\Exceptions\InvalidDataException;
use Illuminate\Support\Collection;

trait ValidatorClient
{
    /** validator auth params
     * [
     *  domain => string        (required) (ex.: anemone.amocrm.ru)
     *  1.
     *  login => string         (required) (ex.: anemone@google.com)
     *  secret_key => string    (required) (api key)
     *  2.
     *  access_token => string  (required)
     * ]
     * @param mixed $params
     * @return array
     * @throws InvalidDataException
     * */
    protected function validate($params)
    {
        if (is_null($params)) {
            throw new InvalidDataException('Store new Client instance require params. NULL');
        }

        if (is_object($params)) {
            $params = (array)$params;
        }

        if (!is_array($params)) {
            throw new InvalidDataException(
                'Store new Client instance require params should be object (stdClass) or array'
            );
        }

        if (
            isset($params['domain']) && is_string($params['domain']) &&
            (isset($params['login']) && is_string($params['login']) &&
                isset($params['secret_key']) && is_string($params['secret_key'])) ||
            (isset($params['access_token']) && is_string($params['access_token']))
        ) {
            return $params;
        }

        throw new InvalidDataException(
            'Store new Client instance require params should have: [domain => string], ([login => string], [secret_key => string]) or [access_token => string]'
        );
    }

    /** validator for collection
     * @param Collection $collection
     * @return void
     * @throws InvalidDataException
     * */
    protected static function validateInstances(Collection $collection)
    {
        $status = $collection->every(
            function ($client) {
                return $client instanceof Client;
            }
        );
        if (!$status) {
            throw new InvalidDataException('Inject new Collection of instances should be instance of Anemone\Client');
        }
    }

}
