<?php
/**
 *
 * @package   Anemone
 * @category  Helpers
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Helpers;

use Anemone\Core\Collection\Collection;
use Anemone\Models\Pipeline;
use Anemone\Models\StatusPL;

trait Pipelines
{
    /**
     * @var string[] $supportPipelinesEntities
     * */
    protected $supportPipelinesEntities = [
        'lead',
    ];

    /**
     * @return Collection
     */
    public function pl()
    {
        return self::_pl();
    }

    /**
     * @param Collection $collection
     * @return Collection
     */
    public function savePL(Collection $collection)
    {
        return self::_savePL($collection);
    }

    /**
     * @param Collection $collection
     * @return bool
     */
    public function deletePL(Collection $collection)
    {
        return self::_deletePL($collection);
    }

    /**
     * @param int $pipeline
     * @return Collection
     */
    public function statusesPL($pipeline)
    {
        return self::_pl($pipeline);
    }

    /**
     * @param Collection $collection
     * @param int $pipeline
     * @return Collection
     */
    public function saveStatusesPL(Collection $collection, $pipeline)
    {
        return self::_savePL($collection, $pipeline);
    }

    /**
     * @param Collection $collection
     * @param int $pipeline
     * @return bool
     */
    public function deleteStatusesPL(Collection $collection, $pipeline)
    {
        return self::_deletePL($collection, $pipeline);
    }


    /**
     * @param int $pipeline
     * @return Collection
     */
    private function _pl($pipeline = null)
    {
        if (!in_array($this->entity(), $this->supportPipelinesEntities)) {
            return new Collection();
        }

        $result = json_decode($this->client->getQueryService()->get(self::getPathOfPL('get', $pipeline)), true);

        $class = $pipeline ? StatusPL::class : Pipeline::class;
        return (new Collection($result['_embedded'][$pipeline ? 'statuses' : 'pipelines'] ?? []))
            ->map(
                function ($item) use ($class) {
                    unset($item['_links']);
                    return new $class($item);
                }
            );
    }

    /**
     * @param Collection $collection
     * @param int $pipeline
     * @return Collection
     */
    private function _savePL(Collection $collection, $pipeline = null)
    {
        if ($collection->isEmpty()) {
            return $collection;
        }

        [$store, $update] = $collection->reduce(
            function (array $result, $item) {
                $result[isset($item->id) ? 1 : 0]->add($item);
                return $result;
            },
            [new Collection(), new Collection()]
        );

        $collect = new Collection();

        if ($store->isNotEmpty()) {
            $result = json_decode(
                $this->client->getQueryService()->post(self::getPathOfPL('add', $pipeline), $store->toArray()),
                true
            );
            $class = $pipeline ? StatusPL::class : Pipeline::class;

            $collect = $collect->merge(
                (new Collection($result['_embedded'][$pipeline ? 'statuses' : 'pipelines'] ?? []))
                    ->map(
                        function ($item) use ($class) {
                            unset($item['_links']);
                            return new $class($item);
                        }
                    )
            );
        }

        if ($update->isNotEmpty()) {
            $path = self::getPathOfPL('update', $pipeline);
            $collect = $collect->merge(
                $update->reduce(
                    function ($resulted, $item) use ($path, $pipeline) {
                        $data = $item->jsonSerialize();
                        $result = json_decode(
                            $this->client->getQueryService()->patch($path . '/' . $item->id, $data),
                            true
                        );

                        return $resulted->add($pipeline ? new StatusPL($result) : new Pipeline($result));
                    },
                    new Collection()
                )
            );
        }

        return $collect;
    }

    /**
     * @param Collection $collection
     * @param int $pipeline
     * @return bool
     */
    private function _deletePL(Collection $collection, $pipeline = null)
    {
        $deleted = $collection->filter(
            function ($item) {
                return isset($item->id);
            }
        );

        if ($deleted->isEmpty()) {
            return false;
        }

        $path = self::getPathOfPL('delete', $pipeline);

        $deleted->each(
            function ($item) use ($path) {
                $this->client->getQueryService()->delete($path . '/' . $item->id);
            }
        );

        return true;
    }

    /**
     * @param string $code
     * @param int|null $pipeline
     * @return string
     */
    protected function getPathOfPL(string $code, $pipeline)
    {
        $path = aEntryData($this->currentVersion . '.pipeline.' . $code);
        return str_replace(
                '{entity_type}',
                $this->entity() == 'company' ? 'companies' : ($this->entity() . 's'),
                $path
            )
            . ($pipeline ? "/$pipeline/statuses" : '');
    }
}
