<?php
/**
 *
 * @package   Anemone
 * @category  Helpers
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.03.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Helpers;

use Anemone\Contracts\BeModel;

class LinkedMetadata
{
    private $to;
    private $metadata;

    /**
     * @param BeModel $to
     * @param array|null $metadata
     */
    public function __construct(BeModel $to, array $metadata = null)
    {
        $this->to = $to;
        $this->metadata = $metadata;
    }

    /**
     * @param array|null $data
     */
    public function setMetadata(?array $data)
    {
        $this->metadata = $data;
    }

    /**
     * @return BeModel
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return array|null
     */
    public function getMetadata()
    {
        return $this->metadata;
    }
}
