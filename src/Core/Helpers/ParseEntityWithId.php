<?php
/**
 *
 * @package   Anemone
 * @category  Helpers
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.17
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Helpers;

trait ParseEntityWithId
{
    /**
     * @param string|null $name
     * @return string
     */
    protected function getParsedEntityName(string $name = null): string
    {
        $name = $name ?? (method_exists($this, 'entity') ? $this->entity() : '');
        switch (true) {
            case preg_match('/^tag_(lead|contact|customer|company)$/', $name):
                return 'tag';

            default:
                return preg_replace('/_\d+/', '', $name);
        }
    }
}
