<?php
/**
 *
 * @package   Anemone
 * @category  Helpers
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.16
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Core\Helpers;

use Anemone\Core\Collection\Collection;

trait CFWithGroup
{
    use CFOnly;

    /**
     * @return Collection
     * */
    public function groupsCF()
    {
        return self::_cf([], true);
    }

    /**
     * @param Collection $collection
     * @return Collection
     */
    public function saveGroupCF(Collection $collection)
    {
        return self::_saveCF($collection, true);
    }

    /**
     * @param Collection $collection
     * @return bool
     */
    public function deleteGroupCF(Collection $collection)
    {
        return self::_deleteCF($collection, true);
    }
}
