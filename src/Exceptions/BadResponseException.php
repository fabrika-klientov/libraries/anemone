<?php
/**
 *
 * @package   Anemone
 * @category  Exceptions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Exceptions;

class BadResponseException extends AnemoneException
{
}
