<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.16
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeModel;
use Anemone\Exceptions\AnemoneException;
use Anemone\Models\Instances\ElementsInstance;

/**
 * @property-read int $id
 * @property string $name
 * @property-read int $created_by
 * @property int $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property int $sort
 * @property string $type regular | invoices | products
 * @property bool $can_add_elements
 * @property bool $can_show_in_cards
 * @property bool $can_link_multiple
 * @property bool $can_be_deleted
 * @property string|null $sdk_widget_code
 * @property int $account_id
 *
 * @method __construct(\Anemone\Models\Instances\CatalogsInstance $instance = null, array $resource = [])
 * @method $this setInstance(\Anemone\Models\Instances\CatalogsInstance $instance)
 * */
class Catalog extends Model implements BeModel
{
    /**
     * @return ElementsInstance
     *
     * @throws AnemoneException
     */
    public function elements()
    {
        if (empty($this->instance)) {
            throw new AnemoneException(
                'For use load elements - current model should have instance [Anemone\Models\Instances\CatalogsInstance]'
            );
        }

        if (empty($this->id)) {
            throw new AnemoneException('For use load elements - current model should exist');
        }

        return new ElementsInstance($this->instance->getClient(), $this);
    }
}
