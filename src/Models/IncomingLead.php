<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeModel;
use Anemone\Core\Collection\Collection;
use Anemone\Exceptions\AnemoneException;

/**
 * @property string $source_name
 * @property string $uid
 * @property string $source_uid
 * @property string $created_at
 * @property int $pipeline_id
 * @property int $account_id
 * @property string $category sip, mail, forms, chats
 * @property array $_embedded ['leads' => Collection, 'contacts' => Collection, 'companies' => Collection]
 * @property array $metadata (see https://www.amocrm.ru/developers/content/crm_platform/unsorted-api)
 * */
class IncomingLead extends Model implements BeModel
{
    /**
     * @var \Anemone\Models\Instances\IncomingLeadsInstance|null $instance
     * */
    protected $instance;

    /** incoming_entities of leads
     * @param bool $isFull
     * @return Collection
     */
    public function incLeads(bool $isFull = false)
    {
        $collect = self::_inc('leads');
        if (!$isFull || $collect->isEmpty()) {
            return $collect;
        }

        if (is_null($this->instance)) {
            throw new AnemoneException(
                'For use [incLeads] for full collect data this model should initializing [ModelInstance] (In [construct] or [setInstance] method)'
            );
        }

        return $this->instance->getClient()->leads->find($collect->pluck('id')->values()->all())->get();
    }

    /** incoming_entities of contacts
     * @param bool $isFull
     * @return Collection
     */
    public function incContacts(bool $isFull = false)
    {
        $collect = self::_inc('contacts');
        if (!$isFull || $collect->isEmpty()) {
            return $collect;
        }

        if (is_null($this->instance)) {
            throw new AnemoneException(
                'For use [incContacts] for full collect data this model should initializing [ModelInstance] (In [construct] or [setInstance] method)'
            );
        }

        return $this->instance->getClient()->contacts->find($collect->pluck('id')->values()->all())->get();
    }

    /** incoming_entities of companies
     * @param bool $isFull
     * @return Collection
     * @throws AnemoneException
     */
    public function incCompanies(bool $isFull = false)
    {
        $collect = self::_inc('companies');
        if (!$isFull || $collect->isEmpty()) {
            return $collect;
        }

        if (is_null($this->instance)) {
            throw new AnemoneException(
                'For use [incCompanies] for full collect data this model should initializing [ModelInstance] (In [construct] or [setInstance] method)'
            );
        }

        return $this->instance->getClient()->companies->find($collect->pluck('id')->values()->all())->get();
    }

    /**
     * @param string $code
     * @return Collection|mixed
     */
    private function _inc(string $code)
    {
        return is_array($this->_embedded[$code])
            ? new Collection($this->_embedded[$code])
            : ($this->_embedded[$code] ?? new Collection());
    }

    /** accept current model
     * @param int $userId
     * @param int $statusId
     * @return array|null
     * */
    public function accept($userId = null, $statusId = null)
    {
        if (is_null($this->instance)) {
            throw new AnemoneException(
                'For use [accept] this model should initializing [ModelInstance] (In [construct] or [setInstance] method)'
            );
        }

        $result = $this->instance->accept(new Collection([$this]), $userId, $statusId);
        if (isset($result)) {
            return $result->first();
        }

        return null;
    }

    /** decline current model
     * @param int $userId
     * @return array|null
     * */
    public function decline($userId = null)
    {
        if (is_null($this->instance)) {
            throw new AnemoneException(
                'For use [accept] this model should initializing [ModelInstance] (In [construct] or [setInstance] method)'
            );
        }

        $result = $this->instance->decline(new Collection([$this]), $userId);
        if (isset($result)) {
            return $result->first();
        }

        return null;
    }
}
