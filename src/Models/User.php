<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeModel;
use Anemone\Core\Collection\Collection;

/**
 * @property-read int $id
 * @property-read string $name
 * @property-read string $email
 * @property-read string $lang
 * @property-read array $rights
 * @property-read array $_embedded
 * */
class User extends Model implements BeModel
{
    /**
     * @param string $type
     * @return mixed|null
     */
    public function right(string $type)
    {
        return $this->rights[$type] ?? null;
    }

    /**
     * @return Collection
     * */
    public function roles(): Collection
    {
        return new Collection($this->_embedded['roles'] ?? []);
    }

    /**
     * @return Collection
     * */
    public function groups(): Collection
    {
        return new Collection($this->_embedded['groups'] ?? []);
    }
}
