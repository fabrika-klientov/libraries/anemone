<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

abstract class Base implements \JsonSerializable
{
    /**
     * @var array $data
     * */
    protected $data = [];

    /**
     * @param array $resource
     * @param array $prefer
     * @return void
     * */
    public function __construct($resource = [], $prefer = [])
    {
        $this->data = $resource ?? $prefer;
    }

    /** getter for data[$name]
     * @param string $name
     * @return mixed
     * */
    public function __get($name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }

        return null;
    }

    /** getter in data[$name] = $value
     * @param string $name
     * @param mixed $value
     * @return void
     * */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /** isset in data[$name]
     * @param string $name
     * @return bool
     * */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    /**
     * @param string $name
     * @return void
     */
    public function __unset($name)
    {
        unset($this->data[$name]);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function existProp($name)
    {
        return array_key_exists($name, $this->data);
    }

    /** \JsonSerializable
     * @return array
     * */
    public function jsonSerialize()
    {
        return $this->data;
    }

    /** encode to string
     * @return string
     * */
    public function __toString()
    {
        return (string)json_encode($this->data);
    }
}
