<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 20.01.31
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Helpers;

use Anemone\Models\Instances\NotesInstance;
use Illuminate\Support\Str;

/**
 * @method \Anemone\Client getClient()
 * @method string entity()
 * */
trait WithNotes
{
    /** instance of NotesInstance
     * @return NotesInstance|null
     * */
    public function notes()
    {
        if (method_exists($this, 'getClient')) {
            $client = $this->getClient();
        } elseif (isset($this->instance)) {
            $client = $this->instance->getClient();
        }

        if (empty($client)) {
            return null;
        }

        $entity = method_exists($this, 'entity') ? $this->entity() : $this->getEntity();

        $instance = new NotesInstance($client, $entity);
        if (!empty($this->id)) {
            $instance->entityID($this->id);
        }

        return $instance;
    }

    /**
     * @return string
     * */
    protected function getEntity()
    {
        return Str::lower(last(explode('\\', self::class)));
    }
}
