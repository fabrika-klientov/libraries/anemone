<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Helpers;

use Anemone\Core\Collection\Collection;
use Anemone\Exceptions\AnemoneException;

trait WithTasks
{
    /** get lazy loading linked tasks
     * @return Collection<\Anemone\Models\Task>
     * @throws AnemoneException
     * */
    public function tasks()
    {
        if (isset($this->instance)) {
            return $this->instance->getClient()->tasks->forEntityID($this->id)->get();
        }

        throw new AnemoneException(
            'For use load other models - current model should have instance [Anemone\Models\Instances\ModelInstance]'
        );
    }
}
