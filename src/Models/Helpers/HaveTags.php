<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Helpers;

use Anemone\Models\Instances\TagsInstance;

trait HaveTags
{
    /** amo return error 500 for getting
     * @return TagsInstance
     * */
    public function tags()
    {
        return new TagsInstance($this->getClient(), $this->entity());
    }
}
