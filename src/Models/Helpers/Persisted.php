<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.11.20
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Helpers;

/**
 * @method __construct(\Anemone\Models\Instances\ModelInstance $instance = null, array $resource = [])
 * */
trait Persisted
{
    /**
     * @inheritDoc
     * */
    public function getFixed(): array
    {
        return $this->jsonSerialize();
    }

    /**
     * @inheritDoc
     * */
    public function factory(array $data)
    {
        return new static($this->instance, $data);
    }
}
