<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Helpers;

use Anemone\Core\Collection\Collection;
use Anemone\Exceptions\AnemoneException;

trait WithCustomers
{
    /** get lazy loading linked customers
     * @return Collection
     * @throws AnemoneException
     * */
    public function customers()
    {
        if (isset($this->instance)) {
            $customers = $this->customersId();
            if ($customers->isEmpty()) {
                return new Collection();
            }

            return $this->instance->getClient()->customers->find($customers->toArray())->get();
        }

        throw new AnemoneException(
            'For use load other models - current model should have instance [Anemone\Models\Instances\ModelInstance]'
        );
    }

    /**
     * @return Collection
     * */
    public function customersId()
    {
        return new Collection(
            array_map(
                function ($one) {
                    return $one['id'];
                },
                $this->_embedded['customers'] ?? []
            )
        );
    }
}
