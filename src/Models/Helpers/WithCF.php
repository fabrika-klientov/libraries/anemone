<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Helpers;

use Anemone\Core\Collection\Collection;
use Illuminate\Support\Str;

trait WithCF
{

    /** filtering collect of CF for closure or name fields or id field
     * @param int|string|\Closure|mixed $closure
     *
     * ex.:
     * v1. (for Closure)
     * $collectOfCF = $model->cf(function(\Anemone\Contracts\BeCustomField $field) {
     *  return $field->id != 12345;
     * });
     *
     * v2. (for ID)
     * $oneCF = $model->cf(12345);
     *
     * v3. (for name field)
     * $oneCF = $model->cf('jarvis');
     *
     * @return \Anemone\Contracts\BeCustomField|Collection|null
     * */
    public function cf($closure)
    {
        if ($closure instanceof \Closure) {
            return $this->custom_fields_values->filter($closure);
        }

        if (is_numeric($closure)) {
            return $this->custom_fields_values->first(function ($item) use ($closure) {
                return $item->id == $closure;
            });
        }

        if (is_string($closure)) {
            return $this->custom_fields_values->first(function ($item) use ($closure) {
                return Str::upper($item->name) == Str::upper($closure);
            });
        }

        return null;
    }

    /** init default collect of CF
     * @return void
     * */
    protected function initCF()
    {
        if (isset($this->instance) && method_exists($this->instance, 'cf')) {
            $this->custom_fields_values = $this->instance->cf();
            return;
        }

        $this->custom_fields_values = new Collection();
    }
}
