<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.08.13
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Helpers;

use Anemone\Contracts\BeCustomField;
use Anemone\Models\CF\MultiTextCustomField;

trait Phoned
{
    public static $phoneCode = 'PHONE';

    /**
     * @return MultiTextCustomField|null
     * */
    public function phoneField(): ?MultiTextCustomField
    {
        return $this
            ->cf(function (BeCustomField $item) {
                return $item->field_code == self::$phoneCode || $item->code == self::$phoneCode;
            })
            ->first();
    }

    /**
     * @param string|null $enum
     * @return string[]
     */
    public function phones(string $enum = null): array
    {
        $field = $this->phoneField();
        if (empty($field) || empty($field->values)) {
            return [];
        }

        $values = $field->values;
        if ($enum) {
            $values = array_filter($values, function ($item) use ($enum) {
                return $item['enum_code'] == $enum;
            });
        }

        return array_map(function ($item) {
            return $item['value'];
        }, $values);
    }

    /**
     * @param string|null $enum
     * @return string|null
     */
    public function phone(string $enum = null): ?string
    {
        $list = $this->phones($enum);

        return empty($list) ? null : head($list);
    }
}
