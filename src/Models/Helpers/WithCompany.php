<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Helpers;

use Anemone\Exceptions\AnemoneException;
use Anemone\Models\Company;

trait WithCompany
{
    /** get lazy loading linked company
     * @return \Anemone\Models\Company|null
     * @throws AnemoneException
     * */
    public function company()
    {
        if (isset($this->instance)) {
            $companyId = $this->companyID();
            if (empty($companyId)) {
                return null;
            }

            return $this->instance->getClient()->companies->find($companyId)->get()->first();
        }

        throw new AnemoneException('For use load other models - current model should have instance [Anemone\Models\Instances\ModelInstance]');
    }

    /**
     * Only for create entity
     * @param \Anemone\Models\Company|array|int $item
     * @return void
     */
    public function attachCompany($item)
    {
        $id = null;
        switch (true) {
            case $item instanceof Company:
                $id = $item->id;
                break;
            case is_array($item):
                $id = $item['id'];
                break;
            default:
                $id = $item;
        }

        if (empty($id)) {
            return;
        }

        $this->data['_embedded']['companies'] = [['id' => $id]];
    }

    /**
     * @return int|null
     * */
    public function companyID()
    {
        return $this->_embedded['companies'][0]['id'] ?? null;
    }
}
