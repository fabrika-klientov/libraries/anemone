<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Helpers;

use Anemone\Core\Collection\Collection;
use Anemone\Exceptions\InvalidDataException;
use Anemone\Models\Tag;

trait WithTags
{

    /** smart pushing tags
     * @param Collection|Tag|array|string $data
     * @return void
     * @throws InvalidDataException
     * */
    public function attachTags($data)
    {
        if ($data instanceof Collection) {
            if (!$data->every(
                function ($item) {
                    return $item instanceof Tag;
                }
            )) {
                throw new InvalidDataException(
                    'Collection to method [attachTags] should be instances of model [Anemone\Models\Tag]'
                );
            }

            $arrayOfTags = $data
                ->map(
                    function (Tag $tag) {
                        return ['name' => $tag->name];
                    }
                )
                ->toArray();
        } elseif ($data instanceof Tag) {
            $arrayOfTags = [['name' => $data->name]];
        } elseif (is_array($data)) {
            $arrayOfTags = array_map(
                function ($one) {
                    return ['name' => $one];
                },
                $data
            );
        } elseif (is_string($data)) {
            $arrayOfTags = [['name' => $data]];
        }

        if (isset($arrayOfTags)) {
            $this->data['_embedded']['tags'] = array_merge($this->data['_embedded']['tags'] ?? [], $arrayOfTags);
        }
    }

    /** smart detaching tags in models (if null -> all tags detach)
     * @param Collection|Tag|array|string $data
     * @return void
     * @throws InvalidDataException
     * */
    public function detachTags($data = null)
    {
        if (is_null($data)) {
            $this->data['_embedded']['tags'] = [];
            return;
        }

        if ($data instanceof Collection) {
            if (!$data->every(
                function ($item) {
                    return $item instanceof Tag;
                }
            )) {
                throw new InvalidDataException(
                    'Collection to method [detachTags] should be instances of model [Anemone\Models\Tag]'
                );
            }

            $this->data['_embedded']['tags'] = collect($this->data['_embedded']['tags'] ?? [])
                ->filter(
                    function ($item) use ($data) {
                        return $data->every(
                            function ($one) use ($item) {
                                return $one->name != $item['name'];
                            }
                        );
                    }
                )
                ->values()
                ->toArray();
        } elseif ($data instanceof Tag) {
            $this->data['_embedded']['tags'] = collect($this->data['_embedded']['tags'] ?? [])
                ->filter(
                    function ($item) use ($data) {
                        return $item['name'] != $data->name;
                    }
                )
                ->values()
                ->toArray();
        } elseif (is_array($data)) {
            $this->data['_embedded']['tags'] = collect($this->data['_embedded']['tags'] ?? [])
                ->filter(
                    function ($item) use ($data) {
                        return !in_array($item['name'], $data);
                    }
                )
                ->values()
                ->toArray();
        } elseif (is_string($data)) {
            $this->data['_embedded']['tags'] = collect($this->data['_embedded']['tags'] ?? [])
                ->filter(
                    function ($item) use ($data) {
                        return $item['name'] != $data;
                    }
                )
                ->values()
                ->toArray();
        }
    }

    /**
     * @return Collection
     * */
    public function tags()
    {
        return new Collection(
            array_map(
                function ($item) {
                    return new Tag(['id' => $item['id'] ?? null, 'name' => $item['name']]);
                },
                $this->_embedded['tags'] ?? []
            )
        );
    }
}
