<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.17
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Helpers;

use Anemone\Core\Collection\Collection;
use Anemone\Exceptions\AnemoneException;

trait Link
{
    /**
     * @param Collection $collection
     * @param array $metadata (ex.: [12345 => [quantity => 3]] for catalog_elements)
     * @return bool
     * @throws AnemoneException
     */
    public function link(Collection $collection, array $metadata = [])
    {
        if (isset($this->instance) && method_exists($this->instance, 'link')) {
            return $this->instance->link($this, $collection, $metadata);
        }

        throw new AnemoneException(
            'For use link other models - current model should have instance [Anemone\Models\Instances\ModelInstance] and instance should support link trait'
        );
    }

    /**
     * @param Collection $collection
     * @return bool
     * @throws AnemoneException
     */
    public function unlink(Collection $collection)
    {
        if (isset($this->instance) && method_exists($this->instance, 'unlink')) {
            return $this->instance->unlink($this, $collection);
        }

        throw new AnemoneException(
            'For use link other models - current model should have instance [Anemone\Models\Instances\ModelInstance] and instance should support link trait'
        );
    }
}
