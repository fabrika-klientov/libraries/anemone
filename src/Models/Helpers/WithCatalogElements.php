<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.17
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Helpers;

use Anemone\Core\Collection\Collection;
use Anemone\Exceptions\AnemoneException;
use Anemone\Models\Instances\ElementsInstance;

/**
 * */
trait WithCatalogElements
{
    /** get lazy loading linked catalog elements
     * @return Collection
     * @throws AnemoneException
     * */
    public function catalogElements()
    {
        if (isset($this->instance)) {
            $elements = $this->catalogElementsSimple();
            if ($elements->isEmpty()) {
                return new Collection();
            }

            return $elements
                ->groupBy('catalog_id')
                ->map(
                    function ($collect, $catalogId) {
                        $instance = new ElementsInstance($this->instance->getClient(), $catalogId);
                        return $instance->find($collect->pluck('id')->values()->all())->get();
                    }
                )
                ->reduce(
                    function (Collection $result, $collect) {
                        return $result->merge($collect);
                    },
                    new Collection()
                );
        }

        throw new AnemoneException(
            'For use load other models - current model should have instance [Anemone\Models\Instances\ModelInstance]'
        );
    }

    /**
     * @return Collection
     * */
    public function catalogElementsSimple()
    {
        return new Collection(
            array_map(
                function ($one) {
                    return [
                        'id' => $one['id'],
                        'catalog_id' => $one['metadata']['catalog_id'],
                        'quantity' => $one['metadata']['quantity'] ?? 1,
                    ];
                },
                $this->_embedded['catalog_elements'] ?? []
            )
        );
    }
}
