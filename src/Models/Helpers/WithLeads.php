<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Helpers;

use Anemone\Core\Collection\Collection;
use Anemone\Exceptions\AnemoneException;

/**
 * */
trait WithLeads
{
    /** get lazy loading linked leads
     * @return Collection
     * @throws AnemoneException
     * */
    public function leads()
    {
        if (isset($this->instance)) {
            $leads = $this->leadsId();
            if ($leads->isEmpty()) {
                return new Collection();
            }

            return $this->instance->getClient()->leads->find($leads->toArray())->get();
        }

        throw new AnemoneException(
            'For use load other models - current model should have instance [Anemone\Models\Instances\ModelInstance]'
        );
    }

    /**
     * @return Collection
     * */
    public function leadsId()
    {
        return new Collection(
            array_map(
                function ($one) {
                    return $one['id'];
                },
                $this->_embedded['leads'] ?? []
            )
        );
    }
}
