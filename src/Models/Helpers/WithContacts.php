<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Helpers;

use Anemone\Core\Collection\Collection;
use Anemone\Exceptions\AnemoneException;
use Anemone\Models\Contact;

/**
 * */
trait WithContacts
{
    /** get lazy loading linked contacts
     * @return Collection
     * @throws AnemoneException
     * */
    public function contacts()
    {
        if (isset($this->instance)) {
            $contacts = $this->contactsId();
            if ($contacts->isEmpty()) {
                return new Collection();
            }

            return $this->instance->getClient()->contacts->find($contacts->toArray())->get();
        }

        throw new AnemoneException(
            'For use load other models - current model should have instance [Anemone\Models\Instances\ModelInstance]'
        );
    }

    /**
     * @return Collection
     * */
    public function contactsId()
    {
        return new Collection(
            array_map(
                function ($one) {
                    return $one['id'];
                },
                $this->_embedded['contacts'] ?? []
            )
        );
    }

    /**
     * Only for create entity
     * @param Collection|array $list
     * @return void
     */
    public function attachContacts($list)
    {
        if (is_array($list)) {
            $list = new Collection($list);
        }

        $result = $list
            ->map(
                function ($item) {
                    switch (true) {
                        case $item instanceof Contact:
                            return $item->id;
                        case is_array($item):
                            return $item['id'] ?? null;
                        default:
                            return $item;
                    }
                }
            )
            ->filter(
                function ($item) {
                    return !empty($item);
                }
            )
            ->values();

        if ($result->isEmpty()) {
            return;
        }

        $merged = $this->contactsId()->merge($result)->unique();

        $this->data['_embedded']['contacts'] = $merged
            ->map(
                function ($item) {
                    return ['id' => $item];
                }
            )
            ->values()
            ->all();
    }

    /**
     * @return int|null
     * */
    public function mainContactId()
    {
        return array_reduce(
            $this->_embedded['contacts'] ?? [],
            function ($result, $one) {
                return $result ?? ($one['is_main'] ? $one['id'] : $result);
            },
            null
        );
    }
}
