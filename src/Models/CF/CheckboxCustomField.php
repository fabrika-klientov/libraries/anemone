<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\CF;

use Anemone\Contracts\BeCustomField;

class CheckboxCustomField extends BaseCustomField implements BeCustomField
{
    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->type = 'checkbox';
    }

    /**
     * @return bool
     * */
    public function getValue()
    {
        return parent::getValue() == 1;
    }

    /**
     * @param bool $data
     * @return void
     * */
    public function setValue($data)
    {
        parent::setValue($data ? 1 : 0);
    }
}
