<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\CF;

use Anemone\Contracts\BeCustomField;
use Anemone\Models\CF\Helpers\HasEnums;

class SelectCustomField extends BaseCustomField implements BeCustomField
{
    use HasEnums;

    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->type = 'select';
    }

    /**
     * @param string $data
     * @return void
     * */
    public function setValue($data)
    {
        $key = $this->getInEnums($data);

        if (isset($key)) {
            $this->values = [['value' => $data, 'enum_id' => $key]];
        }
    }
}
