<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\CF;

use Anemone\Contracts\BeCustomField;

class OrgLegalNameCustomField extends BaseCustomField implements BeCustomField
{
    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->type = 'org_legal_name';
    }

    /** array of string
     * @return array|null
     * */
    public function getValue()
    {
        return empty($this->values) ? null : array_map(function ($item) {
            return $item['value']['name'] ?? '';
        }, $this->values);
    }

    /** array of string (rewrite old data)
     * @param array $data
     * @return void
     * */
    public function setValue($data)
    {
        $this->values = array_map(function ($item) {
            return [
                'value' => [
                    'name' => $item,
                ],
            ];
        }, $data);
    }
}
