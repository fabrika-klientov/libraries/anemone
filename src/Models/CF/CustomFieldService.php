<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\CF;

use Anemone\Core\Collection\Collection;
use Illuminate\Support\Str;

final class CustomFieldService
{
    /**
     * @var array $codes
     * */
    protected $codes = [
        'text' => 'text',
        'numeric' => 'numeric',
        'checkbox' => 'checkbox',
        'select' => 'select',
        'multiselect' => 'multiSelect',
        'date' => 'date',
        'url' => 'url',
        'multitext' => 'multiText',
        'textarea' => 'textarea',
        'radiobutton' => 'radiobutton',
        'streetaddress' => 'streetAddress',
        'smart_address' => 'smartAddress',
        'birthday' => 'birthday',
        'legal_entity' => 'legalEntity',
        'items' => 'items',
        'org_legal_name' => 'orgLegalName',
        'date_time' => 'dateTime',
        'price' => 'price',
        'category' => 'category',
    ];

    /**
     * @return void
     * */
    public function __construct()
    {
    }

    /** get need model from src data
     * @param array $data
     * @param array|null $inject
     * @return \Anemone\Contracts\BeCustomField|null
     */
    public function getCFModel(array $data, array $inject = null)
    {
        $model = $this->codes[$data['type'] ?? null] ?? null;
        if (is_null($model)) { // type not support
            return null;
        }

        $class = 'Anemone\\Models\\CF\\' . Str::ucfirst($model) . 'CustomField';

        if (!class_exists($class)) {
            throw new \RuntimeException('Class [' . $class . '] not found');
        }

        unset($data['_links']);

        if (isset($inject)) {
            $data = array_merge($data, $inject);
        }

        return new $class($data);
    }

    /** get collect of model from src data
     * @param array $data
     * @param array $injectData
     * @return Collection
     */
    public function getByCollect(array $data, array $injectData = [])
    {
        return (new Collection(
            array_map(
                function ($item) use ($injectData) {
                    return $this->getCFModel(
                        $item,
                        collect($injectData)->first(
                            function ($one) use ($item) {
                                return $one['field_id'] == $item['id'];
                            }
                        )
                    );
                },
                $data
            )
        ))
            ->filter(
                function ($item) {
                    return isset($item);
                }
            )
            ->values();
    }
}
