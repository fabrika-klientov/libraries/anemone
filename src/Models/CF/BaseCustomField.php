<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\CF;

use Anemone\Contracts\BeBaseModel;
use Anemone\Models\Base;

/**
 * @property-read int $id
 * @property string $name
 * @property string $type
 * @property-read int $field_type
 * @property mixed $remind
 * @property string $entity_type
 * @property array $required_statuses only lead
 * @property string $group_id
 * @property bool $is_api_only
 * @property-read string $code
 * @property string $sort
 * @property-read int $account_id
 * @property array $values
 * // other depends on version
 * */
abstract class BaseCustomField extends Base implements BeBaseModel
{

    /** for string data
     * @return string|mixed
     * */
    public function getValue()
    {
        return empty($this->values) ? null : head($this->values)['value'];
    }

    /** for string data
     * @param string|mixed $data
     * @return void
     * */
    public function setValue($data)
    {
        $this->values = [
            ['value' => $data],
        ];
    }
}
