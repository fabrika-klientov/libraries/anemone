<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\CF;

use Anemone\Contracts\BeCustomField;

class NumericCustomField extends BaseCustomField implements BeCustomField
{
    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->type = 'numeric';
    }

    /** numeric
     * @return int
     * */
    public function getValue()
    {
        return empty($this->values) ? null : head($this->values)['value'];
    }

    /** numeric
     * @param int $data
     * @return void
     * */
    public function setValue($data)
    {
        $this->values = [
            ['value' => $data],
        ];
    }
}
