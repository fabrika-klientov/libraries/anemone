<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.01.27
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\CF;

use Anemone\Contracts\BeCustomField;
use Anemone\Models\CF\Helpers\HasEnums;

/**
 * @property-read array $nested
 * */
class CategoryCustomField extends BaseCustomField implements BeCustomField
{
    use HasEnums;

    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->type = 'category';
        $this->enumsKey = 'nested';
    }

    /**
     * @param string $data
     * @return void
     * */
    public function setValue($data)
    {
        $key = $this->getInEnums($data);

        if (isset($key)) {
            $this->values = [['value' => $data, 'enum_id' => $key]];
        }
    }
}
