<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\CF\Helpers;

/**
 * @property string $name
 * @property int $vat_id
 * @property int $kpp
 * */
class SubLegalEntity implements \JsonSerializable
{
    private $data;

    /**
     * @param array $data
     * @return void
     * */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * @param string $name
     * @return string
     * */
    public function __get($name)
    {
        return $this->data[$name] ?? null;
    }

    /**
     * @param string $name
     * @param string $value
     * @return void
     * */
    public function __set($name, $value)
    {
        switch ($name) {
            case 'name':
            case 'vat_id':
            case 'kpp':
                $this->data[$name] = $value;
        }
    }

    /** get converted array key->value
     * @return array
     * */
    public function getValues()
    {
        return $this->data;
    }

    /** \JsonSerializable
     * @return array
     * */
    public function jsonSerialize()
    {
        return $this->data;
    }

    /**
     * @return string
     * */
    public function __toString()
    {
        return (string)json_encode($this->data);
    }
}
