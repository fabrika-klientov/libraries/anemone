<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\CF\Helpers;

/**
 * @property-read array $enums
 * */
trait HasEnums
{
    protected $enumsKey = 'enums';

    /** has value in enums
     * @param string $search
     * @return bool
     * */
    public function inEnums($search)
    {
        if (empty($this->{$this->enumsKey})) {
            return false;
        }

        foreach ($this->{$this->enumsKey} as $value) {
            if ($value['value'] == $search) {
                return true;
            }
        }

        return false;
    }

    /** get key in enums
     * @param string $search
     * @return string|null
     * */
    public function getInEnums($search)
    {
        if (empty($this->{$this->enumsKey})) {
            return null;
        }

        foreach ($this->{$this->enumsKey} as $value) {
            if ($value['value'] == $search) {
                return $value['id'];
            }
        }

        return null;
    }

    /** get value in enums
     * @param string $searchKey
     * @return string|null
     * */
    public function getValueEnums($searchKey)
    {
        if (empty($this->{$this->enumsKey})) {
            return null;
        }

        foreach ($this->{$this->enumsKey} as $value) {
            if ($value['id'] == $searchKey) {
                return $value['value'];
            }
        }

        return null;
    }
}
