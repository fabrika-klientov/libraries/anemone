<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\CF\Helpers;

/**
 * @property string $address_line_1
 * @property string $address_line_2
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $country
 * */
class SybSmartAddress implements \JsonSerializable
{
    private $data;

    /**
     * @param array $data
     * @return void
     * */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param string $name
     * @return string
     * */
    public function __get($name)
    {
        return $this->data[$name] ?? null;
    }

    /**
     * @param string $name
     * @param string $value
     * @return void
     * */
    public function __set($name, $value)
    {
        switch ($name) {
            case 'address_line_1':
            case 'address_line_2':
            case 'city':
            case 'state':
            case 'zip':
            case 'country':
                $this->data[$name] = $value;
        }
    }

    /** get converted array key->value
     * @return array
     * */
    public function getValues()
    {
        return $this->data;
    }

    /** \JsonSerializable
     * @return array
     * */
    public function jsonSerialize()
    {
        return $this->data;
    }

    /**
     * @return string
     * */
    public function __toString()
    {
        return (string)json_encode($this->data);
    }
}
