<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\CF;

use Anemone\Contracts\BeCustomField;
use Anemone\Models\CF\Helpers\SubLegalEntity;

class LegalEntityCustomField extends BaseCustomField implements BeCustomField
{
    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->type = 'legal_entity';
    }

    /** get array in objects Anemone\Models\CF\Helpers\SubLegalEntity or arrays
     * @param bool $isArray
     * @return array
     * */
    public function getValue($isArray = false)
    {
        return $isArray ? $this->getArrayWithoutValue(false) : $this->getArrayWithoutValue();
    }

    /** set value (replaced)
     * ex.:
     * v.1
     * ...
     * $legalEntityCF->setValue(function (array $values) {
     *
     *  $newLE = new Anemone\Models\CF\Helpers\SubLegalEntity();
     *  $newLE->name = 'OOO name';
     *  $newLE->vat_id = 12345;
     *  $newLE->kpp = 54321;
     *  $values[] = $newLE;
     *
     *  return $values;
     * });
     *
     * v.2
     * ...
     * $legalEntityCF->setValue([
     *  [
     *      'name' => 'OOO name',
     *      'vat_id' => 12345,
     *      'kpp' => 54321,
     *  ],
     *  // ...
     * ]);
     *
     * @param array|\Closure $closure
     * @return void
     * */
    public function setValue($closure)
    {
        if ($closure instanceof \Closure) {
            $array = $closure($this->getArrayWithoutValue());
            $this->values = array_map(function (SubLegalEntity $item) {
                return [
                    'value' => $item->getValues(),
                ];
            }, $array);
            return;
        }

        if (is_array($closure)) {
            $this->values = array_map(function ($item) {
                return [
                    'value' => $item,
                ];
            }, $closure);
        }
    }

    /** helper convert
     * @param bool $inObject
     * @return array
     * */
    protected function getArrayWithoutValue(bool $inObject = true)
    {
        return array_map(function ($item) use ($inObject) {
            return $inObject ? new SubLegalEntity($item['value']) : $item['value'];
        }, $this->values ?? []);
    }
}
