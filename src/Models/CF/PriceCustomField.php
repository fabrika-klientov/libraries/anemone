<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.01.27
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\CF;

use Anemone\Contracts\BeCustomField;

class PriceCustomField extends BaseCustomField implements BeCustomField
{
    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->type = 'price';
    }

    /** numeric
     * @return int
     * */
    public function getValue()
    {
        return empty($this->values) ? null : head($this->values)['value'];
    }

    /** numeric
     * @param int $data
     * @return void
     * */
    public function setValue($data)
    {
        $this->values = [
            ['value' => $data],
        ];
    }
}
