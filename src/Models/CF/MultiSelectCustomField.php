<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\CF;

use Anemone\Contracts\BeCustomField;
use Anemone\Models\CF\Helpers\HasEnums;

class MultiSelectCustomField extends BaseCustomField implements BeCustomField
{
    use HasEnums;

    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->type = 'multiselect';
    }

    /**
     * @return array|null
     * */
    public function getValue()
    {
        return empty($this->values) ? null : array_map(
            function ($item) {
                return $item['value'] ?? $this->getValueEnums($item) ?? null;
            },
            $this->values
        );
    }

    /**
     * @param string|string[] $data
     * @param bool $replace
     * @return void
     */
    public function setValue($data, bool $replace = true)
    {
        if (!is_array($data)) {
            $data = [$data];
        }

        if (!$replace && !empty($this->values)) {
            $data = array_merge(
                $data,
                array_map(
                    function ($item) {
                        return $item['value'] ?? null;
                    },
                    $this->values
                )
            );
        }

        $this->values = array_reduce(
            $data,
            function ($result, $value) {
                $key = $this->getInEnums($value);
                if (isset($key)) {
                    $result[] = ['value' => $value, 'enum_id' => $key];
                }

                return $result;
            },
            []
        );
    }
}
