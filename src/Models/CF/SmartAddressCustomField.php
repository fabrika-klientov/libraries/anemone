<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\CF;

use Anemone\Contracts\BeCustomField;
use Anemone\Models\CF\Helpers\SybSmartAddress;

/**
 * @property-read array $subtypes
 * */
class SmartAddressCustomField extends BaseCustomField implements BeCustomField
{
    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->type = 'smart_address';
    }

    /**
     * @param bool $isArray
     * @return SybSmartAddress|array
     * */
    public function getValue($isArray = false)
    {
        return $isArray ? $this->convertToKeyVal() : new SybSmartAddress($this->convertToKeyVal());
    }

    /** set value
     * @param array|\Closure $closure
     * @return void
     * */
    public function setValue($closure)
    {
        if ($closure instanceof \Closure) {
            $subSmartAddress = new SybSmartAddress($this->convertToKeyVal());
            $closure($subSmartAddress);
            $this->values = $this->convertAmoVal($subSmartAddress->getValues());
            return;
        }

        if (is_array($closure)) {
            $this->values = $this->convertAmoVal($closure);
        }
    }

    /** helper converting key->value
     * @return array
     * */
    protected function convertToKeyVal()
    {
        return array_reduce($this->values ?? [], function ($result, $item) {
            $result[$item['subtype']] = $item['value'];
            return $result;
        }, []);
    }

    /** helper for revert converting value
     * @param array $keyVal
     * @return array
     * */
    protected function convertAmoVal($keyVal)
    {
        return array_map(function ($value, $key) {
            return [
                'subtype' => $key,
                'value' => $value,
            ];
        }, $keyVal, array_keys($keyVal));
    }
}
