<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\CF;

use Anemone\Contracts\BeCustomField;
use Anemone\Models\CF\Helpers\HasEnums;

class MultiTextCustomField extends BaseCustomField implements BeCustomField
{
    use HasEnums;

    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->type = 'multitext';
    }

    /** for email phone
     * @param string $enum
     * @param bool $first
     * @return string|int|array|null
     */
    public function getValue($enum = 'WORK', bool $first = true)
    {
        $id = $this->getIdInEnums($enum);
        if (isset($id, $this->values)) {
            $result = [];
            foreach ($this->values as $value) {
                if ($value['enum_id'] == $id) {
                    $result[] = $value['value'];
                }
                if ($first && count($result) > 0) {
                    return head($result);
                }
            }

            return $result;
        }

        return null;
    }

    /** for email phone
     * @param string|int $data
     * @param string $enum
     * @param bool $replace
     * @return void
     */
    public function setValue($data, $enum = 'WORK', bool $replace = false)
    {
        if (!empty($this->values) && $replace) {
            $this->values = array_filter(
                $this->values,
                function ($item) use ($enum) {
                    return $item['enum_id'] != $enum && $item['enum_id'] != $this->getIdInEnums($enum);
                }
            );
        }

        $this->values = array_merge(
            $this->values ?? [],
            [
                [
                    'value' => $data,
                    'enum_id' => $this->getIdInEnums($enum),
                    'enum_code' => $enum,
                ],
            ]
        );
    }

    /** helper
     * @param string $enum
     * @return int|null
     * */
    protected function getIdInEnums($enum)
    {
        return array_reduce(
            $this->enums,
            function ($result, $item) use ($enum) {
                return $result ?? ($item['value'] == $enum ? $item['id'] : $result);
            },
            null
        );
    }

    /** helper
     * @param int $id
     * @return string|null
     * */
    protected function getCodeForId($id)
    {
        return array_reduce(
            $this->enums,
            function ($result, $item) use ($id) {
                return $result ?? ($item['id'] == $id ? $item['value'] : $result);
            },
            null
        );
    }
}
