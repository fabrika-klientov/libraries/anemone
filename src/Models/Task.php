<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeModel;
use Anemone\Models\Helpers\WithNotes;

/**
 * @property-read int $id
 * @property string $text
 * @property int $responsible_user_id
 * @property int $created_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $updated_by
 * @property int $entity_id
 * @property string $entity_type
 * @property string $group_id
 * @property int $duration
 * @property string $complete_till
 * @property string $task_type_id
 * @property bool $is_completed
 * @property-read int $account_id
 * @property array $result
 *
 * @method __construct(\Anemone\Models\Instances\TasksInstance $instance = null, array $resource = [])
 * @method $this setInstance(\Anemone\Models\Instances\TasksInstance $instance)
 * */
class Task extends Model implements BeModel
{
    use WithNotes;
}
