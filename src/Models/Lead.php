<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeModel;
use Anemone\Contracts\BePersist;
use Anemone\Models\Helpers;

/**
 * @property-read int $id
 * @property string $name
 * @property int $price
 * @property int $responsible_user_id
 * @property string $group_id
 * @property int $status_id
 * @property int $pipeline_id
 * @property string $loss_reason_id
 * @property string $source_id
 * @property-read int $created_by
 * @property int $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $closed_at
 * @property string $closest_task_at
 * @property bool $is_deleted
 * @property string $score
 * @property int $account_id
 * @property-read string $is_price_modified_by_robot
 * @property array $_embedded
 * @property \Anemone\Core\Collection\Collection $custom_fields_values
 *
 * @method __construct(\Anemone\Models\Instances\LeadsInstance $instance = null, array $resource = [])
 * @method $this setInstance(\Anemone\Models\Instances\LeadsInstance $instance)
 * */
class Lead extends Model implements BeModel, BePersist
{
    use Helpers\WithContacts;
    use Helpers\WithCompany;
    use Helpers\WithCatalogElements;
    use Helpers\WithTags;
    use Helpers\WithCF;
    use Helpers\WithNotes;
    use Helpers\WithTasks;
    use Helpers\Link;
    use Helpers\Persisted;
}
