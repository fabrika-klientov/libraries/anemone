<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeModel;

/**
 * @property-read string $id
 * @property string $type
 * @property-read array $entity_id
 * @property-read array $entity_type
 * @property-read string $created_at
 * @property-read string $created_by
 * @property-read array $value_after
 * @property-read array $value_before
 * @property-read int $account_id
 * @property-read array $_embedded
 *
 * @method __construct(\Anemone\Models\Instances\EventsInstance $instance = null, array $resource = [])
 * @method $this setInstance(\Anemone\Models\Instances\EventsInstance $instance)
 * */
class Event extends Model implements BeModel
{

}
