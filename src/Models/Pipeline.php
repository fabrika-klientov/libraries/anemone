<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeBaseModel;
use Anemone\Core\Collection\Collection;

/**
 * @property-read int $id
 * @property string $name
 * @property int $sort
 * @property bool $is_main
 * @property bool $is_unsorted_on
 * @property-read int $account_id
 * @property array $_embedded
 * */
class Pipeline extends Base implements BeBaseModel
{
    /**
     * @return Collection
     * */
    public function statuses()
    {
        return (new Collection($this->_embedded['statuses'] ?? []))
            ->map(function ($item) {
                unset($item['_links']);
                return new StatusPL($item);
            });
    }
}
