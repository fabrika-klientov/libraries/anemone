<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Instances;

use Anemone\Client;
use Anemone\Core\Adapters\ServiceAdapter;
use Anemone\Core\Builder\Builder;
use Anemone\Core\Collection\Collection;
use Anemone\Core\Helpers\MoreEntities;
use Anemone\Core\Helpers\ParseEntityWithId;
use Anemone\Exceptions\AnemoneException;
use Anemone\Exceptions\InvalidDataException;
use Illuminate\Support\Str;

abstract class ModelInstance
{
    use MoreEntities;
    use ParseEntityWithId;

    /**
     * @var Client $client
     * */
    protected $client;

    /**
     * @var ServiceAdapter $serviceAdapter
     * */
    protected $serviceAdapter;

    /**
     * @var \Anemone\Core\Cache\CacheService $cacheService
     * */
    protected $cacheService;

    /**
     * @var Builder $builder
     * */
    protected $builder;

    /**
     * @var string $currentVersion
     * */
    protected $currentVersion = '4';

    /**
     * @var array $keyPaths
     * */
    protected $keyPaths = [
        '_select' => 'path',
        '_insert' => 'path',
        '_update' => 'path',
        '_destroy' => 'path',
    ];

    /**
     * @var bool $isCaching
     * */
    protected $isCaching = true; // local ON or OFF cache

    /**
     * @var int|null $entityId
     * */
    protected $entityId = null;

    /**
     * @param Client $client
     * @return void
     * */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->serviceAdapter = new ServiceAdapter($this, $this->currentVersion, self::getParsedEntityName());
        $this->builder = new Builder();
        $this->cacheService = $client->getCacheService();
    }

    /** request method for get data
     * exceptions not light
     * @param int $maxPages
     * @return Collection
     * */
    public function get($maxPages = -1)
    {
        $keySelect = '_select';
        $path = $this->getPath($keySelect);

        // from cache
        if ($this->isCaching) {
            $cacheResult = $this->cacheService->get($this->builder, $this->currentVersion, $this->entity());
            if (isset($cacheResult)) { // get cached data
                $this->serviceAdapter->fromCache(true);
                $data = $this->serviceAdapter->getData(json_decode((string)json_encode($cacheResult), true));

                $this->client->getManagingService()->persist($data);

                return $data;
            }
        }

        // loading from server
        $mergedCollection = new Collection();
        while (true) {
            if ($isMore = $this->isMore()) {
                $this->next();
            }

            if ($maxPages > 0 && $this->page > $maxPages) { // set max count pages for get
                break;
            }

            $result = $this->client
                ->getQueryService()
                ->{$this->getMethod($keySelect)}(
                    $path,
                    $this->serviceAdapter->getSelectRequest($this->builder)
                );
            $collection = $this->serviceAdapter->getData(json_decode($result, true));

            $mergedCollection = $mergedCollection->merge($collection);
            if (!$isMore) {
                break;
            }

            $this->clearLimit();

            if ($collection->isEmpty()) {
                break;
            }

            // if bag (duplicate)
            if ($mergedCollection
                    ->duplicates(
                        function ($item) {
                            return data_get($item, 'id');
                        }
                    )
                    ->count() > $this->countDuplicates && // all entity
                $mergedCollection
                    ->duplicates(
                        function ($item) {
                            return data_get($item, 'uid');
                        }
                    )
                    ->count() > $this->countDuplicates // for IncomingLead
            ) {
                throw new AnemoneException('Error:: loaded duplicate.');
            }
        }

        $this->clearLimit(true); // for next request in current instance

        // update or add to cache
        if ($this->isCaching) {
            $this->cacheService->set($mergedCollection, $this->currentVersion, $this->entity());
        }

        $this->client->getManagingService()->persist($mergedCollection);

        return $mergedCollection;
    }

    /** request method for store or (and) update data
     * exceptions not light
     * @param Collection|null $collect
     * @return bool
     * */
    public function save(Collection $collect = null)
    {
        if (is_null($collect)) {
            return false;
        }

        $this->serviceAdapter->fromCache(false);
        /**
         * @var Collection $storeCollect
         * @var Collection $updateCollect
         * */
        [$storeCollect, $updateCollect] = $collect->reduce(
            function ($result, $item) {
                $result[isset($item->id) ? 1 : 0]->add($item);
                return $result;
            },
            [new Collection(), new Collection()]
        );

        if ($storeCollect->isNotEmpty()) {
            $keyInsert = '_insert';
            $path = $this->getPath($keyInsert);

            $storeData = $this->serviceAdapter->getInsertRequest($storeCollect);
            if ($this->checkAfterOpt($storeData)) {
                $result = $this->client
                    ->getQueryService()
                    ->{$this->getMethod($keyInsert, 'POST')}($path, $storeData);
                $this->serviceAdapter->mergeInsertResponse($storeCollect, json_decode($result, true));
            }
        }

        if ($updateCollect->isNotEmpty()) {
            $keyUpdate = '_update';
            $path = $this->getPath($keyUpdate);

            $updateData = $this->serviceAdapter->getUpdateRequest($updateCollect);
            if ($this->checkAfterOpt($updateData)) {
                $result = $this->client
                    ->getQueryService()
                    ->{$this->getMethod($keyUpdate, 'PATCH')}($path, $updateData);
                $this->serviceAdapter->mergeUpdateResponse($updateCollect, json_decode($result, true));
            }
        }

        // update or add to cache
        if ($this->isCaching) {
            $this->cacheService->set($collect, $this->currentVersion, $this->entity());
        }

        $this->client->getManagingService()->persist($collect);

        return true;
    }

    /** request method for destroy data
     * exceptions not light
     * @param Collection $collect
     * @return bool
     * */
    public function delete(Collection $collect = null)
    {
        $this->serviceAdapter->fromCache(false);
        if (is_null($collect) || $collect->isEmpty()) {
            return false;
        }

        $validate = $collect->every(
            function ($item) {
                return isset($item->id);
            }
        );
        if (!$validate) {
            throw new InvalidDataException(
                'Models in collect should be isset in Amo server and have $id property'
            );
        }

        $keyDestroy = '_destroy';
        $path = $this->getPath($keyDestroy);

        $result = $this->client->getQueryService()->{$this->getMethod($keyDestroy, 'POST')}(
            $path,
            $this->serviceAdapter->getDeleteRequest($collect)
        );
        $this->serviceAdapter->mergeDeleteResponse($collect, json_decode($result, true));

        // update or add to cache
        if ($this->isCaching) {
            $this->cacheService->set($collect, $this->currentVersion, $this->entity(), true);
        }

        $this->client->getManagingService()->detach($collect);

        return true;
    }

    /** get Client
     * @return Client
     * */
    public function getClient()
    {
        return $this->client;
    }

    /** set other version
     * @param string $version
     * @return $this
     * */
    public function setVersion(string $version)
    {
        $this->currentVersion = $version;
        $this->serviceAdapter = new ServiceAdapter($this, $this->currentVersion, self::getParsedEntityName());

        return $this;
    }

    /** get method
     * @param string $key
     * @param string $default
     * @return string
     * */
    protected function getMethod(string $key, $default = 'GET')
    {
        $method = aEntryData($this->currentVersion . '.' . self::getParsedEntityName() . '.' . $key);
        return Str::lower($method ?? $default);
    }

    /** get smart path
     * @param string $keyPath
     * @return string
     * @throws AnemoneException
     * */
    protected function getPath(string $keyPath)
    {
        $entry = aEntryData($this->currentVersion . '.' . self::getParsedEntityName());
        $key = $this->keyPaths[$keyPath];

        if (is_null($key)) {
            throw new AnemoneException(
                'Model entity [' . self::getParsedEntityName() . '] does\'t support method [' . $key . ']'
            );
        }

        $path = $entry[$key] ?? null;
        if (isset($path)) {
            return $path;
        }

        throw new AnemoneException(
            'Path not found for model entity ['
            . self::getParsedEntityName() . '] and for method [' . $key . '] in version [' . $this->currentVersion . ']'
        );
    }

    /**
     * @param mixed|array|Collection $data
     * @return bool
     */
    protected function checkAfterOpt($data): bool
    {
        if (is_array($data)) {
            return !empty($data);
        }

        if ($data instanceof Collection) {
            return $data->isNotEmpty();
        }

        return !empty($data);
    }

    /**
     * @return ServiceAdapter
     * */
    public function getServiceAdapter()
    {
        return $this->serviceAdapter;
    }

    /** code of entity
     * @return string
     * */
    abstract protected function entity();
}
