<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.16
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Instances;

use Anemone\Client;
use Anemone\Contracts\BeInstanceModel;
use Anemone\Core\Builder\Helpers\HaveLimits;
use Anemone\Core\Builder\Helpers\HaveQuery;
use Anemone\Core\Helpers\CFOnly;
use Anemone\Exceptions\InvalidDataException;
use Anemone\Models\Catalog;

class ElementsInstance extends ModelInstance implements BeInstanceModel
{
    use HaveLimits;
    use CFOnly;
    use HaveQuery;

    /**
     * @var array $keyPaths
     * */
    protected $keyPaths = [
        '_select' => 'get',
        '_insert' => 'add',
        '_update' => 'update',
    ];

    /**
     * @var string $currentModelAction
     * */
    protected $currentModelAction = 'element';

    /**
     * @var int $entityId
     * */
    protected $entityId;

    /**
     * @param Client $client
     * @param Catalog|int $catalog
     * @throws InvalidDataException
     */
    public function __construct(Client $client, $catalog)
    {
        parent::__construct($client);

        if ($catalog instanceof Catalog) {
            $this->entityId = $catalog->id;
        }

        if (empty($this->entityId) && is_scalar($catalog)) {
            $this->entityId = (int)$catalog;
        }

        if (empty($this->entityId)) {
            throw new InvalidDataException('Catalog should be exist');
        }

        $this->currentModelAction = $this->currentModelAction . '_' . $this->entityId;
        $this->supportCFEntities[] = $this->entity();
    }

    /** id(s)
     * @param int|array $id
     * @return $this
     * */
    public function find($id)
    {
        $this->builder->where('filter', [
            'id' => $id,
        ]);
        $this->builder->where('id', $id);

        return $this;
    }

    /**
     * @inheritDoc
     * */
    protected function getPath(string $keyPath)
    {
        $path = parent::getPath($keyPath);
        return str_replace('{catalog_id}', $this->entityId, $path);
    }

    /**
     * @return string
     * */
    protected function entity()
    {
        return $this->currentModelAction;
    }
}
