<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Instances;

use Anemone\Client;
use Anemone\Contracts\BeInstanceModel;
use Anemone\Core\Builder\Helpers\HaveId;
use Anemone\Core\Builder\Helpers\HaveLimits;
use Anemone\Core\Collection\Collection;

class NotesInstance extends ModelInstance implements BeInstanceModel
{
    use HaveId;
    use HaveLimits;

    /**
     * @var array $keyPaths
     * */
    protected $keyPaths = [
        '_select' => 'get',
        '_insert' => 'add',
        '_update' => 'update',
    ];
    /**
     * @var string $coreModel
     * */
    protected $coreModel;
    /**
     * @var int $elementID
     * */
    protected $elementID;

    public function __construct(Client $client, string $model)
    {
        parent::__construct($client);
        $this->coreModel = $model;
    }

    public function save(Collection $collect = null)
    {
        if (is_null($collect)) {
            return false;
        }

        $queries = $this->builder->getResult();
        $entityID = null;
        if (!empty($queries['entity_id'])) {
            $entityID = $queries['entity_id'];
        }

        $collect->each(
            function ($item) use ($entityID) {
                if (isset($entityID)) {
                    $item->entity_id = $entityID;
                }
            }
        );

        return parent::save($collect);
    }

    /**
     * @inheritDoc
     * */
    protected function getPath(string $keyPath)
    {
        return str_replace(
            '{entity_type}',
            isset($this->elementID) ? ($this->getTypeOfEntity() . '/' . $this->elementID) : $this->getTypeOfEntity(),
            parent::getPath($keyPath)
        );
    }

    /** for id entity
     * @param int $id
     * @return $this
     * */
    public function entityID($id)
    {
        $this->elementID = $id;
        return $this;
    }

    /** type note
     * @deprecated (don't work (see in future))
     *  4        COMMON        Обычное примечание
     *  13        TASK_RESULT Результат по задаче
     *  25        SYSTEM        Системное сообщение
     *  102    SMS_IN        Входящее смс
     *  103    SMS_OUT    Исходящее смс
     * @param int $type
     * @return $this
     * */
    public function noteType($type)
    {
        $this->builder->where('note_type', $type);
        return $this;
    }

    protected function entity()
    {
        return 'note';
    }

    /** type ID of entity
     * @return string
     * */
    protected function getTypeOfEntity()
    {
        switch ($this->coreModel) {
            case 'contact':
            case 'lead':
            case 'task':
            case 'customer':
                return $this->coreModel . 's';
            case 'company':
                return 'companies';
        }

        return '';
    }
}
