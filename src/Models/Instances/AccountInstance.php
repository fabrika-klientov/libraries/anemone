<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Instances;

use Anemone\Core\Builder\Helpers\HaveWith;
use Anemone\Core\Collection\Collection;

class AccountInstance extends ModelInstance
{
    use HaveWith;

    /**
     * @var string[] $defaultWith
     * */
    protected $defaultWith = [
        'amojo_id',
        'amojo_rights',
        'users_groups',
        'task_types',
        'version',
        'entity_names',
        'datetime_settings',
    ];
    /**
     * @var string
     * */
    protected $currentModelAction = 'account';

    /**
     * @return string
     * */
    protected function entity()
    {
        return $this->currentModelAction;
    }

    /**
     * @param int $clap
     * @return \Anemone\Models\Account|mixed
     */
    public function get($clap = -1)
    {
        $this->with($this->defaultWith);
        return parent::get()->first();
    }

    /** clap
     * @param Collection|null $collect
     * @return bool
     * */
    public function save(Collection $collect = null)
    {
        return false;
    }

    /**
     * @param Collection $collect
     * @return bool
     * */
    public function delete(Collection $collect = null)
    {
        return false;
    }
}
