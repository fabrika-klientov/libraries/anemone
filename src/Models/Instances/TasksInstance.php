<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Instances;

use Anemone\Contracts\BeInstanceModel;
use Anemone\Core\Builder\Helpers\HaveDataCreateModify;
use Anemone\Core\Builder\Helpers\HaveFilter;
use Anemone\Core\Builder\Helpers\HaveId;
use Anemone\Core\Builder\Helpers\HaveLimits;
use Anemone\Core\Builder\Helpers\HavePipelineStatus;
use Anemone\Core\Builder\Helpers\HaveResponsibleUser;
use Anemone\Core\Builder\Helpers\WithCount;
use Anemone\Models\Helpers\WithNotes;

class TasksInstance extends ModelInstance implements BeInstanceModel
{
    use HaveDataCreateModify;
    use HaveId;
    use HaveLimits;
    use HavePipelineStatus;
    use HaveResponsibleUser;
    use WithCount;
    use WithNotes;
    use HaveFilter;

    /**
     * @var array
     */
    protected $keyPaths = [
        '_select' => 'get',
        '_insert' => 'add',
        '_update' => 'update',
    ];

    protected function entity()
    {
        return 'task';
    }

    /**
     * @param bool $status true -> finish or not is false
     * @return $this
     * */
    public function withStatus(bool $status)
    {
        $this->builder->where('filter', [
            'status' => $status ? 1 : 0,
        ]);
        return $this;
    }

    /** by create user ids
     * @param array $id
     * @return $this
     * */
    public function createdBy(array $id)
    {
        $this->builder->where('filter', [
            'created_by' => $id,
        ]);
        return $this;
    }

    /** by task types
     * @param array $types
     * 1 	Call
     * 2 	Meet
     * 3 	Message
     * @return $this
     * */
    public function taskType(array $types)
    {
        $this->builder->where('filter', [
            'task_type' => $types,
        ]);
        return $this;
    }

    /** get for type entity
     * @param string $type lead / contact / company / customer
     * @return $this
     * */
    public function forTypeEntity(string $type)
    {
        $this->builder->where('type', $type);
        return $this;
    }

    /** get for entity id
     * @param int $id
     * @return $this
     * */
    public function forEntityID(int $id)
    {
        $this->builder->where('element_id', $id);
        return $this;
    }

    /** get count
     * @return int
     * */
    public function count()
    {
        $result = $this->countData();
        return $result['response']['summary']['summary_todos'] ?? 0;
    }
}
