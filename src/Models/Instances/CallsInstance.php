<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Instances;

use Anemone\Contracts\BeInstanceModel;
use Anemone\Core\Collection\Collection;

class CallsInstance extends ModelInstance implements BeInstanceModel
{
    /**
     * @var array $keyPaths
     * */
    protected $keyPaths = [
        '_insert' => 'add',
    ];

    /**
     * @param int $clap
     * @return Collection|null
     * @deprecated
     */
    public function get($clap = -1)
    {
        return null;
    }

    /**
     * @param Collection $collect
     * @return bool
     * @deprecated
     * */
    public function delete(Collection $collect = null)
    {
        return false;
    }

    protected function entity()
    {
        return 'call';
    }

}
