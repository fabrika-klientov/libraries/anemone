<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.17
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Instances;

use Anemone\Client;
use Anemone\Contracts\BeInstanceModel;
use Anemone\Core\Builder\Helpers\HaveId;
use Anemone\Core\Builder\Helpers\HaveLimits;
use Anemone\Core\Builder\Helpers\HaveQuery;

class TagsInstance extends ModelInstance implements BeInstanceModel
{
    use HaveId;
    use HaveLimits;
    use HaveQuery;

    /**
     * @var array $keyPaths
     * */
    protected $keyPaths = [
        '_select' => 'get',
        '_insert' => 'add',
    ];

    /**
     * @var string $currentModelAction
     * */
    protected $currentModelAction = 'tag';

    /**
     * @var string $coreModel
     * */
    protected $coreModel;

    public function __construct(Client $client, string $model)
    {
        parent::__construct($client);
        $this->coreModel = $model;
        $this->currentModelAction .= '_' . $model;
    }

    /**
     * @inheritDoc
     * */
    protected function getPath(string $keyPath)
    {
        return str_replace('{entity_type}', $this->getTypeOfEntity(), parent::getPath($keyPath));
    }

    protected function entity()
    {
        return $this->currentModelAction;
    }

    /** type ID of entity
     * @return string
     * */
    protected function getTypeOfEntity()
    {
        switch ($this->coreModel) {
            case 'contact':
            case 'lead':
            case 'customer':
                return $this->coreModel . 's';
            case 'company':
                return 'companies';
        }

        return '';
    }
}
