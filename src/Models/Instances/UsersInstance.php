<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Instances;

use Anemone\Client;
use Anemone\Contracts\BeInstanceModel;
use Anemone\Core\Builder\Helpers\HaveId;
use Anemone\Core\Builder\Helpers\HaveLimits;
use Anemone\Core\Builder\Helpers\HaveWith;
use Anemone\Core\Builder\Helpers\WithCount;

class UsersInstance extends ModelInstance implements BeInstanceModel
{
    use HaveId;
    use HaveLimits;
    use WithCount;
    use HaveWith;

    public function __construct(Client $client)
    {
        parent::__construct($client);
        $this->with(['role', 'group']);
    }

    /**
     * @var array
     */
    protected $keyPaths = [
        '_select' => 'get',
        '_insert' => 'add',
    ];

    protected function entity()
    {
        return 'user';
    }

    /** get count
     * @return int|void
     * */
    public function count()
    {
//        $result = $this->countData();
//        return $result['response']['summary']['summary_todos'] ?? 0;
    }
}
