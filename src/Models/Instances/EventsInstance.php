<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Instances;

use Anemone\Contracts\BeInstanceModel;
use Anemone\Core\Builder\Helpers\HaveDataCreateModify;
use Anemone\Core\Builder\Helpers\HaveId;
use Anemone\Core\Builder\Helpers\HaveLimits;
use Anemone\Core\Builder\Helpers\HaveWith;
use Anemone\Core\Collection\Collection;

class EventsInstance extends ModelInstance implements BeInstanceModel
{
    use HaveDataCreateModify;
    use HaveId;
    use HaveLimits;
    use HaveWith;

    /**
     * @var array $keyPaths
     * */
    protected $keyPaths = [
        '_select' => 'get',
        '_select_types' => 'get_types',
    ];
    /**
     * @var int $countDuplicates
     * */
    protected $countDuplicates = 100; // bug amocrm

    /**
     * @inheritDoc
     * @deprecated
     * */
    public function save(Collection $collect = null)
    {
        return false;
    }

    /**
     * @inheritDoc
     * @deprecated
     * */
    public function delete(Collection $collect = null)
    {
        return false;
    }

    /**
     * @param string $language_code
     * @return Collection
     */
    public function types(string $language_code = 'ru')
    {
        $keySelect = '_select_types';
        $path = $this->getPath($keySelect);

        $result = json_decode(
            $this->client
                ->getQueryService()
                ->{$this->getMethod($keySelect, 'GET')}(
                    $path,
                    ['language_code' => $language_code]
                ),
            true
        );

        return new Collection($result['_embedded']['events_types'] ?? []);
    }

    /** by create user ids
     * @param array $id
     * @return $this
     * */
    public function createdBy(array $id)
    {
        $this->builder->where(
            'filter',
            [
                'created_by' => $id,
            ]
        );
        return $this;
    }

    /** by entities types lead, contact, company, customer, task, catalog_{CATALOG_ID}
     * @param array $entities
     * @return $this
     * */
    public function entities(array $entities)
    {
        $this->builder->where(
            'filter',
            [
                'entity' => $entities,
            ]
        );
        return $this;
    }

    /** by id (require entities method)
     * @param array $id
     * @return $this
     * */
    public function entityId(array $id)
    {
        $this->builder->where(
            'filter',
            [
                'entity_id' => $id,
            ]
        );
        return $this;
    }

    /** types
     * @param array $types
     * @return $this
     * */
    public function type(array $types)
    {
        $this->builder->where(
            'filter',
            [
                'type' => $types,
            ]
        );
        return $this;
    }

    /** values
     * @param array $values
     * @return $this
     * */
    public function valueAfter(array $values)
    {
        $this->builder->where(
            'filter',
            [
                'value_after' => $values,
            ]
        );
        return $this;
    }

    /** values
     * @param array $values
     * @return $this
     * */
    public function valueBefore(array $values)
    {
        $this->builder->where(
            'filter',
            [
                'value_before' => $values,
            ]
        );
        return $this;
    }

    protected function entity()
    {
        return 'event';
    }

}
