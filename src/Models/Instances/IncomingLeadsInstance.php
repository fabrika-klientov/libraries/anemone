<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Instances;

use Anemone\Contracts\BeInstanceModel;
use Anemone\Core\Builder\Builder;
use Anemone\Core\Builder\Helpers\HaveLimits;
use Anemone\Core\Collection\Collection;
use Anemone\Models\IncomingLead;

class IncomingLeadsInstance extends ModelInstance implements BeInstanceModel
{
    use HaveLimits;

    /**
     * @var array $keyPaths
     * */
    protected $keyPaths = [
        '_select' => 'get',
        '_select_summary' => 'get_summary',
        '_insert_sip' => 'add_sip',
        '_insert_form' => 'add_form',
        '_accept' => 'accept',
        '_decline' => 'decline',
    ];

    public function save(Collection $collect = null)
    {
        if (is_null($collect)) {
            return false;
        }

        $this->serviceAdapter->fromCache(false);

        [$sipCollect, $formCollect] = $collect->reduce(
            function ($result, $item) {
                if (isset($item->uid)) {
                    return $result;
                }
                if ($this->validateSip($item)) {
                    $result[0]->add($item);
                } elseif ($this->validateForm($item)) {
                    $result[1]->add($item);
                }
                return $result;
            },
            [new Collection(), new Collection()]
        );


        if ($sipCollect->isNotEmpty()) {
            $keyInsert = '_insert_sip';
            $path = $this->getPath($keyInsert);

            $result = $this->client->getQueryService()->{$this->getMethod($keyInsert, 'POST')}(
                $path,
                $this->serviceAdapter->getInsertRequest($sipCollect)
            );
            $this->serviceAdapter->mergeInsertResponse($sipCollect, json_decode($result, true));
        }

        if ($formCollect->isNotEmpty()) {
            $keyInsert = '_insert_form';
            $path = $this->getPath($keyInsert);

            $result = $this->client->getQueryService()->{$this->getMethod($keyInsert, 'POST')}(
                $path,
                $this->serviceAdapter->getInsertRequest($formCollect)
            );
            $this->serviceAdapter->mergeInsertResponse($formCollect, json_decode($result, true));
        }

        // update or add to cache
        if ($this->isCaching) {
            $this->cacheService->set(
                $collect->filter(
                    function ($item) {
                        return $this->validateSip($item) || $this->validateForm($item);
                    }
                ),
                $this->currentVersion,
                $this->entity()
            );
        }

        return true;
    }

    /** get summary data of IncomingLeads
     * @param string|int|null $from
     * @param string|int|null $to
     * @return array
     * */
    public function summary($from = null, $to = null)
    {
        $key = '_select_summary';
        $path = $this->getPath($key);

        $builder = new Builder();
        if (isset($from) || isset($to)) {
            $aFrom = isset($from) ? ['from' => $from] : [];
            $aTo = isset($to) ? ['to' => $to] : [];
            $builder->where('data', ['filter' => ['date' => array_merge($aFrom, $aTo)]]);
        }

        $result = $this->client->getQueryService()->{$this->getMethod($key, 'GET')}(
            $path,
            $this->serviceAdapter->getSelectRequest($builder)
        );

        return json_decode($result, true);
    }

    /** accepting IncomingLeads
     * @param Collection $collect
     * @param int $userId
     * @param int $statusId
     * @return Collection|null
     * */
    public function accept(Collection $collect, $userId = null, $statusId = null)
    {
        $key = '_accept';
        $path = $this->getPath($key);

        $accepted = $collect->filter(
            function ($item) {
                return isset($item->uid);
            }
        );

        if ($accepted->isNotEmpty()) {
            $params = array_merge($userId ? ['user_id' => $userId] : [], $statusId ? ['status_id' => $statusId] : []);

            return $accepted->reduce(
                function ($result, IncomingLead $incomingLead) use ($path, $key, $params) {
                    $data = $this->client->getQueryService()->{$this->getMethod($key, 'POST')}(
                        str_replace('{uid}', $incomingLead->uid, $path),
                        $params
                    );

                    return isset($data['_embedded']) ? $result->merge($data['_embedded']) : $result;
                },
                new Collection()
            );
        }

        return null;
    }

    /** declining IncomingLeads
     * @param Collection $collect
     * @param int $userId
     * @return Collection|null
     * */
    public function decline(Collection $collect, $userId = null)
    {
        $key = '_decline';
        $path = $this->getPath($key);

        $declined = $collect->filter(
            function ($item) {
                return isset($item->uid);
            }
        );

        if ($declined->isNotEmpty()) {
            return $declined->reduce(
                function ($result, IncomingLead $incomingLead) use ($path, $key, $userId) {
                    $data = $this->client->getQueryService()->{$this->getMethod($key, 'DELETE')}(
                        str_replace('{uid}', $incomingLead->uid, $path),
                        ['user_id' => $userId]
                    );

                    return isset($data['_embedded']) ? $result->merge($data['_embedded']) : $result;
                },
                new Collection()
            );
        }

        return null;
    }

    /** clap
     * @param Collection $collect
     * @return bool
     * */
    public function delete(Collection $collect = null)
    {
        return false;
    }

    /** filter for categories
     * @param array $data [sip, mail, forms, chats]
     * @return $this
     * */
    public function categories(array $data)
    {
        if (!empty($data)) {
            $this->builder->where('filter', ['category' => $data]);
        }
        return $this;
    }

    /**
     * @param mixed $data
     * @return IncomingLeadsInstance
     */
    public function uid($data)
    {
        if (!empty($data)) {
            $this->builder->where('filter', ['uid' => $data]);
        }
        return $this;
    }

    /**
     * @param int $data
     * @return IncomingLeadsInstance
     */
    public function pipeline($data)
    {
        if (!empty($data)) {
            $this->builder->where('filter', ['pipeline_id' => $data]);
        }
        return $this;
    }

    /** order by for categories
     * @param string $field
     * @param string $direction
     * @return $this
     * */
    public function orderBy(string $field, string $direction = 'asc')
    {
        $this->builder->where('order', [$field => $direction]); // > ??
        return $this;
    }

    protected function entity()
    {
        return 'incomingLead';
    }

    /** validating entity for SIP
     * @param IncomingLead $incomingLead
     * @return bool
     * */
    protected function validateSip(IncomingLead $incomingLead)
    {
        $info = $incomingLead->metadata;
        return isset(
            $incomingLead->source_name,
            $incomingLead->source_uid,
            $info['uniq'],
            $info['duration'],
            $info['link'],
            $info['phone'],
            $info['from'],
            $info['called_at'],
            $info['service_code']
        );
    }

    /** validating entity for FORM
     * @param IncomingLead $incomingLead
     * @return bool
     * */
    protected function validateForm(IncomingLead $incomingLead)
    {
        $info = $incomingLead->metadata;
        return isset(
            $incomingLead->source_name,
            $incomingLead->source_uid,
            $info['form_name'],
            $info['form_id'],
            $info['form_page'],
            $info['ip'],
            $info['form_sent_at']
        );
    }
}
