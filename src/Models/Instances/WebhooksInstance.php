<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Instances;

use Anemone\Contracts\BeInstanceModel;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Webhook;

class WebhooksInstance extends ModelInstance implements BeInstanceModel
{
    /**
     * @var array $keyPaths
     * */
    protected $keyPaths = [
        '_select' => 'get',
        '_insert' => 'add',
        '_destroy' => 'delete',
    ];

    public function save(Collection $collect = null)
    {
        if (empty($collect)) {
            return false;
        }

        return $collect->reduce(
            function ($status, Webhook $webhook) {
                $result = parent::save(new Collection([$webhook]));
                return $status && !empty($result);
            },
            true
        );
    }

    /**
     * @param Collection|null $collect
     * @return bool
     * @throws \Exception
     */
    public function delete(Collection $collect = null)
    {
        if (empty($collect)) {
            return false;
        }

        $keyDestroy = '_destroy';
        $path = $this->getPath($keyDestroy);

        return $collect->reduce(
            function ($status, Webhook $webhook) use ($path) {
                $this->client
                    ->getQueryService()
                    ->delete(
                        $path,
                        [],
                        ['json' => $this->serviceAdapter->getDeleteRequest(new Collection([$webhook]))]
                    );
                return $status;
            },
            true
        );
    }

    /**
     * @return string
     * */
    protected function entity()
    {
        return 'webhook';
    }

}
