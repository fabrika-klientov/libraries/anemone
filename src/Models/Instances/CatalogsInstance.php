<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.16
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Instances;

use Anemone\Contracts\BeInstanceModel;
use Anemone\Core\Builder\Helpers\HaveLimits;
use Anemone\Models\Catalog;

class CatalogsInstance extends ModelInstance implements BeInstanceModel
{
    use HaveLimits;

    /**
     * @var array $keyPaths
     * */
    protected $keyPaths = [
        '_select' => 'get',
        '_insert' => 'add',
        '_update' => 'update',
    ];

    /**
     * @var string $currentModelAction
     * */
    protected $currentModelAction = 'catalog';

    /**
     * @param bool $isElementsInstance
     * @return Catalog|ElementsInstance|null
     */
    public function products(bool $isElementsInstance = true)
    {
        $model = self::get()->first(function (Catalog $item) {
            return $item->type == 'products';
        });

        if (empty($model)) {
            return null;
        }

        if (!$isElementsInstance) {
            return $model;
        }

        return $model->elements();
    }
    
    /**
     * @return string
     * */
    protected function entity()
    {
        return $this->currentModelAction;
    }
}
