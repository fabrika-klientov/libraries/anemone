<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Instances;

use Anemone\Client;
use Anemone\Contracts\BeInstanceModel;
use Anemone\Core\Builder\Filter\FilterCF;
use Anemone\Core\Builder\Filter\FilterCreatedAt;
use Anemone\Core\Builder\Filter\FilterCreatedBy;
use Anemone\Core\Builder\Filter\FilterId;
use Anemone\Core\Builder\Filter\FilterName;
use Anemone\Core\Builder\Filter\FilterPipeline;
use Anemone\Core\Builder\Filter\FilterResponsible;
use Anemone\Core\Builder\Filter\FilterStatuses;
use Anemone\Core\Builder\Filter\FilterUpdatedBy;
use Anemone\Core\Builder\Helpers\HaveDataCreateModify;
use Anemone\Core\Builder\Helpers\HaveId;
use Anemone\Core\Builder\Helpers\HaveLimits;
use Anemone\Core\Builder\Helpers\HaveQuery;
use Anemone\Core\Builder\Helpers\HaveStatus;
use Anemone\Core\Builder\Helpers\HaveTasks;
use Anemone\Core\Builder\Helpers\HaveWith;
use Anemone\Core\Builder\Helpers\WithCount;
use Anemone\Core\Helpers\CFEntities;
use Anemone\Core\Helpers\Complex;
use Anemone\Core\Helpers\LinkEntities;
use Anemone\Core\Helpers\Pipelines;
use Anemone\Models\Helpers\HaveTags;
use Anemone\Models\Helpers\WithNotes;

class LeadsInstance extends ModelInstance implements BeInstanceModel
{
    use CFEntities;
    use HaveDataCreateModify;
    use HaveId;
    use HaveLimits;
    use HaveQuery;
    use HaveStatus;
    use HaveTags;
    use HaveTasks;
    use HaveWith;
    use LinkEntities;
    use Pipelines;
    use WithCount;
    use WithNotes;
    use FilterId;
    use FilterName;
    use FilterStatuses;
    use FilterPipeline;
    use FilterCreatedBy;
    use FilterUpdatedBy;
    use FilterResponsible;
    use FilterCreatedAt;
    use FilterCF;
    use Complex;

    /**
     * @var array $keyPaths
     * */
    protected $keyPaths = [
        '_select' => 'get',
        '_insert' => 'add',
        '_update' => 'update',
    ];

    /**
     * @var string $currentModelAction
     * */
    protected $currentModelAction = 'lead';

    public function __construct(Client $client)
    {
        parent::__construct($client);
        $this->with(['contacts', 'catalog_elements']);
    }

    /**
     * @return string
     * */
    protected function entity()
    {
        return $this->currentModelAction;
    }

    /** get count
     * @return int
     * */
    public function count()
    {
        $result = $this->countData();
        return $result['response']['summary']['count'] ?? 0;
    }
}
