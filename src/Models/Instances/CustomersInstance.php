<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models\Instances;

use Anemone\Client;
use Anemone\Contracts\BeInstanceModel;
use Anemone\Core\Builder\Filter\FilterCF;
use Anemone\Core\Builder\Filter\FilterCreatedAt;
use Anemone\Core\Builder\Filter\FilterCreatedBy;
use Anemone\Core\Builder\Filter\FilterId;
use Anemone\Core\Builder\Filter\FilterName;
use Anemone\Core\Builder\Filter\FilterResponsible;
use Anemone\Core\Builder\Filter\FilterUpdatedBy;
use Anemone\Core\Builder\Helpers\HaveDataCreateModify;
use Anemone\Core\Builder\Helpers\HaveId;
use Anemone\Core\Builder\Helpers\HaveLimits;
use Anemone\Core\Builder\Helpers\HaveWith;
use Anemone\Core\Builder\Helpers\WithCount;
use Anemone\Core\Helpers\CFEntities;
use Anemone\Core\Helpers\CuStatuses;
use Anemone\Core\Helpers\LinkEntities;
use Anemone\Models\Helpers\HaveTags;
use Anemone\Models\Helpers\WithNotes;

class CustomersInstance extends ModelInstance implements BeInstanceModel
{
    use CFEntities;
    use CuStatuses;
    use HaveDataCreateModify;
    use HaveId;
    use HaveLimits;
    use HaveTags;
    use HaveWith;
    use LinkEntities;
    use WithCount;
    use WithNotes;
    use FilterId;
    use FilterName;
    use FilterCreatedBy;
    use FilterUpdatedBy;
    use FilterResponsible;
    use FilterCreatedAt;
    use FilterCF;

    /**
     * @var array $keyPaths
     * */
    protected $keyPaths = [
        '_select' => 'get',
        '_insert' => 'add',
        '_update' => 'update',
    ];
    /**
     * @var string $currentModelAction
     * */
    protected $currentModelAction = 'customer';

    public function __construct(Client $client)
    {
        parent::__construct($client);
        $this->with(['contacts', 'companies', 'catalog_elements']);
    }

    /**
     * @return string
     * */
    protected function entity()
    {
        return $this->currentModelAction;
    }

    /** filter for responsible users
     * @param array $id
     * @return $this
     * */
    public function responsibleUser($id)
    {
        $this->builder->where('filter', [
            'main_user' => $id,
        ]);
        return $this;
    }

    /** filter for date create
     * @param string $from
     * @param string $to
     * @return $this
     * */
    public function dateCreate($from, $to)
    {
        $this->builder->where('filter', [
            'date' => [
                'type' => 'create',
                'from' => $from,
                'to' => $to,
            ],
        ]);
        return $this;
    }

    /** filter for date modify
     * @param string $from
     * @param string $to
     * @return $this
     * */
    public function dateModify($from, $to)
    {
        $this->builder->where('filter', [
            'date' => [
                'type' => 'modify',
                'from' => $from,
                'to' => $to,
            ],
        ]);
        return $this;
    }

    /** filter for next date
     * @param string $from
     * @param string $to
     * @return $this
     * */
    public function nextDate($from, $to)
    {
        $this->builder->where('filter', [
            'next_date' => [
                'from' => $from,
                'to' => $to,
            ],
        ]);
        return $this;
    }

    /** get count
     * @return int
     * */
    public function count()
    {
        $result = $this->countData();
        return $result['summary']['total']['count'] ?? 0;
    }
}
