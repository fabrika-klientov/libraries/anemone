<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Core\Collection\Collection;
use Anemone\Exceptions\AnemoneException;
use Anemone\Models\Instances\ModelInstance;

abstract class Model extends Base
{
    /**
     * @var ModelInstance|null $instance
     * */
    protected $instance;

    /**
     * @param ModelInstance|null $instance
     * @param array $resource
     * @return void
     * */
    public function __construct(ModelInstance $instance = null, array $resource = [])
    {
        parent::__construct($resource);
        $this->instance = $instance;

        if (!isset($this->data['custom_fields_values']) && method_exists($this, 'initCF')) {
            $this->initCF();
        }

        if (!isset($this->data['_embedded'])) {
            $this->data['_embedded'] = [];
        }
    }

    /** set instance
     * @param ModelInstance $instance
     * @return void
     * */
    public function setInstance(ModelInstance $instance)
    {
        $this->instance = $instance;
    }

    /**
     * @return \Anemone\Client|null
     * */
    public function getClient()
    {
        if ($this->instance) {
            return $this->instance->getClient();
        }

        return null;
    }

    /**
     * save or update this Model (should have init $this->instance)
     * exceptions not light
     * @return bool
     * */
    public function save()
    {
        if (is_null($this->instance)) {
            throw new AnemoneException('For use [save] this model should initializing [ModelInstance] (In [construct] or [setInstance] method)');
        }

        return $this->instance->save(new Collection([$this]));
    }

    /** delete this Model (should have init $this->instance)
     * exceptions not light
     * @return bool
     * */
    public function delete()
    {
        if (is_null($this->instance)) {
            throw new AnemoneException('For use [delete] this model should initializing [ModelInstance] (In [construct] or [setInstance] method)');
        }

        return $this->instance->delete(new Collection([$this]));
    }

    /** cloning current model (alpha)
     * */
    public function __clone()
    {
        unset($this->data['id']);
    }
}
