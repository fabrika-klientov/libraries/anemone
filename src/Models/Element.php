<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.16
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeModel;
use Anemone\Models\Helpers\WithCF;

/**
 * @property-read int $id
 * @property string $name
 * @property-read int $created_by
 * @property int $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property bool $is_deleted
 * @property int $catalog_id
 * @property array $custom_fields_values
 * @property int $account_id
 *
 * @method __construct(\Anemone\Models\Instances\ElementsInstance $instance = null, array $resource = [])
 * @method $this setInstance(\Anemone\Models\Instances\ElementsInstance $instance)
 * */
class Element extends Model implements BeModel
{
    use WithCF;
}
