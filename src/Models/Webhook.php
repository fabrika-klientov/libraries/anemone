<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeModel;
use Illuminate\Support\Str;

/**
 * @property int $id
 * @property string $destination
 * @property string $created_at
 * @property string $updated_at
 * @property string $account_id
 * @property string $created_by
 * @property string $sort
 * @property bool $disabled
 * @property array $settings
 *
 * @property-read bool $result
 *
 * @method $this responsibleLead(bool $status = true);
 * @method $this responsibleContact(bool $status = true);
 * @method $this responsibleCompany(bool $status = true);
 * @method $this responsibleCustomer(bool $status = true);
 * @method $this responsibleTask(bool $status = true);
 * @method $this restoreLead(bool $status = true);
 * @method $this restoreContact(bool $status = true);
 * @method $this restoreCompany(bool $status = true);
 * @method $this addLead(bool $status = true);
 * @method $this addContact(bool $status = true);
 * @method $this addCompany(bool $status = true);
 * @method $this addCustomer(bool $status = true);
 * @method $this addTask(bool $status = true);
 * @method $this updateLead(bool $status = true);
 * @method $this updateContact(bool $status = true);
 * @method $this updateCompany(bool $status = true);
 * @method $this updateCustomer(bool $status = true);
 * @method $this updateTask(bool $status = true);
 * @method $this deleteLead(bool $status = true);
 * @method $this deleteContact(bool $status = true);
 * @method $this deleteCompany(bool $status = true);
 * @method $this deleteCustomer(bool $status = true);
 * @method $this deleteTask(bool $status = true);
 * @method $this statusLead(bool $status = true);
 * @method $this noteLead(bool $status = true);
 * @method $this noteContact(bool $status = true);
 * @method $this noteCompany(bool $status = true);
 * @method $this noteCustomer(bool $status = true);
 *
 * @method __construct(\Anemone\Models\Instances\WebhooksInstance $instance = null, array $resource = [])
 * @method $this setInstance(\Anemone\Models\Instances\WebhooksInstance $instance)
 * */
class Webhook extends Model implements BeModel
{

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     * */
    public function __call($name, $arguments)
    {
        $status = empty($arguments) ? true : head($arguments);
        $type = Str::snake($name);
        $events = $this->settings ?? [];

        /** @var int $three
         * */
        $three = $status ? null : 1;
        /** @var string $four
         * */
        $four = $status && !in_array($type, $events) ? $type : null;
        array_splice($events, (int)array_search($type, $events), $three, $four);
        $this->settings = $events;

        return $this;
    }

}
