<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.05
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeModel;

/**
 * @property-read int $id
 * @property-read int $entity_id
 * @property-read int $entity_type
 * @property string $phone
 * @property string $direction (inbound|outbound)
 * @property string $uniq
 * @property string $duration
 * @property string $source
 * @property int $call_status
 * @property string $call_result
 * @property string $link
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 * @property string $responsible_user_id
 *
 * @method __construct(\Anemone\Models\Instances\CallsInstance $instance = null, array $resource = [])
 * @method $this setInstance(\Anemone\Models\Instances\CallsInstance $instance)
 * */
class Call extends Model implements BeModel
{

}
