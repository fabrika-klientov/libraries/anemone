<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeBaseModel;

/**
 * @property-read int $id
 * @property int $entity_id
 * @property int $responsible_user_id
 * @property int $created_by
 * @property bool $created_at
 * @property int $updated_at
 * @property int $updated_by
 * @property array $params // text and other
 * @property string $note_type
 * @property int $account_id
 * @property string $group_id
 * */
class Note extends Base implements BeBaseModel
{

    /**
     * @return string|null
     * */
    public function getText(): ?string
    {
        return $this->data['params']['text'] ?? null;
    }

    /**
     * @param string $text
     * @return void
     */
    public function setText(string $text)
    {
        $this->data['params']['text'] = $text;
    }
}
