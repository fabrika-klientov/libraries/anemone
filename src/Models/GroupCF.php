<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeBaseModel;

/**
 * @property-read int $id
 * @property string $name
 * @property bool $is_predefined
 * @property-read string $type
 * @property string $entity_type
 * @property int $sort
 * */
class GroupCF extends Base implements BeBaseModel
{
}
