<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeModel;
use Anemone\Contracts\BePersist;
use Anemone\Models\Helpers;

/**
 * @property-read int $id
 * @property string $name
 * @property int $next_price
 * @property int $next_date
 * @property int $responsible_user_id
 * @property int $status_id
 * @property string $periodicity
 * @property-read int $created_by
 * @property string $created_at
 * @property string $updated_at
 * @property-read bool $is_deleted
 * @property-read int $updated_by
 * @property-read  string $closest_task_at
 * @property int $ltv
 * @property string $purchases_count
 * @property string $average_check
 * @property string $account_id
 * @property \Anemone\Core\Collection\Collection $custom_fields_values
 * @property array $_embedded
 *
 * @method __construct(\Anemone\Models\Instances\CustomersInstance $instance = null, array $resource = [])
 * @method $this setInstance(\Anemone\Models\Instances\CustomersInstance $instance)
 * */
class Customer extends Model implements BeModel, BePersist
{
    use Helpers\Link;
    use Helpers\WithCF;
    use Helpers\WithCompany;
    use Helpers\WithContacts;
    use Helpers\WithCatalogElements;
    use Helpers\WithNotes;
    use Helpers\WithTags;
    use Helpers\WithTasks;
    use Helpers\Persisted;
}
