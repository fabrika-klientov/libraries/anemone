<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeBaseModel;

/**
 * @property-read int $id
 * @property string $name
 * @property int $sort
 * @property bool $is_editable
 * @property int $pipeline_id
 * @property string $color
 * @property string $type
 * @property-read int $account_id
 * */
class StatusPL extends Base implements BeBaseModel
{
}
