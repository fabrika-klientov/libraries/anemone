<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeStaticallyModel;
use Anemone\Models\Instances\AccountInstance;
use Anemone\Models\Instances\ModelInstance;

/**
 * @property-read int $id
 * @property-read string $name
 * @property-read string $subdomain
 * @property-read string $created_at
 * @property-read string $created_by
 * @property-read string $updated_at
 * @property-read string $updated_by
 * @property-read string $current_user_id
 * @property-read string $country
 * @property-read string $customers_mode
 * @property-read bool $is_unsorted_on
 * @property-read int $mobile_feature_version
 * @property-read bool $is_loss_reason_enabled
 * @property-read bool $is_helpbot_enabled
 * @property-read bool $is_technical_account
 * @property-read int $contact_name_display_order
 * @property-read string $amojo_id
 * @property-read int $version
 * @property-read array $entity_names
 * @property-read array $amojo_rights
 * @property-read array $users_groups
 * @property-read array $task_types
 * @property-read array $datetime_settings
 *
 * */
class Account extends Base implements BeStaticallyModel
{
    /**
     * @var \Anemone\Models\Instances\ModelInstance|null $instance
     * */
    protected $instance;

    /**
     * @param AccountInstance $instance
     * @param array $resource
     * @return void
     * */
    public function __construct(AccountInstance $instance = null, array $resource = [])
    {
        parent::__construct($resource);
        $this->instance = $instance;
    }

    /** set instance
     * @param ModelInstance $instance
     * @return $this
     * */
    public function setInstance(ModelInstance $instance)
    {
        $this->instance = $instance;
        return $this;
    }
}
