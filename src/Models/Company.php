<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.29
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Models;

use Anemone\Contracts\BeModel;
use Anemone\Contracts\BePersist;
use Anemone\Models\Helpers;

/**
 * @property-read int $id
 * @property string $name
 * @property int $responsible_user_id
 * @property-read int $created_by
 * @property string $created_at
 * @property string $updated_at
 * @property int $updated_by
 * @property string $group_id
 * @property int $account_id
 * @property-read string $closest_task_at
 * @property \Anemone\Core\Collection\Collection $custom_fields_values
 * @property array $_embedded
 *
 * @method __construct(\Anemone\Models\Instances\CompaniesInstance $instance = null, array $resource = [])
 * @method $this setInstance(\Anemone\Models\Instances\CompaniesInstance $instance)
 * */
class Company extends Model implements BeModel, BePersist
{
    use Helpers\Link;
    use Helpers\WithCF;
    use Helpers\WithContacts;
    use Helpers\WithCustomers;
    use Helpers\WithLeads;
    use Helpers\WithCatalogElements;
    use Helpers\WithNotes;
    use Helpers\WithTags;
    use Helpers\WithTasks;
    use Helpers\Persisted;
    use Helpers\Emailed;
    use Helpers\Phoned;
}
