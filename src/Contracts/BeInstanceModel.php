<?php
/**
 *
 * @package   Anemone
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Contracts;

use Anemone\Core\Collection\Collection;

interface BeInstanceModel
{

    /** get Collect of instance Models
     * @return Collection
     * */
    public function get();

    /** store or update Collect of instance Models
     * @param Collection $collect
     * @return bool
     * */
    public function save(Collection $collect = null);

    /** destroy Collect of instance Models
     * @param Collection $collect
     * @return bool
     * */
    public function delete(Collection $collect = null);
}
