<?php
/**
 *
 * @package   Anemone
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Contracts;

/**
 * @property int $id
 * */
interface BeModel extends BeStaticallyModel
{

    /** store or update model
     * @return bool
     * */
    public function save();

    /** destroy model
     * @return bool
     * */
    public function delete();
}
