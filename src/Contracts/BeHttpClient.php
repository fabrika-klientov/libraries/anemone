<?php
/**
 *
 * @package   Anemone
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.28
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Contracts;

interface BeHttpClient
{
    const _protocol = 'https://';
    const _ping = '/private/ping.php';

    /** get api
     * @param string $link
     * @param array $query
     * @param array $guzzleOptions
     * @param bool $withSessionData
     * @param bool $isResponseInterface
     * @param bool $async
     * @return \Psr\Http\Message\ResponseInterface|mixed
     */
    public function get(
        string $link,
        array $query = [],
        array $guzzleOptions = [],
        bool $withSessionData = true,
        bool $isResponseInterface = false,
        bool $async = false
    );

    /** get api
     * @param string $link
     * @param array $data
     * @param array $guzzleOptions
     * @param bool $withSessionData
     * @param bool $isResponseInterface
     * @param bool $async
     * @return \Psr\Http\Message\ResponseInterface|mixed
     */
    public function post(
        string $link,
        array $data,
        array $guzzleOptions = [],
        bool $withSessionData = true,
        bool $isResponseInterface = false,
        bool $async = false
    );

    /** get api
     * @param string $link
     * @param array $data
     * @param array $guzzleOptions
     * @param bool $withSessionData
     * @param bool $isResponseInterface
     * @param bool $async
     * @return \Psr\Http\Message\ResponseInterface|mixed
     */
    public function patch(
        string $link,
        array $data,
        array $guzzleOptions = [],
        bool $withSessionData = true,
        bool $isResponseInterface = false,
        bool $async = false
    );

    /** get api
     * @param string $link
     * @param array $query
     * @param array $guzzleOptions
     * @param bool $withSessionData
     * @param bool $isResponseInterface
     * @param bool $async
     * @return \Psr\Http\Message\ResponseInterface|mixed
     */
    public function delete(
        string $link,
        array $query = [],
        array $guzzleOptions = [],
        bool $withSessionData = true,
        bool $isResponseInterface = false,
        bool $async = false
    );

    /** get full url
     * @param string $link
     * @param string $domainWithoutHttp
     * @param bool $isAccountDomain
     * @return string
     * */
    public function getUrl(string $link, $domainWithoutHttp = null, $isAccountDomain = false);

    /** merge of async requesting
     * @param \Closure $closure
     * @return array
     * */
    public function merge($closure);

    /**
     * @return \GuzzleHttp\Client
     * */
    public function getGuzzle();
}
