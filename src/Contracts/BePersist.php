<?php
/**
 *
 * @package   Anemone
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.11.20
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Contracts;

interface BePersist
{
    /**
     * @return array
     * */
    public function getFixed(): array;

    /**
     * @param array $data
     * @return mixed
     */
    public function factory(array $data);
}
