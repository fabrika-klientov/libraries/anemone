<?php
/**
 * */

namespace Anemone\Contracts;


interface BeAuth
{
    /**
     * @return array
     * */
    public function auth(): array;

    /**
     * @return array
     * */
    public function getSessionData(): array;
}