<?php
/**
 *
 * @package   Anemone
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Contracts;

/**
 * @method $this filter(callable|null $callback = null)
 * @method mixed first(callable $callback = null, $default = null)
 * @method $this each(callable $callback)
 * @method $this map(callable $callback)
 * @method bool isNotEmpty()
 * @method bool some($key, $operator = null, $value = null)
 * @method $this push($value)
 * @method int count()
 *
 * // ....
 * */
interface BeCollection
{
}
