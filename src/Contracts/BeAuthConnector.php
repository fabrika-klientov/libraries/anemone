<?php


namespace Anemone\Contracts;


interface BeAuthConnector
{
    /**
     * @return void
     * */
    public function __construct();

    /**
     * @param array $authData
     * @return array
     * */
    public function read(array $authData): array;

    /**
     * @param array $authData
     * @param array $refreshAuthData
     * @return bool
     * */
    public function write(array $authData, array $refreshAuthData): bool;
}