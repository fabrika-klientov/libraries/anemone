<?php
/**
 *
 * @package   Anemone
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Contracts;

interface BeServiceAdapter extends BeAdapter
{
    /** collection of models
     * @param mixed $data
     * @return \Anemone\Core\Collection\Collection
     * */
    public function getData($data = null);

    /** params request for select
     * @param \Anemone\Core\Builder\Builder $builder
     * @return array
     * */
    public function getSelectRequest($builder = null);

    /** params request for insert
     * @param \Anemone\Core\Collection\Collection $data
     * @return \Anemone\Core\Collection\Collection|array|null
     * */
    public function getInsertRequest($data = null);

    /** merging result for insert
     * @param \Anemone\Core\Collection\Collection $collect
     * @param array $data
     * @return \Anemone\Core\Collection\Collection
     * */
    public function mergeInsertResponse($collect, $data = null);

    /** params request for update
     * @param \Anemone\Core\Collection\Collection $data
     * @return \Anemone\Core\Collection\Collection
     * */
    public function getUpdateRequest($data = null);

    /** merging result for update
     * @param \Anemone\Core\Collection\Collection $collect
     * @param array $data
     * @return \Anemone\Core\Collection\Collection
     * */
    public function mergeUpdateResponse($collect, $data = null);

    /** params request for delete
     * @param \Anemone\Core\Collection\Collection $data
     * @return \Anemone\Core\Collection\Collection
     * */
    public function getDeleteRequest($data = null);

    /** merging result for delete
     * @param \Anemone\Core\Collection\Collection $collect
     * @param array $data
     * @return \Anemone\Core\Collection\Collection
     * */
    public function mergeDeleteResponse($collect, $data = null);
}
