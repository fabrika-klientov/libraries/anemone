<?php
/**
 *
 * @package   Anemone
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Contracts;

interface BeAdapter
{
    /** collection of models
     * @return \Anemone\Core\Collection\Collection
     * */
    public function getData();

    /** params request for select
     * @return array
     * */
    public function getSelectRequest();

    /** params request for insert
     * @return \Anemone\Core\Collection\Collection|array|null
     * */
    public function getInsertRequest();

    /** merging result for insert
     * @param \Anemone\Core\Collection\Collection $collect
     * @return \Anemone\Core\Collection\Collection
     * */
    public function mergeInsertResponse($collect);

    /** params request for update
     * @return \Anemone\Core\Collection\Collection|array|null
     * */
    public function getUpdateRequest();

    /** merging result for update
     * @param \Anemone\Core\Collection\Collection $collect
     * @return \Anemone\Core\Collection\Collection
     * */
    public function mergeUpdateResponse($collect);

    /** params request for delete
     * @return \Anemone\Core\Collection\Collection|array|null
     * */
    public function getDeleteRequest();

    /** merging result for delete
     * @param \Anemone\Core\Collection\Collection $collect
     * @return \Anemone\Core\Collection\Collection
     * */
    public function mergeDeleteResponse($collect);
}
