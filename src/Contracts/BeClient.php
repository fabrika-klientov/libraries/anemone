<?php
/**
 *
 * @package   Anemone
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.01
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Contracts;

interface BeClient
{

    /** context for BeHttpClient Service
     * @return BeHttpClient
     * */
    public function getQueryService();

    /** context for BeAuthService Service
     * @return BeAuthService
     * */
    public function getAuthService();

    /** account domain
     * @return string
     * */
    public function getDomain();
}
