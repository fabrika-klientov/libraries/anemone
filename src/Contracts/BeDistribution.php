<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 20.01.21
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Contracts;

interface BeDistribution
{
    /** type of distribution [linear, random, ...]
     * @return string
     * */
    public function getType(): ?string;

    /** type of source [group, any, ...]
     * @return string
     * */
    public function getSource(): ?string;

    /** array of ids user or array of id group
     * @param int $id (id group)
     * @return array
     * */
    public function getIds($id = null): array;

    /** array of ids user is excluded [12345, 54321, ...]
     * @return array
     * */
    public function getExcludes(): array;

    /** id user is default
     * @return int
     * */
    public function getDefault(): ?int;

    /** set data (edited after next method service)
     * @param array $data
     * @param int $id (id group)
     * @return void
     * */
    public function setIds(array $data, $id = null);

    /** result
     * @return array
     * */
    public function result(): array;

}