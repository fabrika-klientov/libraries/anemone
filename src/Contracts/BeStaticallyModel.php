<?php
/**
 *
 * @package   Anemone
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Contracts;

use Anemone\Models\Instances\ModelInstance;

interface BeStaticallyModel extends BeBaseModel
{
    /**
     * @param ModelInstance $instance
     * @return static
     * */
    public function setInstance(ModelInstance $instance);
}
