<?php
/**
 *
 * @package   Anemone
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Contracts;

/**
 * @property-read mixed $id
 * @property string $name
 * @property string $field_type
 * @property string|null $field_code
 * @property string $entity_type
 * @property bool $is_editable
 * @property bool $is_required
 * @property bool $is_deletable
 * @property bool $is_visible
 * @property array $enums
 * */
interface BeCustomField
{
    /**
     * @return mixed
     * */
    public function getValue();

    /**
     * @param mixed $data
     * @return void
     * */
    public function setValue($data);
}
