<?php
/**
 *
 * @package   Anemone
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.09.01
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Contracts;

interface BeAuthService
{
    /**
     * @return string
     * */
    public function getDomain();

    /**
     * @return array
     * */
    public function getAuthParams();
}
