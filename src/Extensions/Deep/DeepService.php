<?php
/**
 *
 * @package   Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.08.13
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Deep;

use Anemone\Contracts\BeClient;
use Anemone\Core\Collection\Collection;
use Anemone\Exceptions\InvalidDataException;
use Anemone\Extensions\Core\BaseModel;
use Anemone\Extensions\Deep\Entities\DeepAccount;

class DeepService
{
    /**
     * @var BeClient $client
     * */
    private $client;

    public function __construct(BeClient $client)
    {
        $this->client = $client;
    }

    /**
     * @return Collection
     * */
    public function getAccounts()
    {
        $qs = $this->client->getQueryService();
        $result = $qs->get($qs->getUrl(aExtEntryData('link_profile_account'), $this->client->getDomain()));

        return self::factoryEntities($result, DeepAccount::class, '_embedded.items', true);
    }

    /**
     * @param int $id
     * @return DeepAccount
     * */
    public function getAccount($id)
    {
        $qs = $this->client->getQueryService();
        $result = $qs->get($qs->getUrl(aExtEntryData('link_profile_account') . "/$id", $this->client->getDomain()));

        return self::factoryEntities($result, DeepAccount::class);
    }


    /**
     * @param mixed $data
     * @param string $class
     * @param string|null $keys
     * @param bool $isCollect
     * @return Collection|mixed|null
     * @throws InvalidDataException
     */
    protected static function factoryEntities($data, string $class, string $keys = null, bool $isCollect = false)
    {
        if (!is_subclass_of($class, BaseModel::class)) {
            throw new InvalidDataException('Class is not subclass of BaseModel');
        }

        if (empty($data)) {
            return $isCollect ? new Collection() : null;
        }

        if (is_string($data)) {
            return self::factoryEntities(json_decode($data, true), $class, $keys, $isCollect);
        }

        if (!empty($keys)) {
            $keys = explode('.', $keys);
            while (!empty($keys)) {
                $key = array_shift($keys);
                $data = $data[$key] ?? null;
            }

            return self::factoryEntities($data, $class, null, $isCollect);
        }

        if ($isCollect) {
            return new Collection(
                array_map(function ($item) use($class) {
                    return self::factoryEntities($item, $class);
                }, $data)
            );
        }

        return new $class($data);
    }
}
