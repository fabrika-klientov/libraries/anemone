<?php
/**
 *
 * @package   Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.08.13
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Deep\Entities;

use Anemone\Extensions\Core\BaseModel;

/**
 * @property-read int $id
 * @property-read string $uuid
 * @property-read string $name
 * @property-read string $subdomain
 * @property-read bool $is_admin
 * @property-read int $account_version
 *
 * // only if detail
 * @property-read string $currency
 * @property-read string $lang
 * @property-read string $timezone
 * @property-read array $ip_ranges
 * @property-read string $date_format
 * @property-read string $time_format
 * @property-read string $tags_editable
 * @property-read array $settings
 * */
class DeepAccount extends BaseModel
{

}
