<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Partners\Models;

use Anemone\Exceptions\BadResponseException;
use Anemone\Exceptions\InvalidDataException;
use Anemone\Extensions\Core\Services\HttpClientService;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property array $created_user ['id' => int, 'name' => string, 'last_name' => string, 'email' => string]
 * @property string $name               (optional only after detail or/and find methods)
 * @property string $timezone           (optional only after detail or/and find methods)
 * @property bool $active_paid_access   (optional only after detail or/and find methods)
 * @property bool $rec_pay_usa          (optional only after detail or/and find methods)
 * @property array $tariff              (optional only after detail or/and find methods)
 * @property array $users               (optional only after detail or/and find methods)
 * @property int $trial_end             (optional only after detail or/and find methods)
 * @property int $trial_start           (optional only after detail or/and find methods)
 * @property int $new_trial_end         (optional only after prolong method)
 * @property int $pay_start             (optional only after detail or/and find methods)
 * @property int $pay_end               (optional only after detail or/and find methods)
 * @property int $leads_all             (optional only after detail or/and find methods)
 * @property int $leads_active          (optional only after detail or/and find methods)
 * @property int $leads_inactive        (optional only after detail or/and find methods)
 * @property int $contacts_all          (optional only after detail or/and find methods)
 * @property array $trial_prolonged     (optional only after detail or/and find methods)
 * */
final class Partner implements \JsonSerializable
{
    private $httpClientService;
    private $data;

    /**
     * @param array $data
     * @param HttpClientService $httpClientService
     * @return void
     * @throws InvalidDataException
     * */
    public function __construct(array $data, HttpClientService $httpClientService)
    {
        $this->httpClientService = $httpClientService;
        $this->data = isset($data['id']) ? $data : $this->validate($data);
    }

    /** store account in partners return new Partner with new data
     * @param int $partnerID
     * @return static
     * @throws \Exception
     * */
    public function store($partnerID)
    {
        if (isset($this->data['id'])) {
            throw new InvalidDataException('Account is exist for ID [' . $this->data['id'] . ']');
        }

        // store
        $result = $this->httpClientService->post(aExtEntryData('link_store_account'), [
            'ACTION' => 'REGISTER_NEW_ACCOUNT',
            'account' => [
                'email' => $this->data['created_user']['email'],
                'first_name' => $this->data['created_user']['name'],
                'phone_num' => $this->data['created_user']['phone'] ?? '',
                'link_to_partner' => [
                    'partner_id' => $partnerID,
                ],
            ],
            'AJAX' => 'Y',
        ]);

        if (is_null($result)) {
            throw new BadResponseException('Response for link [' . aExtEntryData('link_store_account') . '] resulted NULL');
        }

        $result = json_decode($result, true);
        if (isset($result['success']) && $result['success']) {
            $email = $this->data['created_user']['email'];
            sleep(1);
            return self::all($this->httpClientService, 'trial')->first(function ($item) use ($email) {
                return $item->created_user['email'] == $email;
            });
        }

        throw new BadResponseException('Account not created. Error:: ' . json_encode($result['error'] ?? []));
    }

    /** get detail for partner !! is Request
     * @return static
     * @throws \Exception
     * */
    public function detail()
    {
        if (empty($this->data['id'])) {
            throw new InvalidDataException('Account is not exist. Required [id]');
        }

        return self::find($this->httpClientService, $this->data['id']);
    }

    /** prolong trial periods (only 7 or/and 14 days)
     * @param int $days (only 7 or/and 14) this operation available: one attempt for 7 days and one attempt for 14 days
     * @return static
     * @throws \Exception
     * */
    public function prolong(int $days)
    {
        if ($days != 7 && $days != 14) {
            throw new InvalidDataException('Method prolong support only param for equals [7] or [14]');
        }

        $detail = $this;
        if (!isset($this->data['trial_prolonged'])) {
            $detail = $this->detail();
        }

        if (isset($detail->trial_prolonged[$days]) && $detail->trial_prolonged[$days]) {
            throw new InvalidDataException('This partner account [' . $detail->id . '] is prolonged for period [' . $days . '] days');
        }

        $result = $this->httpClientService->post(str_replace('{id}', (string)$detail->id, aExtEntryData('link_prolong_trial')), [
            'days' => $days,
        ], [
            'headers' => [
                'Accept' => 'application/json',
                'X-Requested-With' => 'XMLHttpRequest',
            ],
        ]);

        if (is_null($result)) {
            throw new BadResponseException('Response for link [' . str_replace('{id}', (string)$detail->id, aExtEntryData('link_prolong_trial')) . '] returned NULL');
        }

        $result = json_decode($result, true);

        $trialProlonged = $detail->trial_prolonged;
        $trialProlonged[$days] = true;
        $detail->trial_prolonged = $trialProlonged;
        $detail->trial_end = strtotime($result['new_trial_end']);
        $detail->new_trial_end = $result['new_trial_end'];

        return $detail;
    }

    /** get detail for partner !! is Request
     * @param HttpClientService $httpClientService
     * @param int $id
     * @return static
     * @throws \Exception
     * */
    public static function find(HttpClientService $httpClientService, $id)
    {
        $result = $httpClientService->get(aExtEntryData('link_accounts') . '/' . $id, [], [
            'headers' => [
                'Accept' => 'application/json',
                'X-Requested-With' => 'XMLHttpRequest',
            ],
        ]);

        if (is_null($result)) {
            throw new BadResponseException('Response for link [' . aExtEntryData('link_accounts') . '/' . $id . '] returned NULL');
        }

        $result = json_decode($result, true);

        if (isset($result['error'])) {
            throw new BadResponseException('Result has error:: >> ' . json_encode($result));
        }

        $result['id'] = $id;
        $partner = new static($result, $httpClientService);
        $partner->created_user = $result['users'][0] ?? [];

        return $partner;
    }

    /** get all partners in collection
     * @param HttpClientService $httpClientService
     * @param string $filter  trial | expired | paid
     * @return \Illuminate\Support\Collection
     * */
    public static function all(HttpClientService $httpClientService, string $filter = null)
    {
        $link = aExtEntryData('link_accounts');

        return array_reduce(['trial', 'expired', 'paid'], function (Collection $result, $item) use ($httpClientService, $link, $filter) {
            if (is_null($filter) || $item == $filter) {
                $data = $httpClientService->get($link, ['status' => $item], ['headers' => [
                    'X-Requested-With' => 'XMLHttpRequest',
                    'Accept' => 'application/json',
                ]]);
                if (isset($data)) {
                    $data = json_decode($data, true);
                    if (isset($data) && !isset($data['error'])) {
                        return $result->merge(array_map(function ($item) use ($httpClientService) {
                            return new static($item, $httpClientService);
                        }, $data));
                    }
                }
            }
            return $result;
        }, new Collection());
    }

    /** getter
     * @param string $name
     * @return mixed
     * */
    public function __get($name)
    {
        return $this->data[$name] ?? null;
    }

    /** setter
     * @param string $name
     * @param mixed $value
     * @return void
     * */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /** for isset magic
     * @param string $name
     * @return bool
     * */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    /** serialization
     * @return string
     * */
    public function __toString()
    {
        return (string)json_encode($this->data);
    }

    /** serialization \JsonSerializable
     * @return array
     * */
    public function jsonSerialize()
    {
        return $this->data;
    }

    /**
     * @param array $params ['name' => string, 'email' => string, 'phone' => string(optional)]
     * @return array
     * @throws InvalidDataException
     * */
    protected function validate(array $params)
    {
        if (
            !empty($params['name']) && is_string($params['name']) &&
            !empty($params['email']) && is_string($params['email'])
        ) {
            return [
                'created_user' => [
                    'name' => $params['name'],
                    'email' => $params['email'],
                    'phone' => $params['phone'] ?? null,
                ],
            ];
        }

        throw new InvalidDataException('Params should be array of [\'name\' => string, \'email\' => string, \'phone\' => string(optional)]');
    }
}
