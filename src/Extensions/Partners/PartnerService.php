<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Partners;

use Anemone\Exceptions\BadResponseException;
use Anemone\Extensions\Core\Services\HttpClientService;
use Anemone\Extensions\Partners\Models\Partner;

class PartnerService
{
    /**
     * @var HttpClientService $httpClientService
     * */
    private $httpClientService;

    /**
     * @param HttpClientService $httpClientService
     * @return void
     * */
    public function __construct(HttpClientService $httpClientService)
    {
        $this->httpClientService = $httpClientService;
    }

    /** get all partners in collection
     * @param string $filter  trial | expired | paid
     * @return \Illuminate\Support\Collection
     * */
    public function partnersList(string $filter = null)
    {
        return Partner::all($this->httpClientService, $filter);
    }

    /** get all partners in collection
     * @param int $id
     * @return Partner
     * @throws \Exception
     * */
    public function partnerById($id)
    {
        return Partner::find($this->httpClientService, $id);
    }

    /** get all partners in collection
     * @param int $id
     * @return array
     * @throws \Exception
     * */
    public function tariffById($id)
    {
        $result = $this->httpClientService->post(aExtEntryData('link_request_validate'), [
            'id_acc' => $id,
        ]);

        if (is_null($result)) {
            throw new BadResponseException('Response for link [' . aExtEntryData('link_request_validate') . '] returned NULL');
        }

        return json_decode($result, true);
    }
}
