<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Core\Helpers;

use Anemone\Exceptions\InvalidDataException;

trait Validators
{
    /** validator auth params
     * [
     *  user_login => string    (required)
     *  user_password => string (required)
     * ]
     * @param array|null $params
     * @return array
     * @throws InvalidDataException
     * */
    protected function validate($params)
    {
        if (is_null($params)) {
            throw new InvalidDataException('Store new NotApiClient instance require params. NULL');
        }

        if (!is_array($params)) {
            throw new InvalidDataException('Store new NotApiClient instance require params should array');
        }

        if (
            isset($params['user_login']) && is_string($params['user_login']) &&
            isset($params['user_password']) && is_string($params['user_password'])
        ) {
            return $params;
        }

        throw new InvalidDataException('Store new NotApiClient instance require params should have: [user_login => string], [user_password => string]');
    }
}
