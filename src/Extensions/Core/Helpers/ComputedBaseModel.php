<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.23
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Core\Helpers;

abstract class ComputedBaseModel implements \JsonSerializable
{

    /** computed data
     * @var array $data
     * */
    protected $data = [];

    /**
     * @param array $data
     * @return void
     * */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /** isset magic computed
     * @param string $name
     * @return bool
     * */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    /** getter
     * @param string $name
     * @return mixed
     * */
    public function __get($name)
    {
        return $this->data[$name] ?? null;
    }

    /** setter
     * @param string $name
     * @param mixed $value
     * @return void
     * */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /** serializable
     * @return string
     * */
    public function __toString()
    {
        return (string)json_encode($this->data);
    }

    /** serializable \JsonSerializable
     * @return array
     * */
    public function jsonSerialize()
    {
        return $this->data;
    }
}
