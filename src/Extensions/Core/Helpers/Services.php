<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.03.13
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Core\Helpers;

use Anemone\Extensions\Partners\PartnerService;
use Anemone\Extensions\Widgets\IntegrationsService;
use Anemone\Extensions\Widgets\WidgetsService;

/**
 * @property PartnerService $partnerService
 * @property WidgetsService $widgetsService
 * @property IntegrationsService $integrationsService
 * */
trait Services
{
    /**
     * @param string $name
     * @return mixed
     * */
    public function __get($name)
    {
        switch ($name) {
            case 'partnerService':
                return new PartnerService($this->httpClientService);
            case 'widgetsService':
                return new WidgetsService($this);
            case 'integrationsService':
                return new IntegrationsService($this);
        }

        return null;
    }
}
