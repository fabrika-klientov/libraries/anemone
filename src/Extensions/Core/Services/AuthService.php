<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Core\Services;

use Anemone\Contracts\BeAuthService;
use Anemone\Exceptions\BadResponseException;
use GuzzleHttp\Cookie\FileCookieJar;
use GuzzleHttp\Cookie\SetCookie;

class AuthService implements BeAuthService
{
    /**
     * @var HttpClientService $httpClientService
     * */
    private $httpClientService;
    /**
     * @var array $authData
     * */
    private $authData;
    /**
     * @var string $domain
     * */
    private $domain;
    /**
     * @var string $cookiesPath
     * */
    protected $cookiesPath;
    /**
     * @var array $sessionData
     * */
    protected $sessionData = [];

    /**
     * @param array $authData
     * @param string $domain
     * @return void
     * */
    public function __construct(array $authData, $domain)
    {
        $this->authData = $authData;
        $this->domain = $domain;
        $this->cookiesPath = aConfig('cookies.path');
        $this->storeCookiesDir();
        $this->sessionData['cookies'] = new FileCookieJar($this->cookieName(), true);
    }

    /** init service
     * @param HttpClientService $httpClientService
     * @return $this
     * */
    public function initHttpClientService(HttpClientService $httpClientService)
    {
        $this->httpClientService = $httpClientService;
        return $this;
    }

    /** default session in cookies (if other, for example [token], you should override this method)
     * @return array
     * @throws BadResponseException
     * */
    public function getSessionData()
    {
        if ($this->sessionData['cookies']->count() > 0) {
            return $this->sessionData;
        }

        return $this->auth();
    }

    /**
     * @return array
     * @throws BadResponseException
     * */
    public function auth()
    {
        try {
            $this->sessionData['cookies']->clear();

            if (!$this->visitForCookies()) {
                throw new \Exception('Not loaded cookies data');
            }

            $result = json_decode(
                $this->httpClientService->post(
                    $this->httpClientService->getUrl(aExtEntryData('link_auth'), $this->domain),
                    [
                        'csrf_token' => $this->getCookieValue('csrf_token'),
                        'username' => $this->authData['user_login'],
                        'password' => $this->authData['user_password'],
                    ],
                    [
                        'headers' => [
                            'Accept' => 'application/json',
                        ],
                        'cookies' => $this->sessionData['cookies'],
                    ],
                    false
                ),
                true
            );

            if (isset($result)) {
                return $this->sessionData;
            }

            return [];
        } catch (\Exception $exception) {
            throw new BadResponseException('Invalid Auth result. ' . $exception->getMessage(), $exception->getCode());
        }
    }

    /** get domain
     * @inheritDoc
     * */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @inheritDoc
     * */
    public function getAuthParams()
    {
        return array_merge($this->authData, ['domain' => $this->domain]);
    }

    /** cookie value
     * @param string $key
     * @return mixed
     * */
    protected function getCookieValue($key)
    {
        if (isset($this->sessionData['cookies'])) {
            /**
             * @var SetCookie|null $setCookie
             * */
            $setCookie = $this->sessionData['cookies']->getCookieByName($key);
            if ($setCookie) {
                return $setCookie->getValue();
            }
        }

        return null;
    }

    /** set cookie data in CookieJar
     * @return bool
     * @throws \Exception
     * */
    protected function visitForCookies()
    {
        if (is_null($this->httpClientService->get(aExtEntryData('link_page_account'), [], [
            'cookies' => $this->sessionData['cookies'],
        ], false))) {
            return false;
        }

        return true;
    }

    /**
     * @return void
     * @throws \RuntimeException
     * */
    protected function storeCookiesDir()
    {
        if (!file_exists($this->cookiesPath)) {
            if (!mkdir($this->cookiesPath, 0775, true)) {
                throw new \RuntimeException('Did\'t stored path for cookies. Permission denied.');
            }
        }
    }

    /** cookie name
     * @return string
     * */
    protected function cookieName()
    {
        $nameFile = head(explode('@', $this->authData['user_login']));
        $file = $this->cookiesPath . DIRECTORY_SEPARATOR . $nameFile . '.json';
        if (file_exists($file)) {
            return $file;
        }

        if ($fs = fopen($file, 'x')) {
            fclose($fs);
            chmod($file, 0664);
        }

        return $file;
    }
}
