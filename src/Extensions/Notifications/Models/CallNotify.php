<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.23
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Notifications\Models;

use Anemone\Extensions\Core\Helpers\ComputedBaseModel;
use Anemone\Extensions\Notifications\Contracts\BeNotificationModel;

/**
 * @property int $date
 * @property-read string $type
 * @property string $message
 * @property string $header
 * @property string $link
 * @property string $to
 * @property string $from
 * @property string $duration
 * @property int $user
 * @property array $element [id => 1235, type => 'contact', name => '']
 * @property string $click_link
 * */
class CallNotify extends ComputedBaseModel implements BeNotificationModel
{
    public function __construct(array $data = [])
    {
        parent::__construct($data);
        $this->data['type'] = 'call';
        // init if empty
        if (empty($this->data)) {
            $this->data['date'] = time();
        }
    }
}
