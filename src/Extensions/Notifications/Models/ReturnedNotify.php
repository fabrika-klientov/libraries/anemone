<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.23
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Notifications\Models;

use Anemone\Extensions\Core\Helpers\ComputedBaseModel;

/**
 * @property-read string $id
 * @property-read array $entity
 * @property-read array $linked_entity
 * @property-read int $created_at
 * @property-read int $updated_at
 * @property-read bool $is_read
 * @property-read bool $silent
 * @property-read array $body
 * */
class ReturnedNotify extends ComputedBaseModel
{
}
