<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.23
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Notifications\Models;

use Anemone\Extensions\Core\Helpers\ComputedBaseModel;
use Anemone\Extensions\Notifications\Contracts\BeNotificationModel;

/**
 * @property int $date
 * @property-read string $type
 * @property string $message
 * @property string $header
 * @property string $link
 * */
class ErrorNotify extends ComputedBaseModel implements BeNotificationModel
{
    public function __construct(array $data = [])
    {
        parent::__construct($data);
        $this->data['type'] = 'error';
        // init if empty
        if (empty($this->data)) {
            $this->data['date'] = time();
        }
    }
}
