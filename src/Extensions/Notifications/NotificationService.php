<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.23
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Notifications;

use Anemone\Contracts\BeClient;
use Anemone\Core\Collection\Collection;
use Anemone\Extensions\Notifications\Contracts\BeNotificationModel;
use Anemone\Extensions\Notifications\Models\ReturnedNotify;

class NotificationService
{
    /**
     * @var BeClient $client
     * */
    private $client;
    /**
     * @var \Anemone\Contracts\BeHttpClient $httpClient
     * */
    private $httpClient;

    /**
     * @param BeClient $client
     * @return void
     * */
    public function __construct(BeClient $client)
    {
        $this->client = $client;
        $this->httpClient = $this->client->getQueryService();
    }

    /** get notifications (only \Anemone\Extensions\NotApiClient)
     * @return Collection
     * */
    public function get()
    {
        $result = $this->httpClient->get(
            $this->httpClient->getUrl(aExtEntryData('notification_list'), $this->client->getDomain())
        );

        $result = json_decode($result, true);

        if (isset($result, $result['_embedded'], $result['_embedded']['items']) && is_array($result['_embedded']['items'])) {
            return new Collection(array_map(function ($item) {
                return new ReturnedNotify($item);
            }, $result['_embedded']['items']));
        }

        return new Collection();
    }

    /** push notify
     * @param Collection|BeNotificationModel $data
     * @return void
     * */
    public function store($data)
    {
        if ($data instanceof Collection) {
            $data->each(function (BeNotificationModel $item) {
                $this->store($item);
            });
        } elseif ($data instanceof BeNotificationModel) {
            $this->httpClient->post(
                $this->httpClient->getUrl(aExtEntryData('notification_store'), $this->client->getDomain()),
                [
                    "request" => $data->jsonSerialize(),
                ]
            );
        }
    }
}
