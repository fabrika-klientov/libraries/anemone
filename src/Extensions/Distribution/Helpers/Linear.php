<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Distribution\Helpers;

use Illuminate\Support\Collection;
use Anemone\Exceptions\AnemoneException;

trait Linear
{
    /**
     * @return mixed
     * @throws AnemoneException
     * */
    protected function getLinear()
    {
        switch ($this->distribution->getSource()) {
            case 'group':
                /**
                 * @var Collection|int|null $ids
                 * */
                $ids = $this->getIdsForGroup();

                if (!$ids instanceof Collection) {
                    return $ids;
                }

                $current = $this->linearCurrentHelper($ids);
                $this->distribution->setIds($ids->toArray(), $this->distribution->getIds()['id']);

                return $current;

            case 'any':
                /**
                 * @var Collection|int|null $ids
                 * */
                $ids = $this->getIdsForAny();

                if (!$ids instanceof Collection) {
                    return $ids;
                }

                $current = $this->linearCurrentHelper($ids);
                $this->distribution->setIds($ids->toArray());

                return $current;
        }

        throw new AnemoneException('Type of source [' . $this->distribution->getSource() . '] not supported');
    }

    /**
     * @param Collection $ids
     * @return int|null
     * */
    protected function linearCurrentHelper(Collection $ids)
    {
        if ($ids->isEmpty()) {
            return $this->getDef();
        }

        $result = collect(range(1, $ids->count()))
            ->reduce(
                function ($result, $item) use ($ids) {
                    if (isset($result)) {
                        return $result;
                    }

                    $current = $ids->shift();
                    $ids->push($current);

                    return in_array($current['id'], $this->distribution->getExcludes()) ? null : $current['id'];
                },
                null
            );

        return $result ?? $this->getDef();
    }
}
