<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Distribution\Helpers;

use Anemone\Core\Collection\Collection;
use Anemone\Exceptions\AnemoneException;
use Anemone\Models\User;

/**
 * @property-read \Anemone\Client $client
 * */
trait Common
{
    /**
     * @param int $groupId
     * @return \Anemone\Core\Collection\Collection
     * @throws AnemoneException
     * */
    protected function getUsers($groupId = null)
    {
        $users = $this->client->users->get();

        if (!empty($users) && $users->isNotEmpty()) {
            return isset($groupId)
                ? $users->filter(
                    function (User $user) use ($groupId) {
                        $groups = $user->groups()->isEmpty()
                            ? collect([['id' => 0, 'name' => 'Отдел продаж']])
                            : $user->groups();
                        return $groups->some('id', $groupId);
                    }
                )
                : $users;
        }

        throw new AnemoneException('User list is empty');
    }

    /**
     * @return \Illuminate\Support\Collection|int|null
     * @throws AnemoneException
     * */
    protected function getIdsForGroup()
    {
        $groupId = $this->distribution->getIds()['id'];
        $users = $this->getUsers($groupId);
        if ($users->isEmpty()) {
            return $this->getDef();
        }

        $oldUsers = $this->distribution->getIds($groupId);

        return $this->getIdsSync($users, $oldUsers);
    }

    /**
     * @return \Illuminate\Support\Collection|int|null
     * @throws AnemoneException
     * */
    protected function getIdsForAny()
    {
        $users = $this->getUsers();
        if ($users->isEmpty()) {
            return null;
        }

        $oldUsers = $this->distribution->getIds();

        return $this->getIdsSync($users, $oldUsers, true);
    }

    /**
     * @return mixed
     * @throws AnemoneException
     * */
    protected function getDef()
    {
        return $this->getUsers()->some('id', $this->distribution->getDefault())
            ? $this->distribution->getDefault()
            : null;
    }

    /**
     * @param Collection $users
     * @param array $oldUsers
     * @param bool $notPush
     * @return \Illuminate\Support\Collection
     * */
    protected function getIdsSync(Collection $users, array $oldUsers, bool $notPush = false)
    {
        $sync = collect($oldUsers)
            ->filter(
                function ($item) use ($users) {
                    return $users->some('id', $item['id']);
                }
            );

        if ($notPush) {
            return $sync;
        }

        return $sync->merge(
            $users
                ->filter(
                    function (User $user) use ($sync) {
                        return $sync->every('id', '!=', $user->id);
                    }
                )
                ->map(
                    function (User $user) {
                        return [
                            'id' => $user->id,
                            'config' => [],
                        ];
                    }
                )
        );
    }
}
