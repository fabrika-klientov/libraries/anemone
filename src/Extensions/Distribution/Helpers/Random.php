<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 20.01.21
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Distribution\Helpers;

use Anemone\Exceptions\AnemoneException;
use Illuminate\Support\Collection;

trait Random
{
    /**
     * @return mixed
     * @throws AnemoneException
     * */
    protected function getRandom()
    {
        switch ($this->distribution->getSource()) {
            case 'group':
                /**
                 * @var Collection|int|null $ids
                 * */
                $ids = $this->getIdsForGroup();

                if (!$ids instanceof Collection) {
                    return $ids;
                }

                $filtered = $ids
                    ->filter(
                        function ($item) {
                            return !in_array($item['id'], $this->distribution->getExcludes());
                        }
                    );
                $current = $filtered->isNotEmpty() ? $filtered->random() : null;

                return $current['id'] ?? $this->getDef();

            case 'any':
                /**
                 * @var Collection|int|null $ids
                 * */
                $ids = $this->getIdsForAny();

                if (!$ids instanceof Collection) {
                    return $ids;
                }

                $filtered = $ids
                    ->filter(
                        function ($item) {
                            return !in_array($item['id'], $this->distribution->getExcludes());
                        }
                    );
                $current = $filtered->isNotEmpty() ? $filtered->random() : null;

                return $current['id'] ?? $this->getDef();
        }

        throw new AnemoneException('Type of source [' . $this->distribution->getSource() . '] not supported');
    }
}
