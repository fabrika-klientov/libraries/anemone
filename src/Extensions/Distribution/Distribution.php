<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 20.01.21
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Distribution;

use Anemone\Contracts\BeDistribution;

/**
 * data => [
 *      type => linear|random,
 *      source => group|any,
 *      data => [ // users or group
 *          [id => 12345, config => [data => [ [id => 54321, config => []], ... ] ]], // for group
 *          [id => 54321, config => []],
 *      ],
 *      excludes => [12345], // id users
 *      default => 54321, // id user
 * ]
 * */
class Distribution implements BeDistribution
{
    /**
     * @var array $data
     * */
    private $data;

    /**
     * @param array $data
     * @return void
     * */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /** type of distribution [linear, random, ...]
     * @return string
     * */
    public function getType(): ?string
    {
        return $this->data['type'] ?? null;
    }

    /** type of source [group, any, ...]
     * @return string
     * */
    public function getSource(): ?string
    {
        return $this->data['source'] ?? null;
    }

    /** array of ids user or array of id group
     * @param int $id (id group)
     * @return array
     * */
    public function getIds($id = null): array
    {
        if (empty($this->data['data'])) {
            return [];
        }

        switch ($this->getSource()) {
            case 'group':
                $first = head(isset($id) ? array_filter($this->data['data'], function ($item) use ($id) {
                    return $item['id'] == $id;
                }) : $this->data['data']);
                return isset($first) ? (isset($id) ? ($first['config']['data'] ?? []) : $first) : [];

            case 'any':
                return $this->data['data'];
        }

        return [];
    }

    /** array of ids user is excluded [12345, 54321, ...]
     * @return array
     * */
    public function getExcludes(): array
    {
        return $this->data['excludes'] ?? [];
    }

    /** id user is default
     * @return int
     * */
    public function getDefault(): ?int
    {
        return $this->data['default'] ?? null;
    }

    /** set data (edited after next method service)
     * @param array $data
     * @param int $id (id group)
     * @return void
     * */
    public function setIds(array $data, $id = null)
    {
        switch ($this->getSource()) {
            case 'group':
                if (isset($id)) {
                    $this->data['data'] = array_map(function ($item) use ($data, $id) {
                        if ($item['id'] == $id) {
                            $item['config']['data'] = $data;
                        }
                        return $item;
                    }, $this->data['data']);
                } else {
                    $this->data['data'][0]['config']['data'] = $data;
                }
                break;

            case 'any':
                $this->data['data'] = $data;
        }
    }

    /** result
     * @return array
     * */
    public function result(): array
    {
        return $this->data;
    }

}