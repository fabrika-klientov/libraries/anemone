<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Distribution;

use Anemone\Client;
use Anemone\Contracts\BeDistribution;
use Anemone\Exceptions\AnemoneException;
use Anemone\Extensions\Distribution\Helpers\Common;
use Anemone\Extensions\Distribution\Helpers\Linear;
use Anemone\Extensions\Distribution\Helpers\Random;

class DistributionService
{
    use Common;
    use Linear;
    use Random;

    /**
     * @var Client $client
     * */
    private $client;
    /**
     * @var BeDistribution $distribution
     * */
    private $distribution;

    /**
     * @param Client $client
     * */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /** init
     * @param BeDistribution $distribution
     * @return void
     * */
    public function init(BeDistribution $distribution)
    {
        $this->distribution = $distribution;
    }

    /**
     * @return mixed
     * @throws AnemoneException
     * */
    public function next()
    {
        if (!isset($this->distribution)) {
            throw new AnemoneException('Before run next - init the BeDistribution in method [init]');
        }

        switch ($this->distribution->getType()) {
            case 'linear':
                return $this->getLinear();

            case 'random':
                return $this->getRandom();
        }

        throw new AnemoneException('Type of distribution [' . $this->distribution->getType() . '] not supported');
    }

    /**
     * @return array
     * @throws AnemoneException
     * */
    public function result()
    {
        if (!isset($this->distribution)) {
            throw new AnemoneException('Before run result - init the BeDistribution in method [init]');
        }

        return $this->distribution->result();
    }
}
