<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Groups;

use Anemone\Contracts\BeClient;
use Anemone\Contracts\BeCustomField;
use Anemone\Core\Collection\Collection;
use Anemone\Exceptions\AnemoneException;
use Anemone\Extensions\Groups\Models\Group;

/**
 * @deprecated
 * */
class GroupsService
{
    /**
     * @var BeClient $client
     * */
    private $client;
    /**
     * @var \Anemone\Contracts\BeHttpClient $httpClient
     * */
    private $httpClient;
    /**
     * @var array $elementTypes
     * */
    protected $elementTypes = [
        'leads' => 2,
        'contacts' => 1,
        'companies' => 3,
        'customers' => 12,
    ];

    /**
     * @param BeClient $client
     * @return void
     * @deprecated in v4 exist
     */
    public function __construct(BeClient $client)
    {
        $this->client = $client;
        $this->httpClient = $this->client->getQueryService();
    }

    /**
     * @param string $entity [leads, contacts, companies, customers]
     * @return array|Collection|null
     * @deprecated
     */
    public function all(string $entity = null)
    {
        $result = $this->httpClient->get(
            $this->httpClient->getUrl(aExtEntryData('link_custom_fields'), null, true),
            [],
            [
                'headers' => [
                    'X-Requested-With' => 'XMLHttpRequest',
                    'Accept' => 'application/json',
                ],
            ]
        );

        $result = json_decode($result, true);

        if (isset($result['response']['params']['groups'])) {
            $groups = [];
            foreach ($result['response']['params']['groups'] as $type => $values) {
                $groups[$type] = new Collection(
                    array_map(
                        function ($item) {
                            return new Group($item);
                        },
                        $values ?? []
                    )
                );
            }

            return isset($entity) ? ($groups[$entity] ?? null) : $groups;
        }

        return null;
    }

    /**
     * @param Collection $collect
     * @param string $entity [leads, contacts, companies, customers]
     * @return Collection|null of id groups
     * @throws AnemoneException
     * @deprecated
     */
    public function store($collect, string $entity = 'leads')
    {
        $issetGroups = $this->validateBeforeSave($collect, $entity);

        $collect = $collect->filter(
            function (Group $group) {
                return $group->id != 'default';
            }
        );

        $merged = $issetGroups->merge($collect);

        return $this->save($merged, $entity);
    }

    /**
     * @param Collection $collect
     * @param string $entity [leads, contacts, companies, customers]
     * @return Collection|null of id groups
     * @throws AnemoneException
     * @deprecated
     */
    public function update($collect, string $entity = 'leads')
    {
        $issetGroups = $this->validateBeforeSave($collect, $entity);

        $collect = $collect->filter(
            function (Group $group) {
                return $group->id != 'default';
            }
        );

        if ($issetGroups->count() != $collect->count()) {
            throw new AnemoneException('Count elements of group should be equals to count of amo servers');
        }

        if (!$collect->every(
            function (Group $group) use ($issetGroups) {
                return $issetGroups->some(
                    function (Group $item) use ($group) {
                        return $item->id == $group->id;
                    }
                );
            }
        )) {
            throw new AnemoneException('Elements of group is not equals');
        }

        return $this->save($collect, $entity);
    }

    /**
     * @param Collection $collect
     * @param string $entity [leads, contacts, companies, customers]
     * @return Collection|null of id groups
     * @throws AnemoneException
     * @deprecated
     */
    public function destroy($collect, string $entity = 'leads')
    {
        $collect = $collect->filter(
            function (Group $group) {
                return $group->id != 'default';
            }
        );

        $issetGroups = $this->validateBeforeSave($collect, $entity);

        $filtered = $issetGroups->filter(
            function (Group $group) use ($collect) {
                return $collect->every(
                    function (Group $item) use ($group) {
                        return $item->id != $group->id;
                    }
                );
            }
        );

        return $this->save($filtered, $entity);
    }

    /**
     * @param Collection $collect
     * @param string $entity [leads, contacts, companies, customers]
     * @return Collection|null of id groups
     * @deprecated
     */
    protected function save($collect, string $entity)
    {
        $collect->each(
            function (Group $group) use ($entity) {
                if (isset($group->fields) && $group->fields instanceof Collection) {
                    $group->fields = $group->fields
                        ->filter(
                            function (BeCustomField $item) use ($entity) { // element_type filtering
                                return $item->{'element_type'} == $this->getElementType($entity);
                            }
                        )
                        ->map(
                            function (BeCustomField $item) {
                                return $item->id;
                            }
                        )
                        ->values();
                }
            }
        );

        $result = $this->httpClient->post(
            $this->httpClient->getUrl(aExtEntryData('link_custom_fields'), null, true),
            [],
            [
                'headers' => [
                    'X-Requested-With' => 'XMLHttpRequest',
                    'Accept' => 'application/json',
                ],
                'form_params' => [
                    'action' => 'apply_changes',
                    'element_type' => $this->getElementType($entity),
                    'groups' => json_decode((string)json_encode($collect), true),
                ],
            ]
        );

        $result = json_decode($result, true);

        if (isset($result['response']['group_id'])) {
            return new Collection($result['response']['group_id']);
        }

        return null;
    }

    /**
     * @param Collection $collect
     * @param string $entity [leads, contacts, companies, customers]
     * @return Collection
     * @throws AnemoneException
     * */
    protected function validateBeforeSave($collect, string $entity)
    {
        if (!$this->validateEntity($entity)) {
            throw new AnemoneException(
                'Entity ' . $entity . ' not supported. only ["leads", "contacts", "companies", "customers"]'
            );
        }

        if (empty($collect) || $collect->isEmpty()) {
            throw new AnemoneException('Collect is not be empty');
        }

        /**
         * @var Collection $issetGroups
         * */
        $issetGroups = $this->all($entity);

        if (empty($issetGroups) || $issetGroups->isEmpty()) {
            throw new AnemoneException('All groups not loaded');
        }

        return $issetGroups->filter(
            function (Group $group) {
                return $group->id != 'default';
            }
        );
    }

    /**
     * @param string $entity
     * @return bool
     * */
    protected function validateEntity(string $entity)
    {
        return in_array($entity, ['leads', 'contacts', 'companies', 'customers']);
    }

    /**
     * @param string $entity
     * @return int
     * */
    protected function getElementType(string $entity)
    {
        return $this->elementTypes[$entity] ?? null;
    }
}
