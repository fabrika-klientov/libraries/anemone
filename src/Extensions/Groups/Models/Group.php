<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Groups\Models;

use Anemone\Extensions\Core\BaseModel;

/**
 * @deprecated in v4 exist
 * @property string $id
 * @property string $name
 * @property array|\Anemone\Core\Collection\Collection $fields
 * */
class Group extends BaseModel
{

}
