<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions;

use Anemone\Contracts\BeClient;
use Anemone\Extensions\Core\Helpers\Services;
use Anemone\Extensions\Core\Helpers\Validators;
use Anemone\Extensions\Core\Services\AuthService;
use Anemone\Extensions\Core\Services\HttpClientService;

class NotApiClient implements BeClient
{
    use Services;
    use Validators;

    /**
     * @var HttpClientService $httpClientService
     * */
    private $httpClientService;
    /**
     * @var AuthService $authService
     * */
    private $authService;
    private $domain;

    /**
     * @param array $authData [user_login => string, user_password => string]
     * @param string $domain
     * @return void
     * @throws \Anemone\Exceptions\InvalidDataException
     * */
    public function __construct(array $authData, string $domain)
    {
        $this->authService = new AuthService($this->validate($authData), $domain);
        $this->httpClientService = new HttpClientService($this->authService);
        $this->domain = $domain;
    }

    /**
     * @override
     * @return HttpClientService $httpClientService
     * */
    public function getQueryService()
    {
        return $this->httpClientService;
    }

    /**
     * @return AuthService $authService
     * */
    public function getAuthService()
    {
        $this->authService->initHttpClientService($this->httpClientService);
        return $this->authService;
    }

    /** account domain
     * @override
     * @return string
     * */
    public function getDomain()
    {
        return $this->domain;
    }
}
