<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Widgets;

use Anemone\Contracts\BeClient;
use Anemone\Exceptions\InvalidDataException;
use Anemone\Extensions\Widgets\Models\ApiWidget;
use Anemone\Extensions\Widgets\Models\Integration;
use Anemone\Extensions\Widgets\Models\Widget;
use Illuminate\Support\Collection;

class WidgetsService
{

    /**
     * @var BeClient $client
     * */
    private $client;
    /**
     * @var \Anemone\Contracts\BeHttpClient $httpClient
     * */
    private $httpClient;
    /**
     * @var string[] $widgetCategories
     * */
    protected static $widgetCategories = [
        'phone',
        'mail',
        'site',
        'chats',
        'useful',
    ];
    /**
     * @var string[] $widgetUnCategories
     * */
    protected static $widgetUnCategories = [
        'recommended',
        'own_integrations',
    ];

    /**
     * @param BeClient $client
     * @return void
     * */
    public function __construct(BeClient $client)
    {
        $this->client = $client;
        $this->httpClient = $this->client->getQueryService();
    }

    /**
     * @param string $filter in 'phone', 'mail', 'site', 'chats', 'useful', 'recommended', 'own_integrations'
     * if null -> only 'phone', 'mail', 'site', 'chats', 'useful'
     * @return Collection
     * @throws InvalidDataException
     * @throws \Exception
     * */
    public function all($filter = null)
    {
        // load all widgets (not own and not recommend)
        if (is_null($filter) || in_array($filter, self::$widgetCategories)) {
            return $this->getListWidgetsHelper(new Collection(), $filter, self::$widgetCategories);
        }

        if (!in_array($filter, self::$widgetUnCategories)) {
            throw new InvalidDataException('Type filter [' . $filter . '] not supported. Only ' . json_encode(array_merge(self::$widgetCategories, self::$widgetUnCategories)));
        }

        // recommended, own_integrations
        return $this->getListWidgetsHelper(new Collection(), $filter, [$filter]);
    }

    /** get api widgets
     * @return Collection
     * */
    public function allApi()
    {
        return ApiWidget::all($this->httpClient);
    }

    /** store new widget
     * @param string $code
     * @return ApiWidget
     * @throws InvalidDataException
     * @throws \Anemone\Exceptions\BadResponseException
     * */
    public function storeApiWidget(string $code)
    {
        return (new ApiWidget(['code' => $code], $this->httpClient))->store();
    }

    /** pack widget in zip and return path to widget
     * @param string $dir
     * @param string $code
     * @param string $secret
     * @param string $widgetName
     * @return string
     * @throws InvalidDataException
     * */
    public function packWidget(string $dir, string $code, string $secret = null, $widgetName = 'widget.zip')
    {
        return ApiWidget::pack($dir, $code, $secret, $widgetName);
    }

    /**
     * @param Collection $collection
     * @param string|null $filter
     * @param array $next
     * @param int $page
     * @return Collection
     * */
    protected function getListWidgetsHelper($collection, $filter, $next, $page = 1)
    {
        $limit = 100;

        if (empty($next)) {
            return $collection;
        }

        $helper = function () use ($filter, $page, $limit, $next) {
            $allCategories = array_merge(self::$widgetCategories, self::$widgetUnCategories);

            return array_reduce($allCategories, function ($result, $item) use ($filter, $page, $limit, $next) {
                if ((is_null($filter) || $item == $filter) && in_array($item, $next)) {
                    $link = aExtEntryData('link_widget_list_' . $item);
                    $result[$item] = $this->httpClient->get(
                        $this->httpClient->getUrl($link, $this->client->getDomain()) . '/' . $page,
                        ['limit' => $limit],
                        [
                            'headers' => [
                                'X-Requested-With' => 'XMLHttpRequest',
                                'Accept' => 'application/json',
                            ],
                        ],
                        true,
                        false,
                        true
                    );
                }
                return $result;
            }, []);
        };

        $result = $this->httpClient->merge($helper);
        $next = [];

        $collection = array_reduce(
            array_map(function ($item, $key) use ($page, $limit, &$next) {
                $data = json_decode($item->getBody()->getContents(), true);
                if ($page * $limit < $data['widgets']['count']) {
                    $next[] = $key;
                }
                return array_reduce($data['widgets'][$key], function (Collection $collect, $cat) {
                    return $collect->merge(array_map(function ($item) {
                        return $item['type'] == 'widget' ? new Widget($item) : new Integration($item);
                    }, $cat));
                }, new Collection());
            }, $result, array_keys($result)),
            function (Collection $collection, $item) {
                return $collection->merge($item);
            },
            $collection
        );

        return $this->getListWidgetsHelper($collection, $filter, $next, ++$page);
    }
}
