<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.03
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Widgets\Models;

/**
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $version
 * @property string $must_pay
 * @property string $installable
 * @property array $settings
 * @property bool $active
 * @property string $status
 * @property string $logo
 * @property string $logo_small
 * @property int $first_load
 * @property string $descr
 * @property array $langs
 * @property string $categories
 * @property string $category_code
 * @property bool $dp_ready
 * @property bool $is_lead_source
 * @property bool $is_catalog_sdk
 * @property array $tour
 * @property int $rating
 * @property bool $advanced_settings
 * @property string $type
 * @property int $sort
 * @property bool $new_design
 * @property mixed $support
 * @property bool $is_showcase
 * @property string $state
 * @property bool $is_not_fully_installed
 * */
class Widget extends WIBase
{
}
