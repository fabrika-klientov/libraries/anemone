<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.03.12
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Widgets\Models;

/**
 * @property-read string $uuid
 * @property-read string $name
 * @property-read string $description
 * @property-read int $updated_at
 * @property-read int $version_time
 * @property-read bool $is_enabled
 * @property-read array $_links
 * @property-read array $langs
 * @property-read string $type
 * @property-read string $state
 *
 * // only list (method all in IntegrationService)
 * @property-read string $logo
 * @property-read array $scopes
 *
 * // only detail (method find in IntegrationService)
 * @property string $secret
 * @property-read bool $is_editable
 * @property-read bool $has_widget
 * @property-read string $auth_code
 * @property-read bool $is_outdated
 * @property-read bool $has_logo
 * @property-read array $_embedded [settings => array, scopes => array]
 *
 * @property-read string $redirect_uri
 * */
class Integration extends WIBase
{
}
