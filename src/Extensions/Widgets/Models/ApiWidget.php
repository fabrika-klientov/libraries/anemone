<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Widgets\Models;

use Anemone\Client;
use Anemone\Contracts\BeClient;
use Anemone\Contracts\BeHttpClient;
use Anemone\Exceptions\BadResponseException;
use Anemone\Exceptions\InvalidDataException;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

/**
 * @property int $id
 * @property array $additional_data
 * @property string $code
 * @property string $secret_key
 * @property string $version
 * @property int $installs
 * @property array $status
 * @property array $upload
 * @property array $delete
 * @property array $settings_template
 * @property array $settings
 * @property bool $active
 * */
class ApiWidget extends WIBase
{
    private $httpClient;

    /**
     * @param array $data
     * @param BeHttpClient $httpClient
     * @return void
     * */
    public function __construct(array $data, BeHttpClient $httpClient)
    {
        parent::__construct($data);
        $this->httpClient = $httpClient;
    }

    /**
     * @return static
     * @throws InvalidDataException
     * @throws BadResponseException
     * */
    public function store()
    {
        if (empty($this->data['code'])) {
            throw new InvalidDataException('Widget code is required');
        }

        $result = $this->httpClient->post(
            $this->httpClient->getUrl(aExtEntryData('link_widget_list_api'), null, true),
            [
                'action' => 'create',
                'page' => 1,
                'code' => $this->data['code'],
            ],
            [
                'headers' => [
                    'X-Requested-With' => 'XMLHttpRequest',
                ],
            ]
        );

        $result = json_decode($result, true);

        if (isset($result)) {
            if (isset($result['error'])) {
                throw new BadResponseException('Amo returned error. ' . $result['error']);
            }

            return self::find($this->httpClient, $this->data['code']);
        }

        throw new BadResponseException('Amo returned NULL.');
    }

    /**
     * @deprecated
     * @return bool
     * @throws BadResponseException
     * @throws InvalidDataException
     * */
    public function destroy()
    {
        if (empty($this->data['code']) || empty($this->data['secret_key'])) {
            throw new InvalidDataException('[code] and [secret_key] is required');
        }

        $profile = $this->getProfile();
        if (empty($profile)) {
            throw new BadResponseException('For deleting widget need profile data. NULL returned');
        }

        $link = str_replace('{sub}', $profile['subdomain'], aExtEntryData('link_widget_destroy'));
        $zone = Str::afterLast($this->httpClient->getUrl(''), '.');

        $result = $this->httpClient->post(
            $this->httpClient->getUrl($link, str_replace('{zone}', $zone, aExtEntryData('widgets_domain')), true),
            [],
            [
                'form_params' => [
                    'amouser' => $profile['arUser']['login'],
                    'amohash' => $profile['arUser']['password_hash'],
                    'secret' => $this->data['secret_key'],
                    'widget' => $this->data['code'],
                ],
            ]
        );

        return preg_match('/(response|true)/', $result) == 1;
    }

    /** uploading widget to server
     * @param string $widget
     * @param bool $integration
     * @return bool
     * @throws InvalidDataException
     * @throws BadResponseException
     * *@deprecated See self::uploadNew method
     */
    public function upload(string $widget, bool $integration = false)
    {
        if (empty($widget) || !file_exists($widget) || !is_file($widget)) {
            throw new InvalidDataException('File widget [' . $widget . '] not found or not .zip file.');
        }

        if (empty($this->data['code']) || empty($this->data['secret_key'])) {
            throw new InvalidDataException('[code] and [secret_key] is required');
        }

//        $profile = $this->getProfile();
//        if (empty($profile)) {
//            throw new BadResponseException('For uploading widget need profile data. NULL returned');
//        }
        preg_match('/^https?:\/\/(.+?)\..*/', $this->httpClient->getUrl('', null, true), $res);
        $sub = $res[1] ?? 'ALARM';
        $link = str_replace('{sub}', $sub, aExtEntryData('link_widget_upload'));
        $zone = str_replace('/', '', Str::afterLast($this->httpClient->getUrl(''), '.'));

        if ($integration) {
            $integrationData = [
                [
                    'name' => 'client_id',
                    'contents' => $this->data['client_uuid'],
                ],
                [
                    'name' => '_widget',
                    'contents' => 'widget.zip',
                ],
            ];
        }

        $result = $this->httpClient->post(
            $this->httpClient->getUrl($link, str_replace('{zone}', $zone, aExtEntryData('widgets_domain')), true),
            [],
            [
                'multipart' => array_merge(
                    [
                        [
                            'name' => 'widget',
                            'contents' => file_get_contents($widget),
                            'filename' => 'widget.zip',
                            'Content-Type' => 'application/zip',
                        ],
//                        [
//                            'name' => 'amouser',
//                            'contents' => $profile['arUser']['login'],
//                        ],
//                        [
//                            'name' => 'amohash',
//                            'contents' => $profile['arUser']['password_hash'],
//                        ],
                        [
                            'name' => 'secret',
                            'contents' => $this->data['secret_key'],
                        ],
                        [
                            'name' => 'widget',
                            'contents' => $this->data['code'],
                        ],
                    ],
                    $integrationData ?? []
                ),
            ]
        );

        return preg_match('/(response|true)/', $result) == 1;
    }

    /**
     * new version
     * @param string $widget
     * @param string $clientUuid
     * @param BeClient $client
     * @return mixed
     * @throws InvalidDataException
     */
    public static function uploadNew(string $widget, string $clientUuid, BeClient $client)
    {
        if (empty($widget) || !file_exists($widget) || !is_file($widget)) {
            throw new InvalidDataException('File widget [' . $widget . '] not found or not .zip file.');
        }

        $httpClient = $client->getQueryService();

        $link = str_replace('{uuid}', $clientUuid, aExtEntryData('link_upload_integration'));

        if ($client instanceof Client) {
            // injecting another auth data
            $domain = '.' . Str::after($client->getDomain(), '.');
            $jar = CookieJar::fromArray(
                [
                    'access_token' => $client->getAuthService()->getAuthParams()['access_token'],
                ],
                $domain
            );
        }

        $options = [
            'multipart' => [
                [
                    'name' => 'widget',
                    'contents' => file_get_contents($widget),
                    'filename' => 'widget.zip',
                    'Content-Type' => 'application/zip',
                ],
                [
                    'name' => '_widget',
                    'contents' => 'widget.zip',
                ],
            ],
            'headers' => [
                'X-Requested-With' => 'XMLHttpRequest',
            ],
        ];

        if (isset($jar)) {
            $options['cookies'] = $jar;
        }

        $result = $httpClient->post(
            $httpClient->getUrl($link, null, true),
            [],
            $options
        );

        return json_decode($result, true);
    }

    /** set enable/disable widget
     * @param bool $status
     * @return bool
     * */
    public function power(bool $status)
    {
        return $this->setSettings($this->data['settings'], $status);
    }

    /** set settings
     * @param array $settings
     * @param bool $power
     * @return bool
     * */
    public function setSettings(array $settings, bool $power = null)
    {
        $this->data['settings'] = $settings;

        if (isset($power)) {
            $this->data['active'] = $power;
            $this->data['settings']['active'] = $power ? 'Y' : 'N';
        }

        $settingsParams = [];
        foreach ($this->data['settings'] as $key => $value) {
            if ($key != 'active') {
                $settingsParams[$key] = $value;
            }
        }
        $result = $this->httpClient->post(
            $this->httpClient->getUrl(aExtEntryData('link_widget_edit'), null, true),
            [
                'action' => 'edit',
                'id' => $this->data['id'],
                'code' => $this->data['code'],
                'widget_active' => isset($this->data['active']) && $this->data['active'] ? 'Y' : 'N',
                'settings' => $settingsParams,
            ],
            [
                'headers' => [
                    'X-Requested-With' => 'XMLHttpRequest',
                ],
            ]
        );

        $result = json_decode($result, true);

        if (empty($result)) {
            return false;
        }

        return $result['response']['status'] == 'ok';
    }

    /** get list in collect ApiWidget
     * @param BeHttpClient $httpClient
     * @return Collection
     * */
    public static function all(BeHttpClient $httpClient)
    {
        $result = $httpClient->get(
            $httpClient->getUrl(aExtEntryData('link_widget_list_api'), null, true),
            [],
            [
                'headers' => [
                    'X-Requested-With' => 'XMLHttpRequest',
                    'Accept' => 'application/json',
                ],
            ]
        );

        $result = json_decode($result, true);
        if (empty($result) || empty($result['response']['widgets']['items'])) {
            return new Collection();
        }

        $collection = new Collection(
            array_map(
                function ($item) use ($httpClient) {
                    return new self($item, $httpClient);
                },
                $result['response']['widgets']['items']
            )
        );

        $result = $httpClient->get(
            $httpClient->getUrl(aExtEntryData('link_widget_list_all_api'), null, true),
            [
                'widget_id' => $collection->map(
                    function ($item) {
                        return $item->id;
                    }
                )->join(','),
            ],
            [
                'headers' => [
                    'X-Requested-With' => 'XMLHttpRequest',
                    'Accept' => 'application/json',
                ],
            ]
        );

        $result = json_decode($result, true);
//        if (empty($result) || empty($result['_embedded']['items'])) {
//            return new Collection();
//        }

        return $collection->map(
            function ($item) use ($result) {
                $one = head(
                    array_filter(
                        $result['_embedded']['items'] ?? [],
                        function ($one) use ($item) {
                            return $one['widget_id'] == $item->id;
                        }
                    )
                );

                if (!empty($one)) {
                    $item->settings_template = $one['settings_template'] ?? null;
                    $item->settings = $one['settings'] ?? null;
                    $item->active = $one['active'] == 1;
                }

                return $item;
            }
        );
    }

    /**
     * @param BeHttpClient $httpClient
     * @param string $code
     * @return static
     * @throws InvalidDataException
     * */
    public static function find(BeHttpClient $httpClient, $code)
    {
        if (empty($code)) {
            throw new InvalidDataException('Code is required');
        }

        return self::all($httpClient)->first(
            function ($item) use ($code) {
                return $item->code == $code;
            }
        );
    }

    /** array of widgets with integration
     * @param BeHttpClient $httpClient
     * @return array
     *
     * @throws BadResponseException
     */
    public static function allInArea(BeHttpClient $httpClient)
    {
        try {
            $result = json_decode(
                $httpClient->post(
                    $httpClient->getUrl(
                        aExtEntryData('link_widget_list_with_user_settings'),
                        null,
                        true
                    ),
                    [],
                    [
                        'headers' => [
                            'X-Requested-With' => 'XMLHttpRequest',
                            'Accept' => 'application/json',
                        ],
                        'form_params' => [
                            'area' => 'widgetsSettings',
                        ],
                    ]
                ),
                true
            );

            if (isset($result['widgets'])) {
                foreach ($result['widgets'] as $key => &$widget) {
                    $widget['code'] = $key;
                }

                return array_values($result['widgets']);
            }

            return [];
        } catch (\Exception $exception) {
            throw new BadResponseException($exception->getMessage());
        }
    }

    /** one of widgets with integration
     * @param BeHttpClient $httpClient
     * @param string $codeOrUuidIntegration
     * @return array|null
     *
     * @throws BadResponseException
     */
    public static function findInArea(BeHttpClient $httpClient, string $codeOrUuidIntegration)
    {
        return collect(static::allInArea($httpClient))->first(
            function ($item) use ($codeOrUuidIntegration) {
                return $item['code'] == $codeOrUuidIntegration
                    || (isset($item['oauth_client_uuid']) && $item['oauth_client_uuid'] == $codeOrUuidIntegration);
            }
        );
    }

    /** array of widgets with integration OWN
     * @param BeHttpClient $httpClient
     * @return array
     *
     * @throws BadResponseException
     */
    public static function allOwnIntegrations(BeHttpClient $httpClient)
    {
        try {
            $result = json_decode(
                $httpClient->get(
                    $httpClient->getUrl(
                        aExtEntryData('link_widget_list_own_integrations') . '/1', // page
                        null,
                        true
                    ),
                    [],
                    [
                        'headers' => [
                            'X-Requested-With' => 'XMLHttpRequest',
                            'Accept' => 'application/json',
                        ],
                    ]
                ),
                true
            );

            $helper = function (array $list) {
                $complete = [];
                foreach ($list as $key => &$widget) {
                    if (empty($widget['code'])) {
                        $widget['code'] = $key;
                    }

                    $complete[] = $widget;
                }

                return $complete;
            };

            $complete = array_merge(
                array_values($helper($result['widgets']['own_integrations']['installed'] ?? [])),
                array_values($helper($result['widgets']['own_integrations']['not_installed'] ?? [])),
                array_values($helper($result['integrations'] ?? []))
            );

            return array_values($complete);
        } catch (\Exception $exception) {
            throw new BadResponseException($exception->getMessage());
        }
    }

    /** one of widgets with integration OWN
     * @param BeHttpClient $httpClient
     * @param string $codeOrUuidIntegration
     * @return array|null
     *
     * @throws BadResponseException
     */
    public static function findOwnIntegration(BeHttpClient $httpClient, string $codeOrUuidIntegration)
    {
        return collect(static::allOwnIntegrations($httpClient))->first(
            function ($item) use ($codeOrUuidIntegration) {
                return $item['code'] == $codeOrUuidIntegration
                    || (isset($item['client']['uuid']) && $item['client']['uuid'] == $codeOrUuidIntegration)
                    || (isset($item['oauth_client_uuid']) && $item['oauth_client_uuid'] == $codeOrUuidIntegration);
            }
        );
    }

    /** pack/repack widget from path folder or path file .zip
     * @param string $dir
     * @param string $code
     * @param string $secret
     * @param string $filename
     * @param bool $isIntegration
     * @return string
     * @throws InvalidDataException
     * */
    public static function pack(
        string $dir,
        string $code = null,
        string $secret = null,
        string $filename = 'widget.zip',
        bool $isIntegration = false
    ) {
        if (is_dir($dir)) {
            $dir = (string)preg_replace('/\/$/', '', $dir);
            $zipFile = $dir . DIRECTORY_SEPARATOR . 'widget.zip';
        } elseif (is_file($dir) && last(explode('.', $dir)) == 'zip') {
            $pathInArray = explode(DIRECTORY_SEPARATOR, $dir);
            $oldName = array_pop($pathInArray);
            $dir = join(DIRECTORY_SEPARATOR, $pathInArray);
            $zipFile = $dir . DIRECTORY_SEPARATOR . $oldName;
            $tmpDir = $dir . DIRECTORY_SEPARATOR . 'tmp_' . time();

            $zip = new ZipArchive();
            if ($zip->open($zipFile) === true) {
                $zip->extractTo($tmpDir);
                $zip->close();
            } else {
                throw new InvalidDataException('Not extracted zip file in tmp path');
            }
        } else {
            throw new InvalidDataException('Params $dir should be [DIR] or [FILE] type of [ZIP]');
        }

        $zip = new ZipArchive();
        if ($zip->open($zipFile, ZipArchive::CREATE | ZipArchive::OVERWRITE) !== true) {
            throw new \RuntimeException('Archive not created. File [' . $zipFile . ']');
        }

        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($tmpDir ?? $dir),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file) {
            if (!$file->isDir()) {
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($tmpDir ?? $dir) + 1);

                if ($relativePath == 'widget.zip' || (isset($oldName) && $relativePath == $oldName)) {
                    continue;
                }

                // manifest action
                if ($relativePath == 'manifest.json') {
                    $manifest = json_decode((string)file_get_contents($filePath), true);

                    if ($isIntegration) {
                        unset($manifest['widget']['code'], $manifest['widget']['secret_key']);
                    } else {
                        $manifest['widget']['code'] = $code;
                        $manifest['widget']['secret_key'] = $secret;
                    }

                    $fp = fopen($filePath, "w");
                    if ($fp) {
                        fwrite($fp, (string)json_encode($manifest));
                        fclose($fp);
                    } else {
                        throw new \RuntimeException('Permission denied');
                    }
                }

                $zip->addFile($filePath, $relativePath);
            }
        }

        $zip->close();
        $newFile = $dir . DIRECTORY_SEPARATOR . $filename;
        rename($zipFile, $newFile);

        // delete the tmp path
        if (isset($tmpDir)) {
            $iterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($tmpDir, \FilesystemIterator::SKIP_DOTS),
                RecursiveIteratorIterator::CHILD_FIRST
            );

            foreach ($iterator as $path) {
                if ($path->isDir()) {
                    rmdir((string)$path);
                } else {
                    unlink((string)$path);
                }
            }
            rmdir($tmpDir);
        }

        return $newFile;
    }

    /** profile data
     * @return array|null
     * */
    protected function getProfile()
    {
        $result = $this->httpClient->get(
            $this->httpClient->getUrl(aExtEntryData('link_profile'), null, true),
            [],
            [
                'headers' => [
                    'X-Requested-With' => 'XMLHttpRequest',
                    'Accept' => 'application/json',
                ],
            ]
        );

        $result = json_decode($result, true);

        if (isset($result)) {
            return $result['response']['params'] ?? null;
        }

        return null;
    }
}
