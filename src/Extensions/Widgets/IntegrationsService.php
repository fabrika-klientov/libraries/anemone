<?php
/**
 *
 * @package   Anemone
 * @category  Extensions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.30
 * @link      https://fabrika-klientov.ua
 */

namespace Anemone\Extensions\Widgets;

use Anemone\Client;
use Anemone\Contracts\BeClient;
use Anemone\Exceptions\AnemoneException;
use Anemone\Exceptions\BadResponseException;
use Anemone\Extensions\Widgets\Models\ApiWidget;
use Anemone\Extensions\Widgets\Models\Integration;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class IntegrationsService
{

    /**
     * @var BeClient $client
     * */
    private $client;
    /**
     * @var \Anemone\Contracts\BeHttpClient $httpClient
     * */
    private $httpClient;

    /**
     * @param BeClient $client
     * @return void
     * */
    public function __construct(BeClient $client)
    {
        $this->client = $client;
        $this->httpClient = $this->client->getQueryService();
    }

    /**
     * @return Collection
     * */
    public function all()
    {
        $result = json_decode(
            $this->httpClient->get(
                $this->httpClient->getUrl(aExtEntryData('link_integration'), $this->client->getDomain()),
                [],
                [
                    'headers' => [
                        'X-Requested-With' => 'XMLHttpRequest',
                        'Accept' => 'application/json',
                    ],
                ]
            ),
            true
        );

        if (isset($result['_embedded']['items'])) {
            return new Collection(
                array_map(
                    function ($item) {
                        return new Integration($item);
                    },
                    $result['_embedded']['items'] ?? []
                )
            );
        }

        return collect();
    }

    /**
     * @param string $id
     * @return Integration|null
     * */
    public function find(string $id)
    {
        $result = json_decode(
            $this->httpClient->get(
                $this->httpClient->getUrl(aExtEntryData('link_integration') . '/' . $id, $this->client->getDomain()),
                [],
                [
                    'headers' => [
                        'X-Requested-With' => 'XMLHttpRequest',
                        'Accept' => 'application/json',
                    ],
                ]
            ),
            true
        );

        if (isset($result)) {
            return new Integration($result);
        }

        return null;
    }

    /** store or update integration (ONLY USING \Anemone\Extensions\NotApiClient)
     * @param array $name
     * @param array $description
     * @param array $scopes
     * @param string $redirect_uri
     * @param string $uuid
     * @return Integration|null
     *
     * @throws AnemoneException
     */
    public function save(array $name, array $description, array $scopes, string $redirect_uri, string $uuid = null)
    {
        try {
            $result = json_decode(
                $this->httpClient->{isset($uuid) ? 'patch' : 'post'}(
                    $this->httpClient->getUrl(
                        aExtEntryData('link_integration') . (isset($uuid) ? ('/' . $uuid) : ''),
                        $this->client->getDomain()
                    ),
                    [
                        'name' => $name,
                        'description' => $description,
                        'scopes' => $scopes,
                        'redirect_uri' => $redirect_uri,
                        'uuid' => $uuid,
                    ],
                    [
                        'headers' => [
                            'X-Requested-With' => 'XMLHttpRequest',
                            'Accept' => 'application/json',
                        ],
                    ]
                ),
                true
            );

            if (isset($result)) {
                return new Integration($result);
            }

            return null;
        } catch (\Exception $exception) {
            throw new AnemoneException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param Integration|string $integration
     * @return bool
     * */
    public function delete($integration)
    {
        if ($integration instanceof Integration) {
            $integration = $integration->uuid;
        }

        if (!empty($integration)) {
            try {
                $this->httpClient->delete(
                    $this->httpClient->getUrl(
                        str_replace('{uuid}', $integration, aExtEntryData('link_delete_widget_integration')),
                        $this->client->getDomain()
                    ),
                    [],
                    [
                        'headers' => [
                            'X-Requested-With' => 'XMLHttpRequest',
                            'Accept' => 'application/json',
                        ],
                    ]
                );
            } catch (\Exception $exception) {
                // maybe widget not exist
            }

            try {
                $result = json_decode(
                    $this->httpClient->delete(
                        $this->httpClient->getUrl(
                            aExtEntryData('link_integration') . '/' . $integration,
                            $this->client->getDomain()
                        ),
                        [],
                        [
                            'headers' => [
                                'X-Requested-With' => 'XMLHttpRequest',
                                'Accept' => 'application/json',
                            ],
                        ]
                    ),
                    true
                );

                return isset($result['deleted']) && $result['deleted'];
            } catch (\Exception $exception) {
                return false;
            }
        }

        return false;
    }

    /**
     * @deprecated
     * @param Integration|string $integration
     * @return array
     * @throws AnemoneException
     */
    public function generateIntegrationData($integration)
    {
        if ($integration instanceof Integration) {
            $integration = $integration->uuid;
        }

        if (empty($integration)) {
            throw new AnemoneException('Before run store this integration');
        }

        try {
            return json_decode(
                $this->httpClient->post(
                    $this->httpClient->getUrl(
                        str_replace('{uuid}', $integration, aExtEntryData('link_generate_widget_data')),
                        $this->client->getDomain()
                    ),
                    [],
                    [
                        'headers' => [
                            'X-Requested-With' => 'XMLHttpRequest',
                            'Accept' => 'application/json',
                        ],
                    ]
                ),
                true
            );
        } catch (\Exception $exception) {
            throw new AnemoneException($exception->getMessage());
        }
    }

    /**
     * @param Integration $integration
     * @return Integration
     * @throws AnemoneException
     */
    public function refreshSecret(Integration $integration)
    {
        if (empty($integration->uuid)) {
            throw new AnemoneException('Before run refreshSecret store this integration');
        }

        try {
            $result = json_decode(
                $this->httpClient->patch(
                    $this->httpClient->getUrl(
                        str_replace('{uuid}', $integration->uuid, aExtEntryData('link_integration_refresh_secret')),
                        $this->client->getDomain()
                    ),
                    [],
                    [
                        'headers' => [
                            'X-Requested-With' => 'XMLHttpRequest',
                            'Accept' => 'application/json',
                        ],
                    ]
                ),
                true
            );

            if (!empty($result)) {
                $integration->secret = $result['secret'];

                return $integration;
            }

            throw new AnemoneException('Secret not updated');
        } catch (\Exception $exception) {
            throw new AnemoneException($exception->getMessage());
        }
    }

    /**
     * @deprecated See: self::uploadNew method
     * @param string $widget
     * @param string $code
     * @param string $secret_key
     * @param string $client_uuid
     * @return bool
     * @throws \Anemone\Exceptions\InvalidDataException
     * @throws \Anemone\Exceptions\BadResponseException
     */
    public function upload(string $widget, string $code, string $secret_key, string $client_uuid)
    {
        $model = new ApiWidget(
            ['code' => $code, 'secret_key' => $secret_key, 'client_uuid' => $client_uuid],
            $this->httpClient
        );

        return $model->upload($widget, true);
    }

    /**
     * @param string $widget
     * @param string $client_uuid
     * @return mixed
     * @throws \Anemone\Exceptions\InvalidDataException
     */
    public function uploadNew(string $widget, string $client_uuid)
    {
        return ApiWidget::uploadNew($widget, $client_uuid, $this->client);
    }

    /**
     * @param string $logo
     * @param Integration|string $integration
     * @return array
     * @throws AnemoneException
     */
    public function uploadLogo(string $logo, $integration)
    {
        if ($integration instanceof Integration) {
            $integration = $integration->uuid;
        }

        if (empty($integration)) {
            throw new AnemoneException('Before upload logo store this integration');
        }

        if (!file_exists($logo) || !is_file($logo)) {
            throw new AnemoneException('File [' . $logo . '] not found or is not type of file');
        }

        try {
            $file = Str::afterLast($logo, '/');

            return json_decode(
                $this->httpClient->post(
                    $this->httpClient->getUrl(
                        str_replace(
                            '{link_integration}',
                            aExtEntryData('link_integration') . '/' . $integration,
                            aExtEntryData('link_upload_logo')
                        ),
                        $this->client->getDomain()
                    ),
                    [],
                    [
                        'multipart' => [
                            [
                                'name' => 'logo',
                                'contents' => file_get_contents($logo),
                                'filename' => $file,
                                'Content-Type' => mime_content_type($logo),
                            ],
                            [
                                'name' => '_logo',
                                'contents' => $file,
                            ],
                        ],
                    ]
                ),
                true
            );
        } catch (\Exception $exception) {
            throw new AnemoneException($exception->getMessage());
        }
    }

    /** pack widget (integration) in zip and return path to widget
     * @param string $dir
     * @param string $widgetName
     * @return string
     * @throws \Anemone\Exceptions\InvalidDataException
     * */
    public function packIntegrationWidget(string $dir, $widgetName = 'widget.zip')
    {
        return ApiWidget::pack($dir, null, null, $widgetName, true);
    }

    /** settings for widget in integration
     * @param Integration $integration
     * @param array $settings
     * @param bool $power
     * @param bool $own
     * @return bool
     *
     * @throws AnemoneException
     * @throws BadResponseException
     */
    public function setSettings(Integration $integration, array $settings, bool $power = null, bool $own = false)
    {
        if (!isset($integration->uuid)) {
            throw new AnemoneException('Before run - integration should be created');
        }

        $one = $own
            ? ApiWidget::findOwnIntegration($this->httpClient, $integration->uuid)
            : ApiWidget::findInArea($this->httpClient, $integration->uuid);
        if (empty($one)) {
            throw new AnemoneException('Integration for [' . $integration->uuid . '] not found');
        }

        $widget = new ApiWidget($one, $this->httpClient);

        return $widget->setSettings($settings, $power);
    }

    /**
     * @param Integration $integration
     * @return array
     * @throws AnemoneException
     */
    public function convertCodeToAccessToken(Integration $integration)
    {
        if (!isset($integration->uuid)) {
            throw new AnemoneException('Before run - integration should be created');
        }

        try {
            $result = json_decode(
                $this->httpClient->post(
                    $this->httpClient->getUrl(aExtEntryData('link_oauth'), null, true),
                    [],
                    [
                        'headers' => [
                            'Content-Type' => 'application/json',
                        ],
                        'json' => [
                            'client_id' => $integration->uuid,
                            'client_secret' => $integration->secret,
                            'grant_type' => 'authorization_code',
                            'code' => $integration->auth_code,
                            'redirect_uri' => $integration->_links['redirect_uri']['href'] ?? '',
                        ]
                    ],
                    false
                ),
                true
            );

            if (isset($result['access_token'], $result['refresh_token'], $result['token_type'])) {
                return $result;
            }

            return [];
        } catch (\Exception $exception) {
            throw new BadResponseException('Invalid Auth result. ' . $exception->getMessage(), $exception->getCode());
        }
    }

    /** refresh token
     * @param Integration $integration
     * @param string $refresh_token
     * @return array
     * @throws AnemoneException
     */
    public function refreshToken(Integration $integration, string $refresh_token)
    {
        try {
            $result = json_decode(
                $this->httpClient->post(
                    $this->httpClient->getUrl(aExtEntryData('link_oauth'), null, true),
                    [],
                    [
                        'headers' => [
                            'Content-Type' => 'application/json',
                        ],
                        'json' => [
                            'client_id' => $integration->uuid,
                            'client_secret' => $integration->secret,
                            'grant_type' => 'refresh_token',
                            'refresh_token' => $refresh_token,
                            'redirect_uri' => $integration->_links['redirect_uri']['href'] ?? '',
                        ],
                    ],
                    false
                ),
                true
            );

            if (isset($result['access_token'], $result['refresh_token'], $result['token_type'])) {
                return $result;
            }

            return [];
        } catch (\Exception $exception) {
            throw new BadResponseException('Invalid Auth result. ' . $exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * [id => int, subdomain => string, domain => string, top_level_domain => string]
     * @return array|null
     */
    public function domain()
    {
        try {
            if (!$this->client instanceof Client) {
                throw new \Exception('Only ' . Client::class . ' client');
            }

            $link = $this->httpClient->getUrl(aExtEntryData('subdomain'), null, true);
            $link = preg_replace('/^(https?:\/\/)\w+\./', '$1www.', $link);

            return json_decode($this->httpClient->get($link), true);
        } catch (\Exception $exception) {
            return null;
        }
    }
}
