<?php
/**
 *
 * Helper functions
 * Is system for library Anemone
 *
 * Not move this file
 */

/**
 * encapsulation global classes
 * */

namespace AnemoneClasses {

    use Anemone\Exceptions\AnemoneException;
    use Dotenv\Dotenv;
    use Monolog\Handler\RotatingFileHandler;
    use Monolog\Handler\StreamHandler;
    use Monolog\Logger;
    use Monolog\Processor\ProcessorInterface;
    use Monolog\Processor\UidProcessor;


    class GrayProcessor implements ProcessorInterface
    {
        protected $logger;

        public function __construct()
        {
            $logConfig = aConfig('logging');

            $transport = new \Gelf\Transport\UdpTransport(
                $logConfig['grayHost'],
                $logConfig['grayPort'],
                \Gelf\Transport\UdpTransport::CHUNK_SIZE_LAN
            );

            $publisher = new \Gelf\Publisher();
            $publisher->addTransport($transport);

            $this->logger = new \Gelf\Logger(
                $publisher,
                empty($_ENV['APP_ENV']) ? 'production' : $_ENV['APP_ENV'],
                ['box' => empty($_ENV['APP_NAME']) ? 'undefined' : $_ENV['APP_NAME']]
            );
        }

        public function __invoke(array $record): array
        {
            $this->logger->log($record['level_name'], $record['message'], $record['context']);
            return $record;
        }

    }

    /**
     * @method static array apiEntries()
     * @method static array extensionsEntries()
     * @method static array availableVersion()
     * @method static Logger|null logger()
     * */
    final class AHelper
    {
        /**
         * @var AHelper $_context
         * */
        private static $_context;
        /**
         * @var array $_properties
         * */
        private static $_properties = [
            '_logger' => null,
            '_extensionsEntries' => null,
            '_apiEntries' => [],
            '_availableVersion' => ['_', '4'],
        ];

        private function __construct()
        {
            self::$_context = $this;
        }

        /** load singleton
         * @return void
         * @throws \Anemone\Exceptions\AnemoneException
         * */
        public static function init()
        {
            if (isset(self::$_context)) {
                throw new AnemoneException('AHelper support only be singleton');
            }

            new static();

            self::$_context->load();
            self::$_context->initLogger();
        }

        /** loading configs
         * @return void
         * */
        protected function load()
        {
            if (file_exists(aAppPath() . DIRECTORY_SEPARATOR . '.env')) {
                $dotenv = Dotenv::createImmutable(aAppPath());
                $dotenv->load();
            }

            self::$_properties['_extensionsEntries'] = require aLibPath()
                . DIRECTORY_SEPARATOR . 'configs'
                . DIRECTORY_SEPARATOR . 'extensions'
                . DIRECTORY_SEPARATOR . 'entries.php';

            foreach (self::$_properties['_availableVersion'] as $value) {
                self::$_properties['_apiEntries'][$value] =
                    require aLibPath()
                        . DIRECTORY_SEPARATOR . 'configs'
                        . DIRECTORY_SEPARATOR . 'entries'
                        . DIRECTORY_SEPARATOR . 'entries.v' . $value . '.php';
            }
        }

        /** init logger
         * @return void
         * */
        protected function initLogger()
        {
            $logConfig = aConfig('logging');
            // if logging is ON
            if ($logConfig['power']) {
                if ($logConfig['logger'] == 'own') {
                    self::$_properties['_logger'] = new Logger('anemone');
                    self::$_properties['_logger']->useMicrosecondTimestamps(false);
                    $handler = $logConfig['driver'] == 'daily'
                        ? new RotatingFileHandler(
                            $logConfig['path'], (int)$logConfig['days'], Logger::DEBUG, true, 0664
                        )
                        : new StreamHandler($logConfig['path'], Logger::DEBUG, true, 0664);
                    self::$_properties['_logger']->pushHandler($handler);
                    self::$_properties['_logger']->pushProcessor(new UidProcessor());
                    // set gelf
                    if ($logConfig['gelf']) {
                        self::$_properties['_logger']->pushProcessor(new GrayProcessor());
                    }
                }
            }
        }

        /**
         * @param string $name
         * @param array $arguments
         * @return mixed
         * */
        public static function __callStatic($name, $arguments)
        {
            $method = '_' . $name;
            if (isset(self::$_properties[$method])) {
                return self::$_properties[$method];
            }

            return null;
        }
    }
}

// HELPER GLOBAL FUNCTION

namespace {

    use AnemoneClasses\AHelper;
    use Symfony\Component\Translation\Exception\NotFoundResourceException;

    /** app path
     * @return string
     * */
    function aAppPath()
    {
        $listPath = explode('/vendor', __DIR__);
        if (count($listPath) > 1) {
            array_pop($listPath);
            return join('/vendor', $listPath);
        }

        return array_shift($listPath);
    }

    /** anemone lib path
     * @return string
     * */
    function aLibPath()
    {
        return explode('/helpers', __DIR__)[0];
    }

    /** get val in global config (.dot is separate)
     * @param string $query
     * @return mixed
     * */
    function aConfig(string $query = '')
    {
        $query = trim($query);
        $config = require aLibPath() . DIRECTORY_SEPARATOR . 'configs' . DIRECTORY_SEPARATOR . 'global.php';
        if (empty($query)) {
            return $config;
        }

        $pathList = explode('.', $query);

        return array_reduce(
            $pathList,
            function ($result, $item) {
                return is_null($result) ? null : ($result[$item] ?? null);
            },
            $config
        );
    }

    /** get entry data
     * @param string $query (ex.: '2_5.lead') available - '2', '2_5', '3', '_'
     * @return mixed
     * @throws \Symfony\Component\Translation\Exception\NotFoundResourceException
     * */
    function aEntryData(string $query)
    {
        $query = trim($query);
        if (empty($query)) {
            throw new NotFoundResourceException('$query string is required');
        }

        $pathList = explode('.', $query);
        if (!in_array(head($pathList), aAvailableVersion())) {
            throw new NotFoundResourceException('Version API [' . head($pathList) . '] not available');
        }

        return array_reduce(
            $pathList,
            function ($result, $item) {
                return is_null($result) ? null : ($result[$item] ?? null);
            },
            AHelper::apiEntries()
        );
    }

    /** get extensions entry data
     * @param string $query
     * @return mixed
     * */
    function aExtEntryData(string $query)
    {
        $query = trim($query);
        if (empty($query)) {
            return AHelper::extensionsEntries();
        }

        $pathList = explode('.', $query);

        return array_reduce(
            $pathList,
            function ($result, $item) {
                return is_null($result) ? null : ($result[$item] ?? null);
            },
            AHelper::extensionsEntries()
        );
    }

    /** get env variables
     * @param string $key
     * @param mixed $default
     * @return mixed
     * */
    function aEnv(string $key, $default = '')
    {
        if (function_exists('env')) { // for laravel env fun helper
            $value = env($key, $default);
        } else {
            $value = $_ENV[$key] ?? '';
            $value = $value == '' ? $default : $value;
            $value = $value == 'true' ? true : ($value == 'false' ? false : $value);
        }

        return $value;
    }

    /** get available versions
     * @return array
     * */
    function aAvailableVersion()
    {
        return AHelper::availableVersion();
    }

    /** two ways for write LOG
     * 1. with Closure (ex.: aLogger(function (\Monolog\Logger $logger) { $logger->error('message'); });)
     * 2. $logger instance (ex.: aLogger()->error('message');) // aLogger() is maybe NULL
     * @param Closure $closure
     * @return \Psr\Log\LoggerInterface|null
     * */
    function aLogger(Closure $closure = null)
    {
        switch (aConfig('logging.logger')) {
            case 'own':
                $logger = AHelper::logger();
                break;
            case 'laravel':
                /**
                 * @var mixed $fn
                 * */
                $fn = 'logger';
                $logger = function_exists($fn) && aConfig('logging.power') ? $fn() : null;
                break;
            default:
                $logger = null;
        }

        if (isset($closure, $logger) && $closure instanceof Closure) {
            $closure($logger);
        }

        return $logger;
    }

    AHelper::init();
}
