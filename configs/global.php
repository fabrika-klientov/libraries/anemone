<?php
/**
 * Config for library
 * Detail in README.md
 *
 * @package   Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.21
 * @link      https://fabrika-klientov.ua
 */

return [
    'general' => [
        'user_update' => aEnv('A_GENERAL_USER_UPDATE', -1)
    ],
    'logging' => [
        'power' => aEnv('A_LOG_POWER', true),
        'logger' => aEnv('A_LOG_LOGGER', 'own'), // own | laravel
        'driver' => aEnv('A_LOG_DRIVER', 'daily'), // daily | stack
        'path' => aAppPath() . DIRECTORY_SEPARATOR . aEnv('A_LOG_PATH', 'storage/logs/amo') . '/anemone.log',
        'days' => aEnv('A_LOG_DAYS', 7),
        'crop' => aEnv('A_LOG_CROP', true),
        'gelf' => aEnv('A_LOG_GELF', false),
        'grayHost' => aEnv('A_LOG_GRAY_HOST', '127.0.0.1'),
        'grayPort' => aEnv('A_LOG_GRAY_PORT', 12201),
    ],
    'cookies' => [
        'path' => aAppPath() . DIRECTORY_SEPARATOR . aEnv('A_COOKIES_PATH', 'storage/cookies/amo'),
    ],
    'cache' => [
        'power' => aEnv('A_CACHE_POWER', true),
        'path' => aAppPath() . DIRECTORY_SEPARATOR . aEnv('A_CACHE_PATH', 'storage/cache/amo'),
        'timings' => [
            'other' => aEnv('A_CACHE_T_OTHER', 300),
            'account' => aEnv('A_CACHE_T_ACCOUNT', 600),
            'lead' => aEnv('A_CACHE_T_LEAD', 60),
            'customer' => aEnv('A_CACHE_T_CUSTOMER', 60),
            'contact' => aEnv('A_CACHE_T_CONTACT', 60),
            'company' => aEnv('A_CACHE_T_COMPANY', 60),
            'task' => aEnv('A_CACHE_T_TASK', 60),
            'note' => aEnv('A_CACHE_T_NOTE', 60),
            'incomingLead' => aEnv('A_CACHE_T_INCOMING_LEAD', 60),
            'call' => aEnv('A_CACHE_T_CALL', 60),
            'event' => aEnv('A_CACHE_T_EVENT', 60),
            'webhook' => aEnv('A_CACHE_T_EVENT', 60),
            'user' => aEnv('A_CACHE_T_USER', 600),
            'catalog' => aEnv('A_CACHE_T_CATALOG', 600),
            'element' => aEnv('A_CACHE_T_ELEMENT', 300),
            'tag' => aEnv('A_CACHE_T_TAG', 300),

            // custom fields
            'cf_lead' => aEnv('A_CACHE_T_CF_LEAD', 600),
            'cf_customer' => aEnv('A_CACHE_T_CF_CUSTOMER', 600),
            'cf_contact' => aEnv('A_CACHE_T_CF_CONTACT', 600),
            'cf_company' => aEnv('A_CACHE_T_CF_COMPANY', 600),
            'cf_element' => aEnv('A_CACHE_T_CF_ELEMENT', 600),
            //
        ],
    ],
    'versions' => [
        'default' => [
            'account' => aEnv('A_VERSION_ACCOUNT', '4'),
            'lead' => aEnv('A_VERSION_LEAD', '4'),
            'customer' => aEnv('A_VERSION_CUSTOMER', '4'),
            'contact' => aEnv('A_VERSION_CONTACT', '4'),
            'company' => aEnv('A_VERSION_COMPANY', '4'),
            'task' => aEnv('A_VERSION_TASK', '4'),
            'note' => aEnv('A_VERSION_NOTE', '4'),
            'incomingLead' => aEnv('A_VERSION_INCOMING_LEAD', '4'),
            'call' => '4',
            'event' => '4',
            'webhook' => '4',
            //
        ],
    ],
    'auth' => [
        'connector' => aEnv('A_AUTH_CONNECTOR', \Anemone\Core\Auth\Connectors\JsonConnector::class), // push class of Anemone\Contracts\BeAuthConnector
        'path' => aAppPath() . DIRECTORY_SEPARATOR . aEnv('A_AUTH_PATH', 'storage/oauth/amo'),
    ],
];
