<?php
/**
 * Config entries for AMO CRM
 *
 * @package   Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.21
 * @link      https://fabrika-klientov.ua
 * @deprecated
 */

return [
    'auth' => [
        'path' => '/private/api/auth.php',
    ],
    'account' => [
        'path' => '/api/v2/account',
        '_select' => 'GET',
    ],
    'lead' => [
        'get' => '/api/v2/leads',
        'add' => '/api/v2/leads',
        'update' => '/api/v2/leads',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
    ],
    'contact' => [
        'get' => '/api/v2/contacts',
        'add' => '/api/v2/contacts',
        'update' => '/api/v2/contacts',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
    ],
    'company' => [
        'get' => '/api/v2/companies',
        'add' => '/api/v2/companies',
        'update' => '/api/v2/companies',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
    ],
    'customer' => [
        'get' => '/api/v2/customers',
        'add' => '/api/v2/customers',
        'update' => '/api/v2/customers',
        'delete' => '/api/v2/customers',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
        '_destroy' => 'POST',
    ],
    'task' => [
        'get' => '/api/v2/tasks',
        'add' => '/api/v2/tasks',
        'update' => '/api/v2/tasks',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
    ],
    'note' => [
        'get' => '/api/v2/notes',
        'add' => '/api/v2/notes',
        'update' => '/api/v2/notes',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
    ],
    'incomingLead' => [
        'add_sip' => '/api/v2/incoming_leads/sip',
        'add_form' => '/api/v2/incoming_leads/form',
        'accept' => '/api/v2/incoming_leads/accept',
        'decline' => '/api/v2/incoming_leads/decline',
        'get' => '/api/v2/incoming_leads',
        'get_summary' => '/api/v2/incoming_leads/summary',
        '_select' => 'GET',
        '_select_summary' => 'GET',
        '_insert_sip' => 'POST',
        '_insert_form' => 'POST',
        '_accept' => 'POST',
        '_decline' => 'POST',
    ],
    'field' => [
        'add' => '/api/v2/fields',
        'delete' => '/api/v2/fields',
        '_insert' => 'POST',
        '_destroy' => 'POST',
    ],
    'call' => [
        'add' => '/api/v2/calls',
        '_insert' => 'POST',
    ],
    'event' => [
        'get' => '/api/v2/events',
        'add' => '/api/v2/events',
        '_select' => 'GET',
        '_insert' => 'POST',
    ],
    'pipeline' => [ // in future
        'get' => '/api/v2/pipelines',
        'add' => '/private/api/v2/json/pipelines/set',
        'update' => '/private/api/v2/json/pipelines/set',
        'delete' => '/private/api/v2/json/pipelines/delete',
    ],
    'webhook' => [
        'get' => '/api/v2/webhooks',
        'add' => '/api/v2/webhooks/subscribe',
        'delete' => '/api/v2/webhooks/unsubscribe',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_destroy' => 'POST',
    ],
];
