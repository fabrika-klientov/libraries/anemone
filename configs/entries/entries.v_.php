<?php
/**
 * Config entries for AMO CRM
 *
 * @package   Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.21
 * @link      https://fabrika-klientov.ua
 * @deprecated
 */

return [ // tmp
    'auth' => [
        'path' => '/private/api/auth.php', // ??
    ],
    'account' => [
        'path' => '/api/v2/account', // ??
    ],
    'lead' => [
        'get' => '/ajax/leads/list',
        'add' => '/ajax/leads/detail',
        'update' => '/ajax/leads/detail',
        'delete' => '/ajax/leads/multiple/delete',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
        '_destroy' => 'POST',
    ],
    'contact' => [
        'get' => '/ajax/contacts/list', // element_type	1
        'add' => '/ajax/contacts/detail',
        'update' => '/ajax/contacts/detail',
        'delete' => '/ajax/contacts/multiple/delete',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
        '_destroy' => 'POST',
    ],
    'company' => [
        'get' => '/ajax/contacts/list', // element_type	3
        'add' => '/ajax/companies/detail',
        'update' => '/ajax/companies/detail',
        'delete' => '/ajax/companies/multiple/delete',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
        '_destroy' => 'POST',
    ],
    'customer' => [
        'get' => '/ajax/v2/customers/list',
        'add' => '/ajax/v1/customers/set',
        'update' => '/ajax/v1/customers/set',
        'delete' => '/ajax/v1/customers/set',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
        '_destroy' => 'POST',
    ],
    'task' => [
        'get' => '/ajax/todo/list',
        'add' => '/private/notes/edit2.php',
        'update' => '/private/notes/edit2.php',
        'delete' => '/ajax/v1/multiactions/set',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
        '_destroy' => 'POST',
    ],
    'note' => [ // (not supported)
        'path' => '/api/v2/notes',
    ],
    'incomingLead' => [ // (not supported)
        'add_sip' => '/api/v2/incoming_leads/sip',
        'add_form' => '/api/v2/incoming_leads/form',
        'accept' => '/api/v2/incoming_leads/accept',
        'decline' => '/api/v2/incoming_leads/decline',
        'get' => '/api/v2/incoming_leads',
        'get_summary' => '/api/v2/incoming_leads/summary',
    ],
    'field' => [
        'get' => '/ajax/settings/custom_fields',
        'add' => '/ajax/settings/custom_fields',
        'update' => '/ajax/settings/custom_fields',
        'delete' => '/ajax/settings/custom_fields',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
        '_destroy' => 'POST',
    ],
    'pipeline' => [ // in future
        'get' => '/api/v2/pipelines',
        'add' => '/private/api/v2/json/pipelines/set',
        'update' => '/private/api/v2/json/pipelines/set',
        'delete' => '/private/api/v2/json/pipelines/delete',
    ],
    'webhook' => [ // in future
        'get' => '/api/v2/webhooks',
        'add' => '/api/v2/webhooks/subscribe',
        'delete' => '/api/v2/webhooks/unsubscribe',
    ],
];
