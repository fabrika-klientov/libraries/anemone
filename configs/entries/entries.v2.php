<?php
/**
 * Config entries for AMO CRM
 *
 * @package   Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.21
 * @link      https://fabrika-klientov.ua
 * @deprecated
 */

return [
    'auth' => [
        'path' => '/private/api/auth.php',
    ],
    'account' => [
        'path' => '/private/api/v2/json/accounts/current',
        '_select' => 'GET',
    ],
    'lead' => [
        'get' => '/private/api/v2/json/leads/list',
        'add' => '/private/api/v2/json/leads/set',
        'update' => '/private/api/v2/json/leads/set',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
    ],
    'contact' => [
        'get' => '/private/api/v2/json/contacts/list',
        'add' => '/private/api/v2/json/contacts/set',
        'update' => '/private/api/v2/json/contacts/set',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
    ],
    'company' => [
        'get' => '/private/api/v2/json/company/list',
        'add' => '/private/api/v2/json/company/set',
        'update' => '/private/api/v2/json/company/set',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
    ],
    'customer' => [
        'get' => '/private/api/v2/json/customers/list',
        'add' => '/private/api/v2/json/customers/set',
        'update' => '/private/api/v2/json/customers/set',
        'delete' => '/private/api/v2/json/customers/set',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
        '_destroy' => 'POST',
    ],
    'task' => [
        'get' => '/private/api/v2/json/tasks/list',
        'add' => '/private/api/v2/json/tasks/set',
        'update' => '/private/api/v2/json/tasks/set',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
    ],
    'note' => [
        'get' => '/private/api/v2/json/notes/list',
        'add' => '/private/api/v2/json/notes/set',
        'update' => '/private/api/v2/json/notes/set',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'POST',
    ],
    'incomingLead' => [ // not documents
        'add_sip' => '/api/v2/incoming_leads/sip',
        'add_form' => '/api/v2/incoming_leads/form',
        'accept' => '/api/v2/incoming_leads/accept',
        'decline' => '/api/v2/incoming_leads/decline',
        'get' => '/api/v2/incoming_leads',
        'get_summary' => '/api/v2/incoming_leads/summary',
    ],
    'field' => [
        'add' => '/private/api/v2/json/fields/set',
        'delete' => '/private/api/v2/json/fields/set',
        '_insert' => 'POST',
        '_destroy' => 'POST',
    ],
    'pipeline' => [ // in future
        'get' => '/api/v2/pipelines',
        'add' => '/private/api/v2/json/pipelines/set',
        'update' => '/private/api/v2/json/pipelines/set',
        'delete' => '/private/api/v2/json/pipelines/delete',
    ],
    'webhook' => [ // in future
        'get' => '/api/v2/webhooks',
        'add' => '/api/v2/webhooks/subscribe',
        'delete' => '/api/v2/webhooks/unsubscribe',
    ],
];
