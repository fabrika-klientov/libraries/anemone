<?php
/**
 * Config entries for AMO CRM
 *
 * @package   Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.21
 * @link      https://fabrika-klientov.ua
 * @deprecated
 */

return [ // tmp
    'auth' => [
        'path' => '/private/api/auth.php', // not found auth in v3
    ],
    'account' => [
        'path' => '/api/v2/account', // not found account in v3
        '_select' => 'GET',
    ],
    'lead' => [
        'get' => '/v3/leads',
        '_select' => 'GET',
    ],
    'contact' => [
        'get' => '/v3/contacts',
        '_select' => 'GET',
    ],
    'company' => [
        'get' => '/v3/companies',
        '_select' => 'GET',
    ],
    'customer' => [
        'get' => '/v3/customers',
        '_select' => 'GET',
    ],
    'task' => [ // (not supported)
        'path' => '/api/v2/tasks',
    ],
    'note' => [ // (not supported)
        'path' => '/api/v2/notes',
    ],
    'incomingLead' => [ // (not supported)
        'add_sip' => '/api/v2/incoming_leads/sip',
        'add_form' => '/api/v2/incoming_leads/form',
        'accept' => '/api/v2/incoming_leads/accept',
        'decline' => '/api/v2/incoming_leads/decline',
        'get' => '/api/v2/incoming_leads',
        'get_summary' => '/api/v2/incoming_leads/summary',
    ],
    'field' => [ // in future
        'path' => '/api/v2/fields',
    ],
    'pipeline' => [ // in future
        'get' => '/api/v2/pipelines',
        'add' => '/private/api/v2/json/pipelines/set',
        'update' => '/private/api/v2/json/pipelines/set',
        'delete' => '/private/api/v2/json/pipelines/delete',
    ],
    'webhook' => [ // in future
        'get' => '/api/v2/webhooks',
        'add' => '/api/v2/webhooks/subscribe',
        'delete' => '/api/v2/webhooks/unsubscribe',
    ],
    'tag' => [
        'get' => '/v3/{entity}/tags',
        '_select' => 'GET',
    ],
];
