<?php
/**
 * Config entries for AMO CRM
 *
 * @package   Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.21
 * @link      https://fabrika-klientov.ua
 */

return [
    'auth' => [
        'path' => '/private/api/auth.php',
    ],
    'account' => [
        'path' => '/api/v4/account',
        '_select' => 'GET',
    ],
    'user' => [
        'get' => '/api/v4/users',
        'add' => '/api/v4/users',
        '_select' => 'GET',
        '_insert' => 'POST',
    ],
    'lead' => [
        'get' => '/api/v4/leads',
        'add' => '/api/v4/leads',
        'update' => '/api/v4/leads',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'PATCH',
    ],
    'contact' => [
        'get' => '/api/v4/contacts',
        'add' => '/api/v4/contacts',
        'update' => '/api/v4/contacts',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'PATCH',
    ],
    'company' => [
        'get' => '/api/v4/companies',
        'add' => '/api/v4/companies',
        'update' => '/api/v4/companies',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'PATCH',
    ],
    'customer' => [
        'get' => '/api/v4/customers',
        'add' => '/api/v4/customers',
        'update' => '/api/v4/customers',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'PATCH',
    ],
    'task' => [
        'get' => '/api/v4/tasks',
        'add' => '/api/v4/tasks',
        'update' => '/api/v4/tasks',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'PATCH',
    ],
    'note' => [
        'get' => '/api/v4/{entity_type}/notes',
        'add' => '/api/v4/{entity_type}/notes',
        'update' => '/api/v4/{entity_type}/notes',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'PATCH',
    ],
    'tag' => [
        'get' => '/api/v4/{entity_type}/tags',
        'add' => '/api/v4/{entity_type}/tags',
        '_select' => 'GET',
        '_insert' => 'POST',
    ],
    'incomingLead' => [
        'add_sip' => '/api/v4/leads/unsorted/sip',
        'add_form' => '/api/v4/leads/unsorted/forms',
        'accept' => '/api/v4/leads/unsorted/{uid}/accept',
        'decline' => '/api/v4/leads/unsorted/{uid}/decline',
        'get' => '/api/v4/leads/unsorted',
        'get_summary' => '/api/v4/leads/unsorted/summary',
        '_select' => 'GET',
        '_select_summary' => 'GET',
        '_insert_sip' => 'POST',
        '_insert_form' => 'POST',
        '_accept' => 'POST',
        '_decline' => 'DELETE',
    ],
    'field' => [
        'get' => '/api/v4/{entity_type}{entity_id}/custom_fields',
        'add' => '/api/v4/{entity_type}{entity_id}/custom_fields',
        'update' => '/api/v4/{entity_type}{entity_id}/custom_fields',
        'delete' => '/api/v4/{entity_type}{entity_id}/custom_fields',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'PATCH',
        '_destroy' => 'DELETE',
    ],
    'call' => [
        'add' => '/api/v4/calls',
        '_insert' => 'POST',
    ],
    'event' => [
        'get' => '/api/v4/events',
        'get_types' => '/api/v4/events/types',
        '_select' => 'GET',
        '_select_types' => 'GET',
    ],
    'pipeline' => [
        'get' => '/api/v4/leads/pipelines',
        'add' => '/api/v4/leads/pipelines',
        'update' => '/api/v4/leads/pipelines',
        'delete' => '/api/v4/leads/pipelines',
    ],
    'customer_statuses' => [ // in future
        'get' => '/api/v4/customers/statuses',
        'add' => '/api/v4/customers/statuses',
        'update' => '/api/v4/customers/statuses',
        'delete' => '/api/v4/customers/statuses',
    ],
    'webhook' => [
        'get' => '/api/v4/webhooks',
        'add' => '/api/v4/webhooks',
        'delete' => '/api/v4/webhooks',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_destroy' => 'DELETE',
    ],
    'catalog' => [
        'get' => '/api/v4/catalogs',
        'add' => '/api/v4/catalogs',
        'update' => '/api/v4/catalogs',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'PATCH',
    ],
    'element' => [
        'get' => '/api/v4/catalogs/{catalog_id}/elements',
        'add' => '/api/v4/catalogs/{catalog_id}/elements',
        'update' => '/api/v4/catalogs/{catalog_id}/elements',
        '_select' => 'GET',
        '_insert' => 'POST',
        '_update' => 'PATCH',
    ],
];
