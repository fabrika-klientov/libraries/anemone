<?php
/**
 *
 * @package   Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.21
 * @link      https://fabrika-klientov.ua
 */

return [
    'host' => aEnv('A_EX_AMO_HOST', 'www.amocrm.ru'), // recommended

    // auth
    'link_auth' => '/oauth2/authorize',
    // oauth2
    'link_oauth' => '/oauth2/access_token',
    'subdomain' => '/oauth2/account/subdomain',

    // partners
    'link_page_account' => '/partners/private/new_account/',
    'link_store_account' => '/partners/private/partn_reg_account.php',
    'link_form_invoice' => '/partners/private/shop/bill',
    'link_request_invoice' => '/partners/private/shop/addbillpartners',
    'link_request_validate' => '/partners/private/ajax/bill/idaccvalidat/',
    'link_discount_invoice' => '/partners/private/ajax/bill/contractdisc/',
    'link_accounts' => '/partners/private/ajax/accounts',
    'link_prolong_trial' => '/partners/private/ajax/accounts/{id}/trial_extension/',

    // widgets
    'link_widget_list_own_integrations' => '/ajax/settings/widgets/category/own_integrations',

    'link_widget_list_recommended' => '/ajax/settings/widgets/category/recommended',

    'link_widget_list_phone' => '/ajax/settings/widgets/category/phone',
    'link_widget_list_mail' => '/ajax/settings/widgets/category/mail',
    'link_widget_list_site' => '/ajax/settings/widgets/category/site',
    'link_widget_list_chats' => '/ajax/settings/widgets/category/chats',
    'link_widget_list_useful' => '/ajax/settings/widgets/category/useful',

    'link_widget_list_all_api' => '/api/v2/widgets',

    'link_widget_list_api' => '/ajax/settings/dev',
    'link_widget_edit' => '/ajax/widgets/edit',
    'link_widget_destroy' => '/{sub}/delete/',
    'link_widget_upload' => '/{sub}/upload/',

    'widgets_domain' => 'widgets.amocrm.{zone}',

    // integration
    'link_widget_list_with_user_settings' => '/ajax/widgets/list',
    'link_integration' => '/v3/clients',
    'link_integration_refresh_secret' => '/v3/clients/{uuid}/secret/',
    'link_generate_widget_data' => '/ajax/widgets/{uuid}/widget/', // deprecated
    'link_upload_integration' => '/ajax/widgets/{uuid}/widget/upload/',
    'link_upload_logo' => '{link_integration}/files',
    'link_delete_widget_integration' => '/ajax/widgets/{uuid}/widget/delete',

    // profile
    'link_profile' => '/ajax/settings/profile',
    'link_profile_account' => '/v3/accounts',

    // notifications
    'notification_list' => '/v3/inbox/list',
    'notification_store' => '/ajax/v1/inbox/add/',

    // custom fields & groups
    'link_custom_fields' => '/ajax/settings/custom_fields/',
];
