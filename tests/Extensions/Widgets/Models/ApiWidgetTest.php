<?php
/**
 *
 * Date: 30.10.19
 * Time: 21:26
 */

namespace Tests\Extensions\Widgets\Models;

use Anemone\Client;
use Anemone\Extensions\NotApiClient;
use Anemone\Extensions\Widgets\Models\ApiWidget;
use PHPUnit\Framework\TestCase;

class ApiWidgetTest extends TestCase
{
    protected static $DOMAIN = 'jarvis.amocrm.ru';
    protected static $VALID_PARAM_PASS = [
        'user_login' => 'jarvis@google.com',
        'user_password' => 'password',
    ];
    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];

    public function test__construct()
    {
        $client = new Client(self::$VALID_PARAM);
        $unClient = new NotApiClient(self::$VALID_PARAM_PASS, self::$DOMAIN);
        new ApiWidget([], $client->getQueryService());
        new ApiWidget([], $unClient->getQueryService());
        $this->assertTrue(true);
    }

    public function testStore()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testDestroy()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testUpload()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testPower()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testSetSettings()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testAll()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testFind()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testPack()
    {
        $this->assertTrue(true); // expected widget
    }
}
