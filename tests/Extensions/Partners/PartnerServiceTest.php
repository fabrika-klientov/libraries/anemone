<?php
/**
 *
 * Date: 30.10.19
 * Time: 21:12
 */

namespace Tests\Extensions\Partners;

use Anemone\Extensions\Core\Services\AuthService;
use Anemone\Extensions\Core\Services\HttpClientService;
use Anemone\Extensions\Partners\PartnerService;
use PHPUnit\Framework\TestCase;

class PartnerServiceTest extends TestCase
{
    protected static $DOMAIN = 'jarvis.amocrm.ru';
    protected static $VALID_PARAM_PASS = [
        'user_login' => 'jarvis@google.com',
        'user_password' => 'password',
    ];

    public function test__construct()
    {
        $authService = new AuthService(self::$VALID_PARAM_PASS, self::$DOMAIN);
        $service = new HttpClientService($authService);
        new PartnerService($service);
        $this->assertTrue(true);
    }

    public function testPartnersList()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testPartnerById()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testTariffById()
    {
        $this->assertTrue(true); // expected http request
    }
}
