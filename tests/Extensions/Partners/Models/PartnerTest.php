<?php
/**
 *
 * Date: 30.10.19
 * Time: 21:15
 */

namespace Tests\Extensions\Partners\Models;

use Anemone\Exceptions\InvalidDataException;
use Anemone\Extensions\Core\Services\AuthService;
use Anemone\Extensions\Core\Services\HttpClientService;
use Anemone\Extensions\Partners\Models\Partner;
use PHPUnit\Framework\TestCase;

class PartnerTest extends TestCase
{
    protected static $DOMAIN = 'jarvis.amocrm.ru';
    protected static $VALID_PARAM_PASS = [
        'user_login' => 'jarvis@google.com',
        'user_password' => 'password',
    ];

    public function test__construct()
    {
        $authService = new AuthService(self::$VALID_PARAM_PASS, self::$DOMAIN);
        $service = new HttpClientService($authService);
        new Partner(['name' => 'Name test', 'email' => 'email@test'], $service);
        $this->assertTrue(true);
    }

    public function test__constructInvalidDataException1()
    {
        $this->expectException(InvalidDataException::class);
        $authService = new AuthService(self::$VALID_PARAM_PASS, self::$DOMAIN);
        $service = new HttpClientService($authService);
        new Partner(['email' => 'email@test'], $service);
        $this->fail('Expected InvalidDataException');
    }

    public function test__constructInvalidDataException2()
    {
        $this->expectException(InvalidDataException::class);
        $authService = new AuthService(self::$VALID_PARAM_PASS, self::$DOMAIN);
        $service = new HttpClientService($authService);
        new Partner(['name' => 'i name', 'phone' => 12345], $service);
        $this->fail('Expected InvalidDataException');
    }

    public function testStore()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testDetail()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testProlong()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testFind()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testAll()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testJsonSerialize()
    {
        $authService = new AuthService(self::$VALID_PARAM_PASS, self::$DOMAIN);
        $service = new HttpClientService($authService);
        $partner = new Partner(['name' => 'Name test', 'email' => 'email@test', 'phone' => 567890], $service);
        $this->assertIsArray($partner->jsonSerialize());
        $this->assertCount(3, $partner->jsonSerialize()['created_user']);
    }
}
