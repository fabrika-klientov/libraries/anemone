<?php
/**
 *
 * Date: 30.10.19
 * Time: 21:05
 */

namespace Tests\Extensions\Notifications;

use Anemone\Client;
use Anemone\Extensions\NotApiClient;
use Anemone\Extensions\Notifications\NotificationService;
use PHPUnit\Framework\TestCase;

class NotificationServiceTest extends TestCase
{
    protected static $DOMAIN = 'jarvis.amocrm.ru';
    protected static $VALID_PARAM_PASS = [
        'user_login' => 'jarvis@google.com',
        'user_password' => 'password',
    ];
    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];

    public function test__construct()
    {
        $client = new Client(self::$VALID_PARAM);
        $unClient = new NotApiClient(self::$VALID_PARAM_PASS, self::$DOMAIN);
        new NotificationService($client);
        new NotificationService($unClient);
        $this->assertTrue(true);
    }

    public function testGet()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testStore()
    {
        $this->assertTrue(true); // expected http request
    }
}
