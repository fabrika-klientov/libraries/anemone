<?php
/**
 *
 * Date: 30.10.19
 * Time: 20:26
 */

namespace Tests\Extensions;

use Anemone\Exceptions\InvalidDataException;
use Anemone\Extensions\Core\Services\HttpClientService;
use Anemone\Extensions\NotApiClient;
use Anemone\Extensions\Partners\PartnerService;
use Anemone\Extensions\Widgets\WidgetsService;
use PHPUnit\Framework\TestCase;

class NotApiClientTest extends TestCase
{
    protected static $DOMAIN = 'jarvis.amocrm.ru';
    protected static $VALID_PARAM_PASS = [
        'user_login' => 'jarvis@google.com',
        'user_password' => 'password',
    ];

    public function test__construct()
    {
        new NotApiClient(self::$VALID_PARAM_PASS, self::$DOMAIN);
        $this->assertTrue(true);
    }

    public function test__constructInvalidDataException1()
    {
        $this->expectException(InvalidDataException::class);
        new NotApiClient([], self::$DOMAIN);
        $this->fail('Expected InvalidDataException');
    }

    public function test__constructInvalidDataException2()
    {
        $this->expectException(InvalidDataException::class);
        $params = self::$VALID_PARAM_PASS;
        unset($params['user_login']);
        new NotApiClient($params, self::$DOMAIN);
        $this->fail('Expected InvalidDataException');
    }

    public function test__constructInvalidDataException3()
    {
        $this->expectException(InvalidDataException::class);
        $params = self::$VALID_PARAM_PASS;
        unset($params['user_password']);
        new NotApiClient($params, self::$DOMAIN);
        $this->fail('Expected InvalidDataException');
    }

    public function testGetQueryService()
    {
        $client = new NotApiClient(self::$VALID_PARAM_PASS, self::$DOMAIN);
        $this->assertInstanceOf(HttpClientService::class, $client->getQueryService());
    }

    public function testGetDomain()
    {
        $client = new NotApiClient(self::$VALID_PARAM_PASS, self::$DOMAIN);
        $this->assertEquals(self::$DOMAIN, $client->getDomain());
    }

    public function testPartnerService()
    {
        $client = new NotApiClient(self::$VALID_PARAM_PASS, self::$DOMAIN);
        $this->assertInstanceOf(PartnerService::class, $client->partnerService);
    }

    public function testWidgetsService()
    {
        $client = new NotApiClient(self::$VALID_PARAM_PASS, self::$DOMAIN);
        $this->assertInstanceOf(WidgetsService::class, $client->widgetsService);
    }
}
