<?php
/**
 *
 * Date: 30.10.19
 * Time: 20:53
 */

namespace Tests\Extensions\Core;

use Anemone\Extensions\Core\Services\AuthService;
use Anemone\Extensions\Core\Services\HttpClientService;
use PHPUnit\Framework\TestCase;

class AuthServiceTest extends TestCase
{
    protected static $DOMAIN = 'jarvis.amocrm.ru';
    protected static $VALID_PARAM_PASS = [
        'user_login' => 'jarvis@google.com',
        'user_password' => 'password',
    ];

    public function test__construct()
    {
        new AuthService(self::$VALID_PARAM_PASS, self::$DOMAIN);
        $this->assertTrue(true);
    }

    public function testInitHttpClientService()
    {
        $authService = new AuthService(self::$VALID_PARAM_PASS, self::$DOMAIN);
        $httpClientService = new HttpClientService($authService);
        $authService->initHttpClientService($httpClientService);
        $this->assertTrue(true);
    }

    public function testGetSessionData()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testAuth()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testGetDomain()
    {
        $authService = new AuthService(self::$VALID_PARAM_PASS, self::$DOMAIN);

        $this->assertEquals(self::$DOMAIN, $authService->getDomain());
    }
}
