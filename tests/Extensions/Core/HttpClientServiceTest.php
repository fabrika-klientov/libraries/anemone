<?php
/**
 *
 * Date: 30.10.19
 * Time: 20:58
 */

namespace Tests\Extensions\Core;

use Anemone\Extensions\Core\Services\AuthService;
use Anemone\Extensions\Core\Services\HttpClientService;
use PHPUnit\Framework\TestCase;

class HttpClientServiceTest extends TestCase
{
    protected static $DOMAIN = 'jarvis.amocrm.ru';
    protected static $VALID_PARAM_PASS = [
        'user_login' => 'jarvis@google.com',
        'user_password' => 'password',
    ];

    public function test__construct()
    {
        $authService = new AuthService(self::$VALID_PARAM_PASS, self::$DOMAIN);
        new HttpClientService($authService);
        $this->assertTrue(true);
    }

    public function testGet()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testPost()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testPatch()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testDelete()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testMerge()
    {
        $this->assertTrue(true); // expected http request
    }

    public function testGetUrl()
    {
        $authService = new AuthService(self::$VALID_PARAM_PASS, self::$DOMAIN);
        $service = new HttpClientService($authService);

        $this->assertEquals('https://www.amocrm.ru/leads', $service->getUrl('leads'));
        $this->assertEquals('https://google.com/leads', $service->getUrl('leads', 'google.com'));
        $this->assertEquals('https://google.com/leads', $service->getUrl('https://google.com/leads'));
        $this->assertEquals('https://' . self::$DOMAIN . '/leads', $service->getUrl('leads', null, true));
    }
}
