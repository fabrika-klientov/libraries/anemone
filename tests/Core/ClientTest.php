<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core;


use Anemone\Client;
use Anemone\Core\Auth\AuthService;
use Anemone\Core\Cache\CacheService;
use Anemone\Core\Query\QueryService;
use Anemone\Exceptions\InvalidDataException;
use Anemone\Exceptions\InvalidVersionException;
use Anemone\Models\Instances\ModelInstance;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    /**
     * @var Client $client
     * */
    protected $client;
    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];

    /**
     * @dataProvider additionProvider
     */
    public function testConstruct($a, $b)
    {
        $client = new Client(self::$VALID_PARAM);
        $this->assertTrue(isset($client));
    }

    /**
     * @dataProvider additionInvalidDataExceptionsProvider
     */
    public function testHasInvalidDataExceptionsConstruct($a, $b)
    {
//        $this->expectException(InvalidDataException::class);
        new Client(self::$VALID_PARAM);
        $this->assertTrue(true);
    }

    /**
     * @dataProvider additionInvalidVersionExceptionsProvider
     */
    public function testHasInvalidVersionExceptionsConstruct($a, $b)
    {
//        $this->expectException(InvalidVersionException::class);
        new Client(self::$VALID_PARAM);
        $this->assertTrue(true);
    }

    public function testAddInstance()
    {
        Client::addInstance($this->client);
        Client::addInstance(new Collection([$this->client]));
        $this->assertTrue(true);
    }

    public function testAddInstanceInvalidDataExceptionOne()
    {
        $this->expectException(InvalidDataException::class);
        Client::addInstance(null);
        $this->fail('Expected exception');
        $this->assertTrue(true);
    }

    public function testAddInstanceInvalidDataExceptionCollect()
    {
        $this->expectException(InvalidDataException::class);
        Client::addInstance(new Collection(['invalid']));
        $this->fail('Expected exception');
        $this->assertTrue(true);
    }

    public function testGetInstanceForDomain()
    {
        $instance = Client::getInstance(self::$VALID_PARAM['domain']);
        $this->assertInstanceOf(Client::class, $instance);
    }

    public function testGetInstanceForCollectInArray()
    {
        $instanceCollect = Client::getInstance([self::$VALID_PARAM['domain']]);
        $this->assertInstanceOf(Collection::class, $instanceCollect);
    }

    public function testGetInstanceForCollectInNull()
    {
        $instanceCollect = Client::getInstance();
        $this->assertInstanceOf(Collection::class, $instanceCollect);
    }

    public function testGetInstanceForDomainUndefined()
    {
        $instance = Client::getInstance('domain-not-found.amocrm.ru');
        $this->assertNull($instance);
    }

    public function testInitClients()
    {
        Client::initClients(new Collection([self::$VALID_PARAM]));
        $this->assertTrue(true);
    }

    public function testInitClientsHasInvalidDataException()
    {
        $this->expectException(InvalidDataException::class);
        $one = self::$VALID_PARAM;
        unset($one['login']);
        Client::initClients(new Collection([$one]));
        $this->fail('Expected InvalidDataException');
    }

    public function testGetDomain()
    {
        $this->assertEquals(self::$VALID_PARAM['domain'], $this->client->getDomain());
    }

    public function testGetAuthService()
    {
        $this->assertInstanceOf(AuthService::class, $this->client->getAuthService());
    }

    public function testGetQueryService()
    {
        $this->assertInstanceOf(QueryService::class, $this->client->getQueryService());
    }

    public function testGetCacheService()
    {
        $this->assertInstanceOf(CacheService::class, $this->client->getCacheService());
    }

    public function testInstances()
    {
        $data = ['account', 'leads', 'customers', 'contacts', 'companies', 'tasks'];
        foreach ($data as $datum) {
            $this->assertInstanceOf(ModelInstance::class, $this->client->{$datum});
        }
    }


    protected function setUp(): void
    {
        $this->client = new Client(self::$VALID_PARAM);
    }

    protected function tearDown(): void
    {
        $this->client = null;
    }

    public function additionProvider()
    {
        return [
            'valid_without_versions' => [
                self::$VALID_PARAM,
                null,
            ],
            'valid_with_versions' => [
                (object)self::$VALID_PARAM,
                ['2', '2_5'],
            ],
        ];
    }

    public function additionInvalidDataExceptionsProvider()
    {
        $invalidParam = self::$VALID_PARAM;
        unset($invalidParam['domain']);

        return [
            'ex_valid_without_versions' => [
                $invalidParam,
                null,
            ],
            'ex_valid_with_versions' => [
                null,
                ['2', '2_5'],
            ],
        ];
    }

    public function additionInvalidVersionExceptionsProvider()
    {
        return [
            'ex_valid_with_versions' => [
                self::$VALID_PARAM,
                ['2', '2_5', 'no_version'],
            ],
        ];
    }

}
