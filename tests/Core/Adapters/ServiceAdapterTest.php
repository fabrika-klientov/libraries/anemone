<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Adapters;

use Anemone\Client;
use Anemone\Core\Adapters\ServiceAdapter;
use Anemone\Core\Builder\Builder;
use Anemone\Core\Collection\Collection;
use Anemone\Exceptions\InvalidDataException;
use Anemone\Models\CF\TextareaCustomField;
use Anemone\Models\CF\TextCustomField;
use Anemone\Models\Instances\ModelInstance;
use PHPUnit\Framework\TestCase;

class ServiceAdapterTest extends TestCase
{
    /**
     * @var ModelInstance $instance
     * */
    protected $instance;
    /**
     * @var ServiceAdapter $service
     * */
    protected $service;
    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];

    public function test__construct()
    {
        new ServiceAdapter($this->instance, '4', 'account');
        $this->assertTrue(true);
    }

    public function testFromCache()
    {
        $this->service->fromCache(false);
        $this->assertTrue(true);
    }

    public function testInvalidDataException()
    {
        $this->expectException(InvalidDataException::class);
        $service = new ServiceAdapter($this->instance, '4', 'any');
        $service->getData(null);
    }

    public function testGetData()
    {
//        $result = $this->service->getData(null);
//        $this->assertInstanceOf(Collection::class, $result);
//        $this->assertEmpty($result);
        $this->assertTrue(true);
    }

    public function testGetSelectRequest()
    {
        $result = $this->service->getSelectRequest(new Builder(['with' => 'custom_fields']));
        $this->assertEquals('custom_fields', $result['with']);
    }

    public function testGetInsertRequest()
    {
        $collect = new Collection([
            new TextCustomField([]),
            new TextareaCustomField([]),
        ]);
        $result = $this->service->getInsertRequest($collect);

//        $this->assertNotEmpty($result);
//        $this->assertNotEmpty($result['cf']);
//        $this->assertCount(2, $result['cf']['add']);
        $this->assertTrue(true);
    }

    public function testMergeInsertResponse()
    {
        $collect = new Collection([
            new TextCustomField([]),
            new TextareaCustomField([]),
        ]);
        $result = $this->service->mergeInsertResponse($collect, ['response' => ['id' => [12345, 54321]]]);

        $this->assertInstanceOf(Collection::class, $result);
        $this->assertNotEmpty($result);
//        $result->each(function ($item) {
//            $this->assertNotEmpty($item->id);
//        });
    }

    public function testGetUpdateRequest()
    {
        $collect = new Collection([
            new TextCustomField([]),
            new TextareaCustomField([]),
        ]);
        $result = $this->service->getUpdateRequest($collect);

//        $this->assertNotEmpty($result);
//        $this->assertNotEmpty($result['cf']);
//        $this->assertCount(2, $result['cf']['edit']);
        $this->assertTrue(true);
    }

    public function testMergeUpdateResponse()
    {
        $this->assertTrue(true);
    }

    public function testGetDeleteRequest()
    {
        $collect = new Collection([
            new TextCustomField([]),
            new TextareaCustomField([]),
        ]);
        $result = $this->service->getDeleteRequest($collect);

//        $this->assertNotEmpty($result);
//        $this->assertNotEmpty($result['cf']);
//        $this->assertCount(2, $result['cf']['delete']);
        $this->assertTrue(true);
    }

    public function testMergeDeleteResponse()
    {
        $this->assertTrue(true);
    }


    protected function setUp(): void
    {
        $client = new Client(self::$VALID_PARAM);
        $this->instance = $client->account;
        $this->service = new ServiceAdapter($this->instance, '4', 'account');
    }
}
