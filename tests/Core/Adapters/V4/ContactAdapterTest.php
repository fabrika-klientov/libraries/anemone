<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Adapters\V4;

use Anemone\Client;
use Anemone\Core\Adapters\V4\ContactAdapter;
use Anemone\Core\Builder\Builder;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Contact;
use Anemone\Models\Instances\ContactsInstance;
use PHPUnit\Framework\TestCase;

class ContactAdapterTest extends TestCase
{
    /**
     * @var ContactsInstance $instance
     * */
    protected $instance;
    protected $currentPath = __DIR__ . DIRECTORY_SEPARATOR;
    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];

    public function testGetData()
    {
        $fileJSON = 'contacts_response.json';
        $data = json_decode(file_get_contents($this->currentPath . $fileJSON), true);
        $adapter = new ContactAdapter($this->instance, $data);
//        $collection = $adapter->getData(); // have request

        $this->assertTrue(true);
    }

    public function testGetSelectRequest()
    {
        $builder = new Builder();
        $builder->where('id', 12345);
        $adapter = new ContactAdapter($this->instance, $builder);
        $requestData = $adapter->getSelectRequest();

        $this->assertEquals(12345, $requestData['id']);
    }

    public function testGetInsertRequest()
    {
        $collection = new Collection([
            new Contact(null, [
                'name' => 'Contact name',
                'custom_fields' => new Collection(), // exclude request
            ]),
            new Contact(null, [
                'name' => 'Contact name 2',
                'custom_fields' => new Collection(), // exclude request
            ]),
        ]);
        $adapter = new ContactAdapter($this->instance, $collection);
        $requestData = $adapter->getInsertRequest();

        $this->assertNotNull($requestData);
//        $this->assertNotNull($requestData['add']);
//        $this->assertCount(2, $requestData['add']);
    }

    public function testMergeInsertResponse()
    {
        $collection = new Collection([
            new Contact(null, [
                'name' => 'Contact name',
                'custom_fields' => new Collection(), // exclude request
            ]),
        ]);
        $fileJSON = 'contacts_insert_response.json';
        $data = json_decode(file_get_contents($this->currentPath . $fileJSON), true);
        $adapter = new ContactAdapter($this->instance, $data);
//        $collect = $adapter->mergeInsertResponse($collection);

//        $this->assertInstanceOf(Collection::class, $collect);
//        $this->assertTrue($collect->every(function ($item) {
//            return isset($item->id);
//        }));
        $this->assertTrue(true);
    }

    public function testGetUpdateRequest()
    {
        $collection = new Collection([
            new Contact(null, [
                'name' => 'Contact name',
                'custom_fields' => new Collection(), // exclude request
            ])
        ]);
        $adapter = new ContactAdapter($this->instance, $collection);
        $requestData = $adapter->getUpdateRequest();

        $this->assertNotNull($requestData);
//        $this->assertNotNull($requestData['update']);
//        $this->assertCount(1, $requestData['update']);
    }

    public function testMergeUpdateResponse()
    {
        $collection = new Collection([
            new Contact(null, [
                'id' => 1712943,
                'name' => 'Contact name',
                'custom_fields' => new Collection(), // exclude request
            ]),
        ]);
        $fileJSON = 'contacts_update_response.json';
        $data = json_decode(file_get_contents($this->currentPath . $fileJSON), true);
        $adapter = new ContactAdapter($this->instance, $data);
        $collect = $adapter->mergeUpdateResponse($collection);

        $this->assertInstanceOf(Collection::class, $collect);
//        $this->assertTrue($collect->every(function ($item) {
//            return isset($item->updated_at);
//        }));
    }

    public function testGetDeleteRequest()
    {
        $this->assertTrue(true);
    }

    public function testMergeDeleteResponse()
    {
        $this->assertTrue(true);
    }


    protected function setUp(): void
    {
        $client = new Client(self::$VALID_PARAM);
        $this->instance = $client->contacts;
    }
}
