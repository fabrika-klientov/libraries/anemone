<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Adapters\V4;

use Anemone\Client;
use Anemone\Core\Adapters\V4\NoteAdapter;
use Anemone\Core\Builder\Builder;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Instances\NotesInstance;
use Anemone\Models\Note;
use PHPUnit\Framework\TestCase;

class NoteAdapterTest extends TestCase
{

    /**
     * @var NotesInstance $instance
     * */
    protected $instance;
    protected $currentPath = __DIR__ . DIRECTORY_SEPARATOR;
    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];

    public function testGetData()
    {
        $fileJSON = 'notes_response.json';
        $data = json_decode(file_get_contents($this->currentPath . $fileJSON), true);
        $adapter = new NoteAdapter($this->instance, $data);
//        $collection = $adapter->getData(); // have request

        $this->assertTrue(true);
    }

    public function testGetSelectRequest()
    {
        $builder = new Builder();
        $builder->where('id', 12345);
        $adapter = new NoteAdapter($this->instance, $builder);
        $requestData = $adapter->getSelectRequest();

        $this->assertEquals(12345, $requestData['id']);
    }

    public function testGetInsertRequest()
    {
        $collection = new Collection([
            new Note([
                'element_id' => "1712943",
                'element_type' => "2",
                'note_type' => "4",
                'text' => "texta",
            ]),
            new Note([
                'element_id' => "1712943",
                'element_type' => "2",
                'note_type' => "4",
                'text' => "texta 2",
            ]),
        ]);
        $adapter = new NoteAdapter($this->instance, $collection);
        $requestData = $adapter->getInsertRequest();

        $this->assertNotNull($requestData);
//        $this->assertNotNull($requestData['add']);
//        $this->assertCount(2, $requestData['add']);
    }

    public function testMergeInsertResponse()
    {
        $collection = new Collection([
            new Note([
                'element_id' => "1712943",
                'element_type' => "2",
                'note_type' => "4",
                'text' => "texta",
            ]),
        ]);
        $fileJSON = 'notes_insert_response.json';
        $data = json_decode(file_get_contents($this->currentPath . $fileJSON), true);
        $adapter = new NoteAdapter($this->instance, $data);
//        $collect = $adapter->mergeInsertResponse($collection);

//        $this->assertInstanceOf(Collection::class, $collect);
//        $this->assertTrue($collect->every(function ($item) {
//            return isset($item->id);
//        }));
        $this->assertTrue(true);
    }

    public function testGetUpdateRequest()
    {
        $collection = new Collection([
            new Note([
                'element_id' => "1712943",
                'element_type' => "2",
                'note_type' => "4",
                'text' => "texta",
            ])
        ]);
        $adapter = new NoteAdapter($this->instance, $collection);
        $requestData = $adapter->getUpdateRequest();

        $this->assertNotNull($requestData);
//        $this->assertNotNull($requestData['update']);
//        $this->assertCount(1, $requestData['update']);
    }

    public function testMergeUpdateResponse()
    {
        $collection = new Collection([
            new Note([
                'id' => 1712943,
                'name' => 'Lead name',
                'custom_fields' => new Collection(), // exclude request
            ]),
        ]);
        $fileJSON = 'notes_update_response.json';
        $data = json_decode(file_get_contents($this->currentPath . $fileJSON), true);
        $adapter = new NoteAdapter($this->instance, $data);
        $collect = $adapter->mergeUpdateResponse($collection);

        $this->assertInstanceOf(Collection::class, $collect);
    }

    public function testGetDeleteRequest()
    {
        $this->assertTrue(true);
    }

    public function testMergeDeleteResponse()
    {
        $this->assertTrue(true);
    }


    protected function setUp(): void
    {
        $client = new Client(self::$VALID_PARAM);
        $this->instance = $client->leads->notes();
    }
}
