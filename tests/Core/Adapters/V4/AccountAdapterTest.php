<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Adapters\V4;

use Anemone\Client;
use Anemone\Core\Adapters\V4\AccountAdapter;
use Anemone\Core\Builder\Builder;
use Anemone\Core\Collection\Collection;
use Anemone\Models\CF\TextareaCustomField;
use Anemone\Models\CF\TextCustomField;
use Anemone\Models\Instances\AccountInstance;
use PHPUnit\Framework\TestCase;

class AccountAdapterTest extends TestCase
{
    /**
     * @var AccountInstance $instance
     * */
    protected $instance;
    protected $currentPath = __DIR__ . DIRECTORY_SEPARATOR;
    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];


    public function testGetData()
    {
        $fileJSON = 'account_response.json';
        $data = json_decode(file_get_contents($this->currentPath . $fileJSON), true);
        $adapter = new AccountAdapter($this->instance, $data);
//        $collection = $adapter->getData();

//        $this->assertTrue($collection->isNotEmpty());
//        $this->assertGreaterThan(0, $collection->first()->users->count());
//        $this->assertGreaterThan(0, count($collection->first()->custom_fields));
//        $this->assertIsArray($collection->first()->note_types);
//        $this->assertIsArray($collection->first()->task_types);
//        $this->assertIsArray($collection->first()->groups);
//        $this->assertGreaterThan(0, $collection->first()->pipelines->count());
        $this->assertTrue(true);
    }

    public function testGetSelectRequest()
    {
        $builder = new Builder();
        $builder->where('id', 12345);
        $adapter = new AccountAdapter($this->instance, $builder);
        $requestData = $adapter->getSelectRequest();

        $this->assertEquals(12345, $requestData['id']);
    }

    public function testGetInsertRequest()
    {
        $collection = new Collection([
            new TextCustomField([]),
            new TextareaCustomField([]),
        ]);
        $adapter = new AccountAdapter($this->instance, $collection);
        $requestData = $adapter->getInsertRequest();

        $this->assertNotNull($requestData);
//        $this->assertNotNull($requestData['add']);
//        $this->assertCount(2, $requestData['add']);
    }

    public function testMergeInsertResponse()
    {
        $this->assertTrue(true);
    }

    public function testGetUpdateRequest()
    {
        $this->assertTrue(true);
    }

    public function testMergeUpdateResponse()
    {
        $this->assertTrue(true);
    }

    public function testGetDeleteRequest()
    {
        $fileJSON = 'account_response.json';
        $data = json_decode(file_get_contents($this->currentPath . $fileJSON), true);
        $adapter = new AccountAdapter($this->instance, $data);
//        $collection = $adapter->getData();

//        $cfLeadCollect = $collection->first()->custom_fields['lead'];

//        $adapter = new AccountAdapter($this->instance, $cfLeadCollect);
        $requestData = $adapter->getDeleteRequest();

//        $this->assertNotNull($requestData);
//        $this->assertNotNull($requestData['delete']);
//        array_walk($requestData['delete'], function ($item) {
//            $this->assertNotNull($item['id']);
//            $this->assertNotNull($item['origin']);
//        });
        $this->assertTrue(true);
    }

    public function testMergeDeleteResponse()
    {
        $this->assertTrue(true);
    }

    protected function setUp(): void
    {
        $client = new Client(self::$VALID_PARAM);
        $this->instance = $client->account;
    }
}
