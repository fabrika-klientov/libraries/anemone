<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Auth;

use Anemone\Client;
use Anemone\Core\Auth\AuthService;
use Anemone\Core\Auth\CookiesDecorator;
use PHPUnit\Framework\TestCase;

class AuthServiceTest extends TestCase
{
    /**
     * @var Client $client
     * */
    protected $client;
    /**
     * @var AuthService $service
     * */
    protected $service;
    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];

    public function test__construct()
    {
        new AuthService(self::$VALID_PARAM);
        $this->assertTrue(true);
    }

    public function testGetDecorator()
    {
        $v2Decorator = $this->service->getDecorator($this->client->getQueryService());
        $v2_5Decorator = $this->service->getDecorator($this->client->getQueryService());
        $v3Decorator = $this->service->getDecorator($this->client->getQueryService());
        $v_Decorator = $this->service->getDecorator($this->client->getQueryService());

//        $this->assertInstanceOf(AuthV2Decorator::class, $v2Decorator);
        $this->assertInstanceOf(CookiesDecorator::class, $v2_5Decorator);
//        $this->assertInstanceOf(AuthV3Decorator::class, $v3Decorator);
//        $this->assertInstanceOf(AuthV_Decorator::class, $v_Decorator);
    }

    public function testGetDecoratorHasInvalidVersionException()
    {
//        $this->expectException(InvalidVersionException::class);
        $this->service->getDecorator($this->client->getQueryService());
        $this->assertTrue(true);
    }

    public function testGetDecoratorHasInvalidVersionExceptionNotInitVersion()
    {
        $this->client = new Client(self::$VALID_PARAM);
        $this->service = $this->client->getAuthService();
//        $this->expectException(InvalidVersionException::class);
//        $this->service->getDecorator($this->client->getQueryService(), '2_5');
        $this->assertTrue(true);
    }

    public function testGetDomain()
    {
        $data = $this->service->getDomain();
        $this->assertEquals(self::$VALID_PARAM['domain'], $data);
    }

    public function testGetAuthParams()
    {
        $params = $this->service->getAuthParams();
        $this->assertEquals(self::$VALID_PARAM['domain'], $params['domain']);
        $this->assertEquals(self::$VALID_PARAM['login'], $params['login']);
        $this->assertEquals(self::$VALID_PARAM['secret_key'], $params['secret_key']);
    }

    public function testGetVersions()
    {
//        $this->assertEquals(['2', '2_5', '3', '_'], $this->service->getVersions());
        $this->assertTrue(true);
    }

    protected function setUp(): void
    {
        $this->client = new Client(self::$VALID_PARAM);
        $this->service = $this->client->getAuthService();
    }
}
