<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Cache;

use Anemone\Client;
use Anemone\Core\Builder\Builder;
use Anemone\Core\Cache\CacheDriver;
use Anemone\Core\Cache\CacheService;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Customer;
use PHPUnit\Framework\TestCase;

class CacheServiceTest extends TestCase
{
    /**
     * @var CacheService $service
     * */
    protected $service;
    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];

    public function testSet()
    {
        $lead1 = new Customer(null, ['id' => 123456, 'name' => 'Lead name of service']);
        $lead2 = new Customer(null, ['id' => 654321, 'name' => 'Lead name two of service']);
        $collect = new Collection([
            $lead1,
            $lead2,
        ]);
        $this->service->set($collect, '2_5', 'customer');
        $this->assertTrue(true);
    }

    public function testGet()
    {
        $builder = new Builder([
            'id' => 123456,
        ]);

        $collect = $this->service->get($builder,'2_5', 'customer');
        $this->assertNotEmpty($collect);

        $collect = $this->service->get($builder->where('id', 99999),'2_5', 'customer');
        $this->assertEmpty($collect);

        $collect = $this->service->get($builder->where('id', [123456]),'2_5', 'customer');
        $this->assertCount(1, $collect);

        $collect = $this->service->get($builder->where('id', [123456, 654321]),'2_5', 'customer');
        $this->assertCount(2, $collect);

        $collect = $this->service->get(new Builder(),'2_5', 'customer');
        $this->assertNull($collect);
    }

    public function testCleanID()
    {
        $this->service->cleanID('2_5', 'customer', [654321]);

        $collection = $this->service->get((new Builder(['id' => 654321])), '2_5', 'customer');
        $this->assertNull($collection);

        $collection = $this->service->get((new Builder(['id' => 123456])), '2_5', 'customer');
        $this->assertInstanceOf(Customer::class, $collection->first());

        $this->service->cleanID('2_5', 'customer');
        $collection = $this->service->get((new Builder(['id' => 123456])), '2_5', 'customer');
        $this->assertNull($collection);
    }

    protected function setUp(): void
    {
        $client = new Client(self::$VALID_PARAM);
        $driver = new CacheDriver($client);
        $this->service = new CacheService($driver);
    }
}
