<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core;

use Anemone\Core\Cache\CacheData;
use Anemone\Models\Lead;
use PHPUnit\Framework\TestCase;

class CacheDataTest extends TestCase
{
    /**
     * @var CacheData $data
     * */
    protected $data;

    public function test__construct()
    {
        $lead = new Lead(null, ['id' => 1234, 'name' => 'name of lead']);
        new CacheData($lead, time());
        $this->assertTrue(true);
    }

    public function testGetExpires()
    {
        $this->assertLessThanOrEqual(time(), $this->data->getExpires());
    }

    public function testGetModel()
    {
        $this->assertInstanceOf(Lead::class, $this->data->getModel());
    }

    public function testGetID()
    {
        $this->assertEquals(1234, $this->data->getID());
    }

    public function testJsonSerialize()
    {
        $this->assertIsArray($this->data->jsonSerialize());
    }

    protected function setUp(): void
    {
        $lead = new Lead(null, ['id' => 1234, 'name' => 'name of lead']);
        $this->data = new CacheData($lead, time());
    }
}
