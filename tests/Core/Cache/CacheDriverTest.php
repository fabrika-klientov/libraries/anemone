<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Cache;

use Anemone\Client;
use Anemone\Core\Cache\CacheDriver;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Lead;
use PHPUnit\Framework\TestCase;

class CacheDriverTest extends TestCase
{
    /**
     * @var Client $client
     * */
    protected $client;
    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];

    public function test__construct()
    {
        new CacheDriver($this->client);
        $this->assertTrue(true);
    }

    public function testWrite()
    {
        $driver = new CacheDriver($this->client);
        $lead1 = new Lead(null, ['id' => 12345, 'name' => 'Lead name']);
        $lead2 = new Lead(null, ['id' => 54321, 'name' => 'Lead name two']);

        $this->assertTrue($driver->write('lead', '2_5', new Collection([
            $lead1,
            $lead2,
        ])));
    }

    public function testRead()
    {
        $driver = new CacheDriver($this->client);
        $collection = $driver->read('lead', '2_5');

        $this->assertCount(2, $collection);
    }

    public function testClearForID()
    {
        $driver = new CacheDriver($this->client);
        $driver->clearForID('lead', '2_5', [12345]);

        $collection = $driver->read('lead', '2_5');
        $this->assertCount(1, $collection);

        $driver->clearForID('lead', '2_5');
        $collection = $driver->read('lead', '2_5');
        $this->assertCount(0, $collection);
    }


    protected function setUp(): void
    {
        $this->client = new Client(self::$VALID_PARAM);
    }
}
