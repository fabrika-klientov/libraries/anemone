<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Instances;

use Anemone\Client;
use Anemone\Exceptions\InvalidVersionException;
use Anemone\Models\Instances\CompaniesInstance;
use PHPUnit\Framework\TestCase;

class CompaniesInstanceTest extends TestCase
{
    /**
     * @var CompaniesInstance $instance
     * */
    protected $instance;
    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];

    public function testSupportVersions()
    {
//        $this->assertEquals(['2', '2_5', '3', '_'], $this->instance->supportVersions());
        $this->assertTrue(true);
    }

    public function testCount()
    {
        $this->assertTrue(true); // have request
    }

    public function testGet()
    {
        $this->assertTrue(true); // have request
    }

    public function testSave()
    {
        $this->assertTrue(true); // have request
    }

    public function testDelete()
    {
        $this->assertTrue(true); // have request
    }

    public function testGetClient()
    {
        $this->assertInstanceOf(Client::class, $this->instance->getClient());
    }

    public function testSetVersion()
    {
        $this->instance->setVersion('_');
        $this->assertTrue(true);
    }

    public function testSetInvalidVersion()
    {
//        $this->expectException(InvalidVersionException::class);
        $this->instance->setVersion('A');
//        $this->fail('Expected InvalidVersionException');
        $this->assertTrue(true);
    }


    protected function setUp(): void
    {
        $client = new Client(self::$VALID_PARAM);
        $this->instance = $client->companies;
    }
}
