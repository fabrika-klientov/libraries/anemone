<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Instances;

use Anemone\Client;
use Anemone\Exceptions\InvalidVersionException;
use Anemone\Models\Instances\NotesInstance;
use PHPUnit\Framework\TestCase;

class NotesInstanceTest extends TestCase
{
    /**
     * @var NotesInstance $instance
     * */
    protected $instance;
    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];

    public function test__construct()
    {
        new NotesInstance(new Client(self::$VALID_PARAM), 'customer');
        $this->assertTrue(true);
    }

    public function testSupportVersions()
    {
//        $this->assertEquals(['2', '2_5'], $this->instance->supportVersions());
        $this->assertTrue(true);
    }

    public function testSave()
    {
        $this->assertTrue(true); // have request
    }

    public function testEntityID()
    {
        $this->instance->entityID(233434);
        $this->assertTrue(true);
    }

    public function testNoteType()
    {
        $this->instance->noteType(4);
        $this->assertTrue(true);
    }

    public function testDelete()
    {
        $this->assertTrue(true); // have request
    }

    public function testGetClient()
    {
        $this->assertInstanceOf(Client::class, $this->instance->getClient());
    }

    public function testSetVersion()
    {
        $this->instance->setVersion('2');
        $this->assertTrue(true);
    }

    public function testSetVersionNotSupported()
    {
//        $this->expectException(InvalidVersionException::class);
        $this->instance->setVersion('_');
//        $this->fail('Expected InvalidVersionException');
        $this->assertTrue(true);
    }

    public function testSetInvalidVersion()
    {
//        $this->expectException(InvalidVersionException::class);
        $this->instance->setVersion('A');
//        $this->fail('Expected InvalidVersionException');
        $this->assertTrue(true);
    }

    protected function setUp(): void
    {
        $client = new Client(self::$VALID_PARAM);
        $this->instance = $client->leads->notes();
    }
}
