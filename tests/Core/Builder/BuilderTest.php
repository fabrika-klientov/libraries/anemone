<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Builder;

use Anemone\Core\Builder\Builder;
use PHPUnit\Framework\TestCase;

class BuilderTest extends TestCase
{

    public function test__construct()
    {
        new Builder();
        $this->assertTrue(true);
    }

    public function testWhere()
    {
        $builder = new Builder();
        $bThis = $builder->where('id', 12345)
            ->where('updated_at', 145433434)
            ->where('other_key', ['array', 'two']);

        $this->assertInstanceOf(Builder::class, $bThis);
    }

    public function testClear()
    {
        $builder = new Builder();
        $builder->where('id', 12345)
            ->where('updated_at', 145433434)
            ->where('other_key', ['array', 'two']);
        $data = $builder->getResult();
        $this->assertNotEmpty($data);

        $builder->clear();
        $data = $builder->getResult();
        $this->assertEmpty($data);
    }

    public function testGetResult()
    {
        $builder = new Builder();
        $builder->where('id', 12345)
            ->where('updated_at', 145433434)
            ->where('other_key', ['array', 'two']);
        $data = $builder->getResult();

        $this->assertNotEmpty($data);
        $this->assertEquals(['array', 'two'], $data['other_key']);
        $this->assertEquals(145433434, $data['updated_at']);
    }

    public function testHasID()
    {
        $builder = new Builder();
        $builder->where('id', 12345)
            ->where('updated_at', 145433434)
            ->where('other_key', ['array', 'two']);

        $this->assertTrue($builder->hasID());
    }
}
