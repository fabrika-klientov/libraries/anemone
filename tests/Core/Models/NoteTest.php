<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Models;

use Anemone\Models\Note;
use PHPUnit\Framework\TestCase;

class NoteTest extends TestCase
{
    public function test__construct()
    {
        new Note([]);
        $this->assertTrue(true);
    }
}
