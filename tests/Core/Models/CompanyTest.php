<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Models;

use Anemone\Client;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Company;
use Anemone\Models\Instances\CompaniesInstance;
use PHPUnit\Framework\TestCase;

class CompanyTest extends TestCase
{

    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];

    public function test__construct()
    {
        new Company(null, [
            'custom_fields' => new Collection(),
        ]);
        $this->assertTrue(true);
    }
    public function testSetInstance()
    {
        $model = new Company(null, [
            'custom_fields' => new Collection(),
        ]);
        $model->setInstance(new CompaniesInstance(new Client(self::$VALID_PARAM)));
        $this->assertTrue(true);
    }
}
