<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Models;

use Anemone\Client;
use Anemone\Core\Collection\Collection;
use Anemone\Models\Account;
use Anemone\Models\CF\NumericCustomField;
use Anemone\Models\CF\SelectCustomField;
use Anemone\Models\CF\TextareaCustomField;
use Anemone\Models\CF\TextCustomField;
use Anemone\Models\Instances\AccountInstance;
use PHPUnit\Framework\TestCase;

class AccountTest extends TestCase
{
    /**
     * @var AccountInstance $instance
     * */
    protected $instance;
    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];
    protected $cf = [];

    public function test__construct()
    {
        new Account(null, []);
        $this->assertTrue(true);
    }
    public function testSetInstance()
    {
        $model = new Account(null, []);
        $model->setInstance($this->instance);
        $this->assertTrue(true);
    }

    public function testCfLead()
    {
//        $model = new Account($this->instance, [
//            'custom_fields' => $this->cf,
//        ]);
//        $collectCF = $model->cfLead(function ($item) {
//            return $item instanceof TextareaCustomField;
//        });

//        $this->assertNotEmpty($collectCF);

//        $collectCF = $model->cfLead(function ($item) {
//            return $item instanceof NumericCustomField;
//        });
//
//        $this->assertEmpty($collectCF);
        $this->assertTrue(true);
    }

    public function testCfCustomer()
    {
//        $model = new Account($this->instance, [
//            'custom_fields' => $this->cf,
//        ]);
//        $collectCF = $model->cfCustomer();
//
//        $this->assertCount(1, $collectCF);
        $this->assertTrue(true);
    }

    public function testCfContact()
    {
//        $model = new Account($this->instance, [
//            'custom_fields' => $this->cf,
//        ]);
//        $collectCF = $model->cfContact(function ($item) {
//            return $item->field_type == 1;
//        });
//
//        $this->assertNotEmpty($collectCF);
        $this->assertTrue(true);
    }

    public function testCfCompany()
    {
//        $model = new Account($this->instance, [
//            'custom_fields' => $this->cf,
//        ]);
//        $collectCF = $model->cfCompany(function ($item) {
//            return $item->field_type == 1;
//        });
//
//        $this->assertEmpty($collectCF);
        $this->assertTrue(true);
    }

    protected function setUp(): void
    {
        $client = new Client(self::$VALID_PARAM);
        $this->instance = $client->account;

//        $this->cf['lead'] = new Collection([
//            new TextareaCustomField([]),
//        ]);
//        $this->cf['customer'] = new Collection([
//            new NumericCustomField([]),
//        ]);
//        $this->cf['contact'] = new Collection([
//            new TextCustomField([]),
//        ]);
//        $this->cf['company'] = new Collection([
//            new SelectCustomField([]),
//        ]);
    }
}
