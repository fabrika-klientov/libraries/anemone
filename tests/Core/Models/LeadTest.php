<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Models;

use Anemone\Client;
use Anemone\Core\Collection\Collection;
use Anemone\Models\CF\TextCustomField;
use Anemone\Models\Instances\LeadsInstance;
use Anemone\Models\Lead;
use PHPUnit\Framework\TestCase;

class LeadTest extends TestCase
{
    /**
     * @var LeadsInstance $instance
     * */
    protected $instance;
    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];

    public function test__construct()
    {
        new Lead(null, [
            'custom_fields' => new Collection(),
        ]);
        $this->assertTrue(true);
    }

    public function testSetInstance()
    {
        $lead = new Lead(null, [
            'custom_fields' => new Collection(),
        ]);
        $lead->setInstance($this->instance);
        $this->assertTrue(true);
    }

    public function testProperties()
    {
        $lead = new Lead(null, [
            'name' => 'Lead name',
            'created_at' => 14565667767,
            'custom_fields' => new Collection(),
        ]);

        $this->assertEquals('Lead name', $lead->name);
        $this->assertEquals(14565667767, $lead->created_at);
//        $this->assertInstanceOf(Collection::class, $lead->tags);
        $this->assertInstanceOf(Collection::class, $lead->custom_fields);

//        $this->assertCount(0, $lead->tags);
        $this->assertCount(0, $lead->custom_fields);

        $lead->attachTags('new_tag');
        $lead->attachTags('new_tag_two');

        $textCF = new TextCustomField([]);
        $textCF->setValue('custom field value');
//        $lead->custom_fields->push($textCF);

//        $this->assertCount(2, $lead->tags);
//        $this->assertCount(1, $lead->custom_fields);
    }

    protected function setUp(): void
    {
        $client = new Client(self::$VALID_PARAM);
        $this->instance = $client->leads;
    }
}
