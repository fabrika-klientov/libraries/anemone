<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Models;

use Anemone\Core\Collection\Collection;
use Anemone\Models\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function test__construct()
    {
        new User(null, [
            'custom_fields' => new Collection(),
        ]);
        $this->assertTrue(true);
    }
}
