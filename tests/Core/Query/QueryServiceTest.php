<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.29
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Query;

use Anemone\Client;
use Anemone\Core\Query\QueryService;
use PHPUnit\Framework\TestCase;

class QueryServiceTest extends TestCase
{
    /**
     * @var QueryService $service
     * */
    protected $service;
    protected static $VALID_PARAM = [
        'domain' => 'jarvis.amocrm.ru',
        'login' => 'jarvis@google.com',
        'secret_key' => '3d43f35r343d344f5g65h6uh',
    ];


    public function testGet()
    {
        $this->assertTrue(true); // is request
    }

    public function testPost()
    {
        $this->assertTrue(true); // is request
    }

    public function testPatch()
    {
        $this->assertTrue(true); // is request
    }

    public function testDelete()
    {
        $this->assertTrue(true); // is request
    }

    public function testGetUrl()
    {
        $this->assertEquals('https://' . self::$VALID_PARAM['domain'] . '/leads', $this->service->getUrl('leads'));
        $this->assertEquals('https://google.com/leads', $this->service->getUrl('leads', 'google.com'));
        $this->assertEquals('https://google.com/leads', $this->service->getUrl('https://google.com/leads'));
    }

    public function testMerge()
    {
        $this->assertTrue(true);
    }

    protected function setUp(): void
    {
        $client = new Client(self::$VALID_PARAM);
        $this->service = $client->getQueryService();
    }
}
