<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\CF;

use Anemone\Models\CF\RadiobuttonCustomField;
use PHPUnit\Framework\TestCase;

class RadiobuttonCustomFieldTest extends TestCase
{

    public function test__construct()
    {
        new RadiobuttonCustomField([]);
        $this->assertTrue(true);
    }

    public function testTypeId()
    {
        $cf = new RadiobuttonCustomField([]);
//        $this->assertEquals(10, $cf->field_type);
        $this->assertTrue(true);
    }

    public function testSetValue()
    {
        $cf = new RadiobuttonCustomField([
//            'enums' => [123 => 'one', 321 => 'two'],
        ]);
        $cf->setValue('one');
        $this->assertTrue(true);
//        $this->assertEquals(123, $cf->getValue());
    }
}
