<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\CF;

use Anemone\Models\CF\Helpers\SybSmartAddress;
use Anemone\Models\CF\SmartAddressCustomField;
use PHPUnit\Framework\TestCase;

class SmartAddressCustomFieldTest extends TestCase
{

    public function test__construct()
    {
        new SmartAddressCustomField([]);
        $this->assertTrue(true);
    }

    public function testTypeId()
    {
        $cf = new SmartAddressCustomField([]);
//        $this->assertEquals(13, $cf->field_type);
        $this->assertTrue(true);
    }

    public function testGetValue()
    {
        $cf = new SmartAddressCustomField([]);
        $this->assertInstanceOf(SybSmartAddress::class, $cf->getValue());
        $this->assertIsArray($cf->getValue(true));
    }

    public function testSetValue()
    {
        $cf = new SmartAddressCustomField([]);

        $cf->setValue(function (SybSmartAddress $smartAddress) {
            $smartAddress->address_line_1 = 'line 1';
            $smartAddress->address_line_2 = 'line 2';
        });
        $this->assertEquals('line 1', $cf->getValue()->address_line_1);
        $this->assertEquals('line 2', $cf->getValue()->address_line_2);

        $cf->setValue([
            'address_line_1' => 'line 1',
            'address_line_2' => 'line 2',
        ]);
        $this->assertEquals('line 1', $cf->getValue()->address_line_1);
        $this->assertEquals('line 2', $cf->getValue()->address_line_2);
        $this->assertEquals('line 1', $cf->getValue(true)['address_line_1']);
        $this->assertEquals('line 2', $cf->getValue(true)['address_line_2']);
    }
}
