<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\CF;

use Anemone\Models\CF\NumericCustomField;
use PHPUnit\Framework\TestCase;

class NumericCustomFieldTest extends TestCase
{
    public function test__construct()
    {
        new NumericCustomField([]);
        $this->assertTrue(true);
    }

    public function testTypeId()
    {
        $cf = new NumericCustomField([]);
//        $this->assertEquals(2, $cf->field_type);
        $this->assertTrue(true);
    }

    public function testGetValue()
    {
        $cf = new NumericCustomField([
            'values' => [
                ['value' => 12345],
            ],
        ]);
        $this->assertEquals(12345, $cf->getValue());
    }

    public function testSetValue()
    {
        $cf = new NumericCustomField([
            'values' => [
                ['value' => 12345],
            ],
        ]);
        $this->assertEquals(12345, $cf->getValue());

        $cf->setValue(54321);
        $this->assertEquals(54321, $cf->getValue());
    }
}
