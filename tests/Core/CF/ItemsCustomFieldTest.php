<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\CF;

use Anemone\Models\CF\ItemsCustomField;
use PHPUnit\Framework\TestCase;

class ItemsCustomFieldTest extends TestCase
{
    public function test__construct()
    {
        new ItemsCustomField([]);
        $this->assertTrue(true);
    }

    public function testTypeId()
    {
        $cf = new ItemsCustomField([]);
//        $this->assertEquals(16, $cf->field_type);
        $this->assertTrue(true);
    }

    public function testGetValue()
    {
        $this->assertTrue(true); // in future
    }

    public function testSetValue()
    {
        $this->assertTrue(true); // in future
    }
}
