<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\CF;

use Anemone\Models\CF\OrgLegalNameCustomField;
use PHPUnit\Framework\TestCase;

class OrgLegalNameCustomFieldTest extends TestCase
{
    public function test__construct()
    {
        new OrgLegalNameCustomField([]);
        $this->assertTrue(true);
    }

    public function testTypeId()
    {
        $cf = new OrgLegalNameCustomField([]);
//        $this->assertEquals(17, $cf->field_type);
        $this->assertTrue(true);
    }

    public function testGetValue()
    {
        $cf = new OrgLegalNameCustomField([
            'values' => [
                ['value' => ['name' => 'one']],
                ['value' => ['name' => 'two']],
            ],
        ]);

        $this->assertEquals(['one', 'two'], $cf->getValue());
    }

    public function testSetValue()
    {
        $cf = new OrgLegalNameCustomField([
            'values' => [
                ['value' => ['name' => 'one']],
                ['value' => ['name' => 'two']],
            ],
        ]);

        $this->assertEquals(['one', 'two'], $cf->getValue());

        $cf->setValue([
            'apple', 'strawberry',
        ]);

        $this->assertEquals(['apple', 'strawberry'], $cf->getValue());
    }
}
