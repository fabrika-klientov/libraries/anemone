<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\CF;

use Anemone\Models\CF\CheckboxCustomField;
use PHPUnit\Framework\TestCase;

class CheckboxCustomFieldTest extends TestCase
{
    public function test__construct()
    {
        new CheckboxCustomField([]);
        $this->assertTrue(true);
    }

    public function testTypeId()
    {
        $cf = new CheckboxCustomField([]);
//        $this->assertEquals(3, $cf->field_type);
        $this->assertTrue(true);
    }

    public function testGetValue()
    {
        $cf = new CheckboxCustomField([
            'values' => [
                ['value' => 1],
            ],
        ]);

        $this->assertTrue($cf->getValue());
    }

    public function testSetValue()
    {
        $cf = new CheckboxCustomField([
            'values' => [
                ['value' => 1],
            ],
        ]);

        $this->assertTrue($cf->getValue());

        $cf->setValue(false);

        $this->assertFalse($cf->getValue());
    }
}
