<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\CF;

use Anemone\Models\CF\StreetAddressCustomField;
use PHPUnit\Framework\TestCase;

class StreetAddressCustomFieldTest extends TestCase
{
    public function test__construct()
    {
        new StreetAddressCustomField([]);
        $this->assertTrue(true);
    }

    public function testTypeId()
    {
        $cf = new StreetAddressCustomField([]);
//        $this->assertEquals(11, $cf->field_type);
        $this->assertTrue(true);
    }
}
