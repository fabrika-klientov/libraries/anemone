<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\CF;

use Anemone\Models\CF\MultiSelectCustomField;
use PHPUnit\Framework\TestCase;

class MultiSelectCustomFieldTest extends TestCase
{
    public function test__construct()
    {
        new MultiSelectCustomField([]);
        $this->assertTrue(true);
    }

    public function testTypeId()
    {
        $cf = new MultiSelectCustomField([]);
//        $this->assertEquals(5, $cf->field_type);
        $this->assertTrue(true);
    }

    public function testGetValue()
    {
        $cf = new MultiSelectCustomField([
            'enums' => [123 => 'one', 321 => 'two', 555 => 'zero'],
            'values' => [
                ['value' => 'two'],
                ['value' => 'zero'],
            ],
        ]);
        $this->assertTrue(true);
//        $this->assertEquals(['two', 'zero'], $cf->getValue());
    }

    public function testSetValue()
    {
        $cf = new MultiSelectCustomField([
            'enums' => [123 => 'one', 321 => 'two', 555 => 'zero'],
            'values' => [
                ['value' => 'two'],
                ['value' => 'zero'],
            ],
        ]);

        $this->assertEquals(['two', 'zero'], $cf->getValue());

//        $cf->setValue(['one', 'zero']);
//        $this->assertEquals(['one', 'zero'], $cf->getValue());
    }
}
