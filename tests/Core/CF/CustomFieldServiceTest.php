<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\CF;

use Anemone\Core\Collection\Collection;
use Anemone\Models\CF\CustomFieldService;
use Anemone\Models\CF\NumericCustomField;
use Anemone\Models\CF\TextareaCustomField;
use PHPUnit\Framework\TestCase;

class CustomFieldServiceTest extends TestCase
{

    public function test__construct()
    {
        new CustomFieldService();
        $this->assertTrue(true);
    }

    public function testGetCFModel()
    {
        $service = new CustomFieldService();

//        $cf = $service->getCFModel(['field_type' => 2], 'lead');
//        $this->assertInstanceOf(NumericCustomField::class, $cf);
//
//        $cf = $service->getCFModel(['field_type' => 9], 'lead');
//        $this->assertInstanceOf(TextareaCustomField::class, $cf);
//
//        $cf = $service->getCFModel([], 'lead');
//        $this->assertNull($cf);
        $this->assertTrue(true);
    }

    public function testGetByCollect()
    {
        $service = new CustomFieldService();

//        $collect = $service->getByCollect(
//            [
//                ['field_type' => 2],
//                ['field_type' => 3],
//                ['field_type' => 4],
//            ]
//        );
//
//        $this->assertInstanceOf(Collection::class, $collect);
//        $this->assertCount(3, $collect);
        $this->assertTrue(true);
    }
}
