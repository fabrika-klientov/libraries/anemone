<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\CF\Helpers;

use Anemone\Models\CF\Helpers\SubLegalEntity;
use PHPUnit\Framework\TestCase;

class SubLegalEntityTest extends TestCase
{

    public function test__construct()
    {
        new SubLegalEntity();
        $this->assertTrue(true);
    }

    public function testGetValues()
    {
        $sub = new SubLegalEntity([
            'name' => 'same name',
            'vat_id' => 123,
            'kpp' => 321,
        ]);

        $this->assertEquals([
            'name' => 'same name',
            'vat_id' => 123,
            'kpp' => 321,
        ], $sub->getValues());
    }

    public function testJsonSerialize()
    {
        $sub = new SubLegalEntity([
            'name' => 'same name',
            'vat_id' => 123,
            'kpp' => 321,
        ]);

        $this->assertEquals([
            'name' => 'same name',
            'vat_id' => 123,
            'kpp' => 321,
        ], $sub->getValues());

        $sub->vat_id = 555;

        $this->assertEquals([
            'name' => 'same name',
            'vat_id' => 555,
            'kpp' => 321,
        ], $sub->getValues());
    }
}
