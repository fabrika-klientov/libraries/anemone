<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\CF\Helpers;

use Anemone\Models\CF\Helpers\SybSmartAddress;
use PHPUnit\Framework\TestCase;

class SybSmartAddressTest extends TestCase
{

    public function test__construct()
    {
        new SybSmartAddress([]);
        $this->assertTrue(true);
    }

    public function testGetValues()
    {
        $smart = new SybSmartAddress([
            'address_line_1' => 'line 1',
            'address_line_2' => 'line 2',
            'city' => 'city any',
            'state' => 'California',
            'country' => 'USA',
        ]);

        $this->assertEquals([
            'address_line_1' => 'line 1',
            'address_line_2' => 'line 2',
            'city' => 'city any',
            'state' => 'California',
            'country' => 'USA',
        ], $smart->getValues());

        $smart->zip = 9988;
        $smart->city = 'new city';
        $this->assertEquals([
            'address_line_1' => 'line 1',
            'address_line_2' => 'line 2',
            'city' => 'new city',
            'state' => 'California',
            'country' => 'USA',
            'zip' => 9988,
        ], $smart->getValues());
    }
}
