<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\CF;

use Anemone\Models\CF\MultiTextCustomField;
use PHPUnit\Framework\TestCase;

class MultiTextCustomFieldTest extends TestCase
{
    public function test__construct()
    {
        new MultiTextCustomField([]);
        $this->assertTrue(true);
    }

    public function testTypeId()
    {
        $cf = new MultiTextCustomField([]);
//        $this->assertEquals(8, $cf->field_type);
        $this->assertTrue(true);
    }

    public function testGetValue()
    {
        $cf = new MultiTextCustomField([
            'enums' => [123 => 'WORK', 321 => 'HOME'],
            'values' => [
                ['value' => 1234567, 'enum' => 123],
                ['value' => 7654321, 'enum' => 321],
            ],
        ]);

        $this->assertTrue(true);
//        $this->assertEquals(1234567, $cf->getValue());
//        $this->assertEquals(1234567, $cf->getValue('WORK'));
//        $this->assertEquals(7654321, $cf->getValue('HOME'));
    }

    public function testSetValue()
    {
        $cf = new MultiTextCustomField([
            'enums' => [123 => 'WORK', 321 => 'HOME'],
            'values' => [
                ['value' => 1234567, 'enum' => 123],
                ['value' => 7654321, 'enum' => 321],
            ],
        ]);

//        $cf->setValue('jarvis@google.com');
//        $cf->setValue('google@google.com', 'HOME');

        $this->assertTrue(true);
//        $this->assertEquals('jarvis@google.com', $cf->getValue());
//        $this->assertEquals('jarvis@google.com', $cf->getValue('WORK'));
//        $this->assertEquals('google@google.com', $cf->getValue('HOME'));
    }
}
