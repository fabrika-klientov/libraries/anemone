<?php
/**
 *
 * @package   Anemone
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.30
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\CF;

use Anemone\Models\CF\Helpers\SubLegalEntity;
use Anemone\Models\CF\LegalEntityCustomField;
use PHPUnit\Framework\TestCase;

class LegalEntityCustomFieldTest extends TestCase
{
    public function test__construct()
    {
        new LegalEntityCustomField([]);
        $this->assertTrue(true);
    }

    public function testTypeId()
    {
        $cf = new LegalEntityCustomField([]);
//        $this->assertEquals(15, $cf->field_type);
        $this->assertTrue(true);
    }

    public function testGetValue()
    {
        $cf = new LegalEntityCustomField([
            'values' => [
                [
                    'value' => [
                        'name' => 'name 1',
                        'vat_id' => 12345,
                        'kpp' => 54321,
                    ],
                ],
                [
                    'value' => [
                        'name' => 'name 2',
                        'vat_id' => 23456,
                        'kpp' => 65432,
                    ],
                ],
            ],
        ]);

        $this->assertEquals('name 1', $cf->getValue()[0]->name);
        $this->assertEquals(12345, $cf->getValue()[0]->vat_id);
        $this->assertEquals(65432, $cf->getValue()[1]->kpp);
        $this->assertEquals('name 1', $cf->getValue(true)[0]['name']);
        $this->assertEquals(12345, $cf->getValue(true)[0]['vat_id']);
        $this->assertEquals(65432, $cf->getValue(true)[1]['kpp']);
    }

    public function testSetValue()
    {
        $cf = new LegalEntityCustomField([]);
        $cf->setValue(function ($subLegalEntityArray) {
            $s1 = new SubLegalEntity();
            $s1->name = 'name 1';
            $s1->vat_id = 12345;
            $s1->kpp = 54321;

            $s2 = new SubLegalEntity([
                'name' => 'name 2',
                'vat_id' => 23456,
                'kpp' => 65432,
            ]);
            return [
                $s1,
                $s2,
            ];
        });

        $this->assertEquals('name 1', $cf->getValue()[0]->name);
        $this->assertEquals(12345, $cf->getValue()[0]->vat_id);
        $this->assertEquals(65432, $cf->getValue()[1]->kpp);
        $this->assertEquals('name 1', $cf->getValue(true)[0]['name']);
        $this->assertEquals(12345, $cf->getValue(true)[0]['vat_id']);
        $this->assertEquals(65432, $cf->getValue(true)[1]['kpp']);
    }
}
